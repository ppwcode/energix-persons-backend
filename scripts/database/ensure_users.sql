DECLARE @sql NVARCHAR(4000)


-- Add Role db_spexecutor, for RW users
IF NOT EXISTS (SELECT 1 FROM sys.database_principals WHERE TYPE = 'R' AND NAME = N'db_spexecutor')
BEGIN
    CREATE ROLE db_spexecutor;
    GRANT EXECUTE TO db_spexecutor;
END

-- Add DBO-user
IF NOT EXISTS (SELECT 1 FROM sys.sysusers WHERE NAME = N'$(LoginDbo)')
BEGIN
    SET @sql = '
    CREATE USER $(LoginDbo) WITH PASSWORD = ''$(LoginDbo)''
    ALTER ROLE db_owner ADD MEMBER $(LoginDbo);
    ';

    EXEC dbo.sp_executesql @sql;
END

-- Add RW-user
IF NOT EXISTS (SELECT 1 FROM sys.sysusers WHERE NAME = N'$(LoginRw)')
BEGIN
    SET @sql = '
    CREATE USER $(LoginRw) WITH PASSWORD = ''$(LoginRw)''
    ALTER ROLE db_datareader ADD MEMBER $(LoginRw);
    ALTER ROLE db_datawriter ADD MEMBER $(LoginRw);
    ALTER ROLE db_spexecutor ADD MEMBER $(LoginRw);
    ';

    EXEC dbo.sp_executesql @sql;
END
