﻿USE master;

--Make sure contained databases is enabled
exec sp_configure 'contained database authentication', 1;  
GO  
RECONFIGURE;  
GO  

DECLARE @sql NVARCHAR(4000)


IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE Name = '$(Database)')
BEGIN
	SET @sql = 
	'CREATE DATABASE [$(Database)];' +
	'ALTER DATABASE $(Database) SET CONTAINMENT = PARTIAL;' +
	'ALTER DATABASE $(Database) MODIFY FILE ( NAME = N''$(Database)'', SIZE = 65536KB , FILEGROWTH = 1024KB );' +
	'ALTER DATABASE $(Database) MODIFY FILE ( NAME = N''$(Database)_log'', SIZE = 65536KB , FILEGROWTH = 10%);' +
	'ALTER DATABASE $(Database) SET ALLOW_SNAPSHOT_ISOLATION ON'	
	EXEC dbo.sp_executesql @sql;
	EXEC dbo.sp_dbcmptlevel @dbname=$(Database), @new_cmptlevel=130;
END
