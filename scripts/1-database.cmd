@echo off
cls

echo ========================================================
echo PRESS 1, 2, 3,... to choose how the SQL DB will be setup
echo ========================================================
echo.
echo 1 - Setup Energix-Persons-db with SQL Server
echo 2 - [92m(default)[0m Run Energix-Persons Migrations
echo 3 - Drop all content (in future maybe  Re-run Energix-Persons Migrations)
echo 99 - EXIT
echo.

:: default action
set M=2
set /P M=Type 1, 2, 3,... then press ENTER: 
IF %M%==1 goto :create-database-core
IF %M%==2 goto :migrate-persons
IF %M%==3 goto :drop-core
IF %M%==99 goto :done
goto :done


:create-database-core
echo [92mCreating databases[0m
call sqlcmd -S localhost -v Database="Energix_Persons" -i "database/create_new_database.sql" -E
call sqlcmd -S localhost -v LoginDbo=Energix_Persons_dbo -v LoginRw=Energix_Persons_rw -d Energix_Persons -i "database/ensure_users.sql" -E
goto :endofscript

:drop-core
echo.
echo [92mRun script, empty DB[0m
call sqlcmd -S localhost -U Energix_Persons_dbo -P Energix_Persons_dbo -d Energix_Persons -i "database/empty_database.sql"
goto :endofscript

:migrate-persons
echo.
echo [92mdotnet build[0m
cd ..\src
call dotnet build Energix.Persons.Migrations.Cli\Energix.Persons.Migrations.Cli.csproj
cd ..\scripts
IF %ERRORLEVEL% == 1 goto :buildfailed

echo.
echo [92mMigrate Energix-Persons Core db[0m
cd ..\
call scratch\bin\Energix.Persons.Migrations.Cli-Development-AnyCPU\net6.0\Energix.Persons.Migrations.Cli.exe
cd ..\..\scripts
IF %ERRORLEVEL% == 1 goto :buildfailed
goto :endofscript




:endofscript
echo.
echo [92mScript Finished[0m
echo.
goto :done

:buildfailed
echo [91mBuild failed[0m

:done