﻿using System;
using System.Collections.Generic;

using Castle.Core;
using Castle.Core.Logging;
using Castle.DynamicProxy;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;

using Moq;

namespace Energix.Persons.Server.Tests.Common
{
    public class AutoMoqResolver : ISubDependencyResolver
    {
        private static readonly HashSet<Type> _castleWindsorTypes =
            new HashSet<Type>
            {
                typeof(IKernelInternal),
                typeof(IKernel),
                typeof(ITypedFactoryComponentSelector),
                typeof(IInterceptor),
                typeof(ILogger)
            };

        public AutoMoqResolver(IKernel kernel)
        {
            Kernel = kernel;
        }

        public IKernel Kernel { get; }

        public bool CanResolve(
            CreationContext context,
            ISubDependencyResolver contextHandlerResolver,
            ComponentModel model,
            DependencyModel dependency)
            => dependency.TargetType.IsInterface
               && !Kernel.HasComponent(dependency.TargetType)
               && !_castleWindsorTypes.Contains(dependency.TargetType);

        public object Resolve(
            CreationContext context,
            ISubDependencyResolver contextHandlerResolver,
            ComponentModel model,
            DependencyModel dependency)
        {
            Type mockType = typeof(Mock<>).MakeGenericType(dependency.TargetType);
            return ((Mock)Kernel.Resolve(mockType, context.AdditionalArguments)).Object;
        }
    }
}
