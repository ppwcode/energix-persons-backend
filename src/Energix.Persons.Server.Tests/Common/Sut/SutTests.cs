﻿using System;
using System.Diagnostics.CodeAnalysis;

using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Castle.Windsor.Proxy;

using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.BusinessLogic.PPW;

using Moq;

using PPWCode.Host.Core;
using PPWCode.Server.Core.RequestContext.Interfaces;
using PPWCode.Vernacular.NHibernate.III;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Tests.Common.Sut
{
    /// <summary>
    ///     Based on <see href="http://blog.ploeh.dk/2013/03/11/auto-mocking-container/" />
    /// </summary>
    /// <typeparam name="T">The type of the system under test</typeparam>
    [ExcludeFromCodeCoverage]
    public abstract class SutTests<T> : BaseTests
        where T : class
    {
        private T _sut;
        protected Mock<ITimeProvider> TimeProviderMock { get; private set; }
        protected Mock<IEnergixRequestContext> RequestContextMock { get; private set; }
        protected Mock<IIdentityProvider> IdentityProviderMock { get; private set; }

        protected T Sut
        {
            get
            {
                if ((_sut == null) && (Container != null))
                {
                    _sut = (T)Container.Resolve(typeof(T));
                }

                return _sut;
            }
        }

        protected override void OnSetup()
        {
            Container = OnCreateContainer;
            OnAfterCreateContainer(Container);
        }

        protected virtual IWindsorContainer OnCreateContainer
            => new WindsorContainer(
                    new DefaultKernel(
                        new InlineDependenciesPropagatingDependencyResolver(),
                        new DefaultProxyFactory()),
                    new DefaultComponentInstaller())
                .AddFacility<SutTestFacility>(
                    f =>
                    {
                        f.SutType = typeof(T);
                        f.MockBehavior = MockBehavior.Strict;
                    });

        protected virtual void OnAfterCreateContainer(IWindsorContainer container)
        {
            OnRegisterMocks(container);
        }

        protected virtual void OnRegisterMocks(IWindsorContainer container)
        {
            // services
            TimeProviderMock = new Mock<ITimeProvider>();
            RequestContextMock = new Mock<IEnergixRequestContext>();
            IdentityProviderMock = new Mock<IIdentityProvider>();

            // setup service mocks
            OnSetupMocks(container);

            // Register services
            container
                .Register(
                    Component
                        .For<ITimeProvider>()
                        .Instance(TimeProviderMock.Object),
                    Component
                        .For<IEnergixRequestContext>()
                        .Instance(RequestContextMock.Object),
                    Component
                        .For<IIdentityProvider>()
                        .Instance(IdentityProviderMock.Object));
        }

        protected virtual void OnSetupMocks(IWindsorContainer container)
        {
            OnSetupTimeProviderMock(TimeProviderMock);
            OnSetupRequestContextMock(RequestContextMock);
            OnSetupIdentityProviderMock(IdentityProviderMock);
        }

        protected virtual void OnSetupTimeProviderMock(Mock<ITimeProvider> mock)
        {
            DateTime currentTime = GetUtcTime(2018, 07, 02, 01, 00, 10);
            UtcNowFunc = () => currentTime;
            mock
                .Setup(tp => tp.Now)
                .Returns(() => UtcNowFunc());
            mock
                .Setup(tp => tp.UtcNow)
                .Returns(() => UtcNowFunc());
        }

        protected DateTime UtcNow
            => Container.Resolve<ITimeProvider>().UtcNow;

        protected DateTime Now
            => Container.Resolve<ITimeProvider>().Now;

        protected DateTime UtcToday
            => Container.Resolve<ITimeProvider>().UtcNow.Date;

        protected DateTime Today
            => Container.Resolve<ITimeProvider>().Now.Date;

        protected Func<DateTime> UtcNowFunc { get; set; }

        protected virtual void OnSetupRequestContextMock(Mock<IEnergixRequestContext> mock)
        {
            mock
                .Setup(rc => rc.RequestTimestamp)
                .Returns(() => UtcNowFunc());
        }

        protected virtual void OnSetupIdentityProviderMock(Mock<IIdentityProvider> mock)
        {
            IdentityName = () => "SutTestUser";
            mock
                .Setup(ip => ip.IdentityName)
                .Returns(() => IdentityName());
        }

        protected virtual Func<string> IdentityName { get; set; }

        protected override void OnTearDown()
        {
            TimeProviderMock = null;
            RequestContextMock = null;
            IdentityProviderMock = null;

            if (_sut != null)
            {
                Container.Release(_sut);
                _sut = null;
            }

            if (Container != null)
            {
                Container.Dispose();
                Container = null;
            }
        }

        public IWindsorContainer Container { get; private set; }
    }
}
