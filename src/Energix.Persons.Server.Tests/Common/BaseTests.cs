﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;

using NUnit.Framework;

using PPWCode.Vernacular.Exceptions.IV;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Tests.Common
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public abstract class BaseTests
    {
        [SetUp]
        public void Setup()
        {
            OnSetup();
        }

        [TearDown]
        public void TearDown()
        {
            OnTearDown();
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            OnOneTimeSetup();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            OnOneTimeTearDown();
        }

        private int _nextId = 1000000;

        protected virtual int NextId
            => Interlocked.Increment(ref _nextId);

        protected virtual void OnOneTimeTearDown()
        {
        }

        protected virtual void OnOneTimeSetup()
        {
        }

        protected virtual void OnSetup()
        {
        }

        protected virtual void OnTearDown()
        {
        }

        protected virtual void CheckCivilized(params ICivilizedObject[] civilizedObjects)
        {
            if (civilizedObjects != null)
            {
                CheckCivilized(civilizedObjects.AsEnumerable());
            }
        }

        protected virtual void CheckCivilized([NotNull] IEnumerable<ICivilizedObject> civilizedObjects)
        {
            CompoundSemanticException cse = new CompoundSemanticException();
            foreach (ICivilizedObject civilizedObject in civilizedObjects.Where(co => !co.IsCivilized))
            {
                cse.AddElement(
                    new SemanticException(
                        $"Each test requires that objects of type, {civilizedObject.GetType().FullName}, are civilized.",
                        civilizedObject.WildExceptions()));
            }

            if (!cse.IsEmpty)
            {
                throw cse;
            }
        }

        protected virtual DateTime GetUtcTime(
            int year,
            int month,
            int day,
            int hour,
            int minute,
            int second)
            => new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc);

        protected virtual DateTime GetUtcDate(int year, int month, int day)
            => GetUtcTime(year, month, day, 0, 0, 0);
    }
}
