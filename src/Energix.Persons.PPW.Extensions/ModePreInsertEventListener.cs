﻿using System.Threading;
using System.Threading.Tasks;

using NHibernate.Event;

namespace Energix.Persons.PPW.Extensions;

public class ModePreInsertEventListener : IPreInsertEventListener
{
    /// <inheritdoc />
    public Task<bool> OnPreInsertAsync(PreInsertEvent @event, CancellationToken cancellationToken)
    {
        if (!(@event.Entity is IMode persistentObject))
        {
            return Task.FromResult(false);
        }

        var requestContext = ((AuditInterceptorWithDi)@event.Session.Interceptor).GetService<IEnergixRequestContext>();
        var mode = requestContext?.Mode;
        persistentObject.Mode = mode;
        return Task.FromResult(false);
    }

    /// <inheritdoc />
    public bool OnPreInsert(PreInsertEvent @event)
    {
        if (!(@event.Entity is IMode persistentObject))
        {
            return false;
        }

        var requestContext = ((AuditInterceptorWithDi)@event.Session.Interceptor).GetService<IEnergixRequestContext>();
        var mode = requestContext?.Mode;
        persistentObject.Mode = mode;
        return false;
    }
}