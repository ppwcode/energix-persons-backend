using System;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

using PPWCode.Server.Core.RequestContext.Implementations;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.PPW.Extensions;

public class EnergixWebApiRequestContext
    : WebApiRequestContext,
      IEnergixRequestContext
{
    public const string ModeHeader = "x-mode";

    public EnergixWebApiRequestContext(
        [JetBrains.Annotations.NotNull] ITimeProvider timeProvider,
        [JetBrains.Annotations.NotNull] ControllerContext controllerContext)
        : base(timeProvider, controllerContext)
    {
    }

    public string Mode
        => GetModeFromRequestHeader(ControllerContext.HttpContext);

    public static string GetModeFromRequestHeader(HttpContext httpContext)
    {
        if (!httpContext.Request.Headers.ContainsKey(ModeHeader))
        {
            throw new ApplicationException($"Unable to find x-mode header");
        }

        StringValues res = httpContext.Request.Headers[ModeHeader];
        return res;
    }
}
