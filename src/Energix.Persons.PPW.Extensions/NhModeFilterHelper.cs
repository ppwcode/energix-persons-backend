using System.Collections.Generic;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Engine;
using NHibernate.Type;

namespace Energix.Persons.PPW.Extensions;

public class NhModeFilterHelper
{
    public const string Name = "ModeFilter";

    public const string ModeParameterName = "mode";

    public static void Configure(Configuration nhConfiguration)
    {
        nhConfiguration.AddFilterDefinition(
            new FilterDefinition(
                Name,
                "mode = :mode",
                new Dictionary<string, IType> { { ModeParameterName, NHibernateUtil.String } },
                true));
    }
}
