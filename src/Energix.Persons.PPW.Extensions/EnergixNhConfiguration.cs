﻿using System.Collections.Generic;
using System.Reflection;

using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Event;
using NHibernate.Mapping;

using PPWCode.Vernacular.NHibernate.III;

namespace Energix.Persons.PPW.Extensions;

public class EnergixNhConfiguration : NhConfiguration
{
    public EnergixNhConfiguration(
        INhInterceptor nhInterceptor,
        INhProperties nhProperties,
        IMappingAssemblies mappingAssemblies,
        IPpwHbmMapping ppwHbmMapping,
        IRegisterEventListener[] registerEventListeners,
        IAuxiliaryDatabaseObject[] auxiliaryDatabaseObjects)
        : base(
            nhInterceptor,
            nhProperties,
            mappingAssemblies,
            ppwHbmMapping,
            registerEventListeners,
            auxiliaryDatabaseObjects)
    {
    }

    /// <inheritdoc />
    protected override Configuration Configuration
    {
        get
        {
            Configuration configuration = new Configuration();

            configuration.Configure();

            // start:: Energix Custom
            configuration.SetListeners(ListenerType.PreInsert, new[] { typeof(ModePreInsertEventListener).AssemblyQualifiedName });
            configuration.SetListeners(ListenerType.PostLoad, new[] { typeof(ModePostLoadEventListener).AssemblyQualifiedName });
            NhModeFilterHelper.Configure(configuration); // must happen BEFORE mapping registration, else classCustomizer Filter method registers dummy definition (and you get duplicateKeyException)

            // end:: Energix Custom

            // Overrule properties if necessary
            foreach (KeyValuePair<string, string> item in NhProperties.GetProperties(configuration))
            {
                if (configuration.Properties.ContainsKey(item.Key))
                {
                    if (string.IsNullOrWhiteSpace(item.Value))
                    {
                        configuration.Properties.Remove(item.Key);
                    }
                    else
                    {
                        configuration.SetProperty(item.Key, item.Value);
                    }
                }
                else
                {
                    configuration.Properties.Add(item);
                }
            }

            // Register interceptor / event-listeners
            var interceptor = NhInterceptor.GetInterceptor();
            if (interceptor != null)
            {
                configuration.SetInterceptor(interceptor);
            }

            foreach (IRegisterEventListener registerListener in RegisterEventListeners)
            {
                registerListener.Register(configuration);
            }

            HbmMapping hbmMapping = PpwHbmMapping.HbmMapping;
            if (hbmMapping != null)
            {
                configuration.AddMapping(hbmMapping);
            }

            // map embedded resource of specified assemblies
            foreach (Assembly assembly in MappingAssemblies.GetAssemblies())
            {
                configuration.AddAssembly(assembly);
            }

            foreach (IAuxiliaryDatabaseObject auxiliaryDatabaseObject in AuxiliaryDatabaseObjects)
            {
                IPpwAuxiliaryDatabaseObject ppwAuxiliaryDatabaseObject = auxiliaryDatabaseObject as IPpwAuxiliaryDatabaseObject;
                ppwAuxiliaryDatabaseObject?.SetConfiguration(configuration);
                configuration.AddAuxiliaryDatabaseObject(auxiliaryDatabaseObject);
            }

            return configuration;
        }
    }
}
