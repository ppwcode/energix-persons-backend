using PPWCode.Server.Core.RequestContext.Interfaces;

namespace Energix.Persons.PPW.Extensions;

public interface IEnergixRequestContext : IRequestContext
{
    public string Mode { get; }
}