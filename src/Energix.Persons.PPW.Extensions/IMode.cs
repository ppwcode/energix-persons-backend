namespace Energix.Persons.PPW.Extensions;

public interface IMode
{
    public string Mode { get; set; }
}
