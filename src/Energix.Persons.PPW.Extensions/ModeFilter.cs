using System.Threading.Tasks;

using Castle.Windsor;

using JetBrains.Annotations;

using Microsoft.AspNetCore.Mvc.Filters;

using NHibernate;

using PPWCode.Host.Core.WebApi;

namespace Energix.Persons.PPW.Extensions;

public class ModeFilter
    : Filter,
      IAsyncActionFilter,
      IAsyncAlwaysRunResultFilter
{
    public ModeFilter([NotNull] IWindsorContainer container)
        : base(container)
    {
    }

    /// <inheritdoc />
    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        ISession session = Container.Resolve<ISession>();

        var modeFilterValue = EnergixWebApiRequestContext.GetModeFromRequestHeader(context.HttpContext);
        session.EnableFilter(NhModeFilterHelper.Name).SetParameter(NhModeFilterHelper.ModeParameterName, modeFilterValue);

        await next();
    }

    /// <inheritdoc />
    public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
    {
        await next();
    }
}
