using System;

using JetBrains.Annotations;

using Microsoft.Extensions.DependencyInjection;

using PPWCode.Vernacular.NHibernate.III;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.PPW.Extensions
{
    /// <inheritdoc cref="AuditInterceptor{T}" />
    [UsedImplicitly]
    public class AuditInterceptorWithDi : AuditInterceptor<long>
    {
        private readonly IServiceProvider _serviceProvider;

        public AuditInterceptorWithDi(
            IServiceProvider serviceProvider,
            [NotNull] IIdentityProvider identityProvider,
            [NotNull] ITimeProvider timeProvider)
            : base(identityProvider, timeProvider, true)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// hack based on https://github.com/nhibernate/nhibernate-core/issues/919
        /// you an only have one interceptor in NH , you can have multiple listeners but those cannot use DI
        /// you can access the interceptor in the listener ...
        /// </summary>
        /// <typeparam name="T">type you want to resolve</typeparam>
        /// <returns>instance of the type</returns>
        public T GetService<T>() => _serviceProvider.GetService<T>();
    }
}
