using System;

using NHibernate.Event;

namespace Energix.Persons.PPW.Extensions;

/// <summary>
/// NH filters don't affect Get or Load
/// user post load event listener to avoid leaking unwanted data to application
/// </summary>
public class ModePostLoadEventListener : IPostLoadEventListener
{
    public void OnPostLoad(PostLoadEvent @event)
    {
        if (!(@event.Entity is IMode persistentObject))
        {
            return;
        }

        var requestContext = ((AuditInterceptorWithDi)@event.Session.Interceptor).GetService<IEnergixRequestContext>();
        var mode = requestContext?.Mode;
        if (persistentObject.Mode != mode)
        {
            throw new ApplicationException($"Entity '{@event.Entity.GetType().Name}' not found for current mode '{mode}'");
        }
    }
}
