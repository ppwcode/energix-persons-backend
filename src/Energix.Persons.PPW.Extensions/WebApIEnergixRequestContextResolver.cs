using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;

using JetBrains.Annotations;

using PPWCode.Server.Core.RequestContext.Implementations;
using PPWCode.Server.Core.RequestContext.Interfaces;

namespace Energix.Persons.PPW.Extensions;

public class WebApiEnergixRequestContextResolver : ISubDependencyResolver
{
    public WebApiEnergixRequestContextResolver([NotNull] IKernel kernel)
    {
        Kernel = kernel;
    }

    public IKernel Kernel { get; }

    public bool CanResolve(
        [NotNull] CreationContext context,
        [NotNull] ISubDependencyResolver contextHandlerResolver,
        [NotNull] ComponentModel model,
        [NotNull] DependencyModel dependency)
    {
        const string ControllerContext = "controllerContext";

        return (dependency.TargetType == typeof(IRequestContext))
               && context.AdditionalArguments.Contains(ControllerContext)
               && context.AdditionalArguments[ControllerContext] is ControllerContext;
    }

    public object Resolve(
        [NotNull] CreationContext context,
        [NotNull] ISubDependencyResolver contextHandlerResolver,
        [NotNull] ComponentModel model,
        [NotNull] DependencyModel dependency)
        => Kernel.Resolve<WebApiRequestContext>(context.AdditionalArguments);
}