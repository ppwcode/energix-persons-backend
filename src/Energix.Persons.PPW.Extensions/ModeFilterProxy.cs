﻿using System.Threading.Tasks;

using Castle.Windsor;

using JetBrains.Annotations;

using Microsoft.AspNetCore.Mvc.Filters;

using PPWCode.Host.Core.WebApi;

namespace Energix.Persons.PPW.Extensions;

public class ModeFilterProxy<T>
    : FilterProxy<T>,
      IAsyncActionFilter,
      IAsyncAlwaysRunResultFilter
    where T : ModeFilter,
    IAsyncActionFilter,
    IAsyncAlwaysRunResultFilter
{
    public ModeFilterProxy([NotNull] IWindsorContainer container, int order)
        : base(container, order)
    {
    }

    /// <inheritdoc />
    public Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        T instance = CreateFilterInstance(Arguments);
        return instance.OnActionExecutionAsync(context, next);
    }

    /// <inheritdoc />
    public Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        => CreateFilterInstance(Arguments).OnResultExecutionAsync(context, next);
}
