﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using NUnit.Framework;

using List = NHibernate.Mapping.List;

namespace Energix.Persons.Host.API.Tests;

public class AffiliateTests : TestBase
{
    public AffiliateTests()
    {
    }

    [Test]
    public async Task NonExistedAffiliateShouldReturn404()
    {
        Console.WriteLine($"TestSetup {PostgressMode} {ApiBaseUrl}");
        var response = await HttpClient.GetAsync($"/api/v1/Affiliate/{int.MaxValue}");
        Assert.NotNull(response);
        var responseBody = await response.Content.ReadAsStringAsync();
        Console.Write(responseBody);
        Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
    }

    [Test]
    public async Task ShouldBeAbleToCreateAndRetrieveAffiliateOnlyInCurrentMode()
    {
        var affiliate = await CreateAffiliate();

        // get in different mode
        var getRequestAfterSave1 = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/Affiliate/{affiliate.Id}");
        getRequestAfterSave1.Headers.Add("x-mode", $"automated-test-{Guid.NewGuid()}");
        var otherModeResponse = await HttpClient.SendAsync(getRequestAfterSave1);
        Assert.True(otherModeResponse.StatusCode == HttpStatusCode.NotFound);

        // same mode as saving
        var getRequestAfterSave2 = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/Affiliate/{affiliate.Id}");
        var currentModeGetResponse = await HttpClient.SendAsync(getRequestAfterSave2);
        Assert.True(currentModeGetResponse.IsSuccessStatusCode);
    }

    [Test]
    public async Task ShouldBeAbleToUpdatePropertiesAndCreateCorrectHistory()
    {
        var affiliate = await CreateAffiliate();
        await Task.Delay(TimeSpan.FromSeconds(2));

        var dateBeforeUpdate = DateTime.UtcNow;
        var json = @"
{
  ""id"":""" + affiliate.Id + @""",
  ""firstName"": ""tim"",
  ""lastName"": ""stuyckens"",
  ""inss"": """ + affiliate.Inss + @""",
  ""dateOfBirth"": ""2022-05-04"",
  ""mortalityTableGender"": ""Male"",
  ""language"": ""NlBe"",
  ""address"": {
    ""street"": ""eenlangestraat"",
    ""houseNumber"": ""48"",
    ""box"": ""3"",
    ""zipCode"": ""3150"",
    ""city"": ""Haacht"",
    ""country"": ""Belgium""
  },
  ""sourced"": ""differentFile""
}
";

        var content = new StringContent(json, Encoding.UTF8, "application/json");
        var putResponse = await HttpClient.PutAsync($"/api/v1/Affiliate/{affiliate.Id}", content);
        Assert.True(putResponse.IsSuccessStatusCode);

        var getRequestAfterUpdate = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/Affiliate/{affiliate.Id}");
        var getResponseAfterUpdate = await HttpClient.SendAsync(getRequestAfterUpdate);
        Assert.True(putResponse.IsSuccessStatusCode);
        var affiliateAfterUpdate = JsonConvert.DeserializeObject<AffiliateDtoForTesting>(getResponseAfterUpdate.Content.ReadAsStringAsync().Result);
        Assert.NotNull(affiliateAfterUpdate);
        Assert.AreEqual("stuyckens", affiliateAfterUpdate.LastName);

        await AssertHistory(affiliate.Id, 2);

        // functionally the same data should not add new history (only change sourced)
        var put2Response = await HttpClient
                               .PutAsync(
                                   $"/api/v1/Affiliate/{affiliate.Id}",
                                   new StringContent(
                                       json.Replace("differentFile", "yetAnotherFile"),
                                       Encoding.UTF8,
                                       "application/json"));
        Assert.True(put2Response.IsSuccessStatusCode);
        await AssertHistory(affiliate.Id, 2);

        await AssertSources(affiliate.Id, dateBeforeUpdate, new List<string> { "neFile01" });
        await AssertSources(affiliate.Id, DateTime.UtcNow, new List<string> { "differentFile", "yetAnotherFile" });
    }

    private async Task AssertHistory(string affiliateId, int historyCount)
    {
        var historyRequest = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/Affiliate/{affiliateId}/history");
        var historyResponseAfterUpdate1 = await HttpClient.SendAsync(historyRequest);
        Assert.True(historyResponseAfterUpdate1.IsSuccessStatusCode);
        var history = JsonConvert.DeserializeObject<HistoryDtoForTesting>(historyResponseAfterUpdate1.Content.ReadAsStringAsync().Result);
        Assert.NotNull(history);
        Assert.AreEqual(historyCount, history.Items.Count);
    }

    private async Task AssertSources(string affiliateId, DateTime date, List<string> expectedSources)
    {
        var at = date.ToString("O");
        var sourceRequest = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/Affiliate/{affiliateId}/sources?at={at}");
        var sourceResponse = await HttpClient.SendAsync(sourceRequest);
        Assert.True(sourceResponse.IsSuccessStatusCode);
        var sources = JsonConvert.DeserializeObject<List<string>>(sourceResponse.Content.ReadAsStringAsync().Result);
        Assert.NotNull(sources);
        Assert.AreEqual(expectedSources.Count, sources.Count);
        Assert.That(sources, Is.EquivalentTo(expectedSources));
    }

    private async Task<AffiliateDtoForTesting> CreateAffiliate()
    {
        var json = @"
{
  ""firstName"": ""tim"",
  ""lastName"": ""stu"",
  ""inss"": """ + Guid.NewGuid() + @""",
  ""dateOfBirth"": ""2022-05-04"",
  ""mortalityTableGender"": ""Male"",
  ""language"": ""NlBe"",
  ""address"": {
    ""street"": ""eenlangestraat"",
    ""houseNumber"": ""48"",
    ""box"": ""3"",
    ""zipCode"": ""3150"",
    ""city"": ""Haacht"",
    ""country"": ""Belgium""
  },
  ""sourced"": ""neFile01""
}
";
        var content = new StringContent(json, Encoding.UTF8, "application/json");
        var response = await HttpClient.PostAsync("/api/v1/Affiliate", content);
        Assert.NotNull(response);
        var responseBody = await response.Content.ReadAsStringAsync();
        Console.Write($"Created affiliated {responseBody}");

        Assert.IsTrue(response.IsSuccessStatusCode);
        var affiliate = JsonConvert.DeserializeObject<AffiliateDtoForTesting>(responseBody);
        Assert.NotNull(affiliate);
        Assert.True(affiliate.FirstName == "tim");
        Assert.True(affiliate.LastName == "stu");
        Assert.True(affiliate.DateOfBirth == "2022-05-04"); // dateonly testing
        return affiliate;
    }

    public class AffiliateDtoForTesting
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string Inss { get; set; }
    }

    public class HistoryDtoForTesting
    {
        public List<string> Items { get; set; }
    }
}
