namespace Energix.Persons.Host.API.Tests;

public enum PostgressMode
{
    EmbeddedWindows = 1,
    WebApiConnection = 2,
    BitbucketPipelines = 3
}
