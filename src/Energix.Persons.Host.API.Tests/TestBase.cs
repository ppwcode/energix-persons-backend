﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using Castle.MicroKernel.Lifestyle;
using Castle.Windsor;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;
using PPWCode.Host.Core.Extensions;
using PPWCode.Vernacular.NHibernate.III;
namespace Energix.Persons.Host.API.Tests;

/// <summary>
/// Testbase for flexible setup: against infra (provide ApiBaseUrl) or in process with empty db (bitbucket postgress service or embedded postgress)
/// https://github.com/mysticmind/mysticmind-postgresembed
/// https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-6.0#basic-tests-with-the-default-webapplicationfactory
/// https://community.atlassian.com/t5/Bitbucket-questions/How-do-I-use-Postgres-in-Bitbucket-Pipelines/qaq-p/461910
/// </summary>
[TestFixture]
[Parallelizable(ParallelScope.Fixtures)]
public class TestBase
{
    protected PostgressMode PostgressMode { get; private set; }

    protected string ApiBaseUrl { get; private set; }

    public TestBase()
    {
        var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

        PostgressMode = Enum.Parse<PostgressMode>(config["PostgressMode"]);
        ApiBaseUrl = config["ApiBaseUrl"];
    }

    /// <summary>
    /// workaround for aggressive disposable warnings
    /// </summary>
    private class ToCleanup
    {
        public MysticMind.PostgresEmbed.PgServer PgServer { get; set; }
        public IWindsorContainer Container { get; set; }
    }

    private ToCleanup _toCleanup = new ToCleanup();

    public HttpClient HttpClient { get; private set; }

    [OneTimeSetUp]
    public void Init()
    {
        var connectionString = GetConnectionString(out var customConnectionstring);
        IWindsorContainer container = null;
        HttpClient = string.IsNullOrWhiteSpace(ApiBaseUrl)
                         ? CreateInMemoryHost(connectionString, out container)
                         : new HttpClient() { BaseAddress = new Uri(ApiBaseUrl) };

        _toCleanup.Container = container;
        HttpClient.DefaultRequestHeaders.Add("x-flow-id", $"{Guid.NewGuid()}");
        HttpClient.DefaultRequestHeaders.Add("x-mode", $"automated-test-{Guid.NewGuid()}");

        // Init schema for empty databases
        if ((PostgressMode == PostgressMode.BitbucketPipelines) || (PostgressMode == PostgressMode.EmbeddedWindows))
        {
            InitDatabaseWithSchema(container);
        }
    }

    private static HttpClient CreateInMemoryHost(string connectionString, out IWindsorContainer container)
    {
        IWindsorContainer timingIssueContainer = null;
        var application = new WebApplicationFactory<Energix.Persons.Hosts.API.Startup>()
            .WithWebHostBuilder(
                builder =>
                {
                    builder.ConfigureAppConfiguration(
                        (context, configBuilder) =>
                        {
                            // override default connectionstring if provided
                            if (!string.IsNullOrWhiteSpace(connectionString))
                            {
                                configBuilder.AddInMemoryCollection(
                                    new Dictionary<string, string>
                                    {
                                        ["ConnectionStrings:Energix.Persons"] = connectionString
                                    });
                            }
                        });

                    builder.ConfigureTestServices(
                        services => { timingIssueContainer = ServiceCollectionExtensions.GetSingletonService<IWindsorContainer>(services); });
                });

        var httpClient = application.CreateClient();
        container = timingIssueContainer;
        return httpClient;
    }

    private string GetConnectionString(out bool customConnectionString)
    {
        var connectionString = string.Empty;
        if (PostgressMode == PostgressMode.BitbucketPipelines)
        {
            connectionString = $"Host=localhost;Port=5432 ;Username=test_user;Password=test_user_password;Database=pipelines;";
            customConnectionString = true;
        }

        if (PostgressMode == PostgressMode.EmbeddedWindows)
        {
            var pgPort = 1000;
            _toCleanup.PgServer = new MysticMind.PostgresEmbed.PgServer("9.5.5.1", port: pgPort, addLocalUserAccessPermission: true);

            // start the postgress server embedded
            _toCleanup.PgServer.Start();
            connectionString = $"Host=localhost;Port={pgPort};Username=postgres;Password=test;Database=postgres;";
            customConnectionString = true;
        }

        customConnectionString = false;
        return connectionString;
    }

    private void InitDatabaseWithSchema(IWindsorContainer container)
    {
        using (var scope = container.BeginScope())
        {
            var nhSession = container.Resolve<ISession>();
            try
            {
                var cmdSchema1 = nhSession.CreateSQLQuery("drop schema dbo cascade");
                cmdSchema1.ExecuteUpdate();
            }
            catch (Exception)
            {
            }

            try
            {
                var cmdSchema2 = nhSession.CreateSQLQuery("create schema dbo");
                cmdSchema2.ExecuteUpdate();
            }
            catch (Exception)
            {
            }

            ExecuteSchemaSql(container);
        }
    }

    [OneTimeTearDown]
    public void Cleanup()
    {
        if (_toCleanup.PgServer != null)
        {
            using (_toCleanup.PgServer)
            {
            }
        }

        if (_toCleanup.Container != null)
        {
            using (_toCleanup.Container)
            {
            }
        }
    }

    private void ExecuteSchemaSql(IWindsorContainer windsorContainer)
    {
        var config = windsorContainer.Resolve<INhConfiguration>();
        SchemaExport schemaExport = new SchemaExport(config.GetConfiguration());
        var stringBuilder = new StringBuilder();
        schemaExport.Create(x => stringBuilder.Append(x), true);
        var sql = stringBuilder.ToString();
        Debug.WriteLine(sql);
    }
}
