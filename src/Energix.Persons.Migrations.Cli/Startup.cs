﻿using System;
using System.IO;
using System.Linq;
using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel;
using Castle.MicroKernel.ModelBuilder.Inspectors;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Conversion;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using Energix.Persons.Migrations.Cli.Bootstrap;
using Energix.Persons.Migrations.Components;
using Energix.Persons.Migrations.Default;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.BatchParser;
using FluentMigrator.Runner.Generators.SqlServer;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PPWCode.Host.Core;
using PPWCode.Host.Core.Extensions;

namespace Energix.Persons.Migrations.Cli
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services, ProgramArguments programArguments)
        {
            IConfigurationRoot configuration =
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

            IConfigurationSection coreSection = configuration.GetSection("Core");
            CoreOptions coreOptions = new CoreOptions();
            coreSection.Bind(coreOptions);
            coreOptions.ThrowIfNotCivilized();

            // Setup our Windsor container
            IWindsorContainer container = services.CreatePPWContainer();

            // Disable Castle-Windsor property injection because FluentMigrator uses ServiceCollection that does not support property injection
            // When property injection is enabled we get a CircularDependencyException between IVersionLoader and IMigrationRunner
            // see also: https://github.com/castleproject/Windsor/blob/master/docs/how-properties-are-injected.md
            PropertiesDependenciesModelInspector propertiesDependenciesModelInspector = container
                .Kernel
                .ComponentModelBuilder
                .Contributors
                .OfType<PropertiesDependenciesModelInspector>()
                .Single();

            container.Kernel.ComponentModelBuilder.RemoveContributor(propertiesDependenciesModelInspector);

            string connectionString = configuration.GetConnectionString("Energix_Persons");
            Type initialMigrationClass = typeof(M0010_InitialDatabase);
            IArguments arguments = new Arguments(programArguments);

            // Configure the FluentMigrator
            services
                .AddFluentMigratorCore()
                .ConfigureRunner(
                    builder =>
                    {
                        builder.Services.TryAddTransient<SqlServerBatchParser>();
                        builder.Services.TryAddScoped<SqlServer2008Quoter>();
                        builder.Services
                            .AddScoped<SqlProcessor>()
                            .AddScoped<IMigrationProcessor>(sp => sp.GetRequiredService<SqlProcessor>())
                            .AddScoped<SqlGenerator>()
                            .AddScoped<IMigrationGenerator>(sp => sp.GetRequiredService<SqlGenerator>());

                        builder
                            .WithGlobalConnectionString(connectionString)
                            .WithGlobalCommandTimeout(TimeSpan.FromMinutes(10))
                            .ScanIn(initialMigrationClass.Assembly)
                            .For.Migrations()
                            .For.EmbeddedResources();
                    })
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .Configure<FluentMigratorLoggerOptions>(
                    cfg =>
                    {
                        cfg.ShowElapsedTime = true;
                        cfg.ShowSql = arguments.ShowSql;
                    })
                .Configure<RunnerOptions>(
                    cfg => { cfg.Tags = coreOptions.TagValues; })
                .Configure<TypeFilterOptions>(
                    cfg => { cfg.Namespace = initialMigrationClass.Namespace; })
                .Configure<ProcessorOptions>(
                    cfg => { cfg.PreviewOnly = arguments.PreviewOnly; });
        }

        public void ConfigureContainer([NotNull] IWindsorContainer container, [NotNull] ProgramArguments programArguments)
        {
            container
                .Kernel
                .Resolver
                .AddSubResolver(new CollectionResolver(container.Kernel, true));

            container
                .AddFacility<TypedFactoryFacility>()
                .AddFacility<LoggingFacility>(
                    facility =>
                        facility
                            .LogUsing<ExtendedLog4netFactory>()
                            .WithConfig("log4net.config"));

            container.Kernel.Register(Component.For<ProgramArguments>().Instance(programArguments));
            IWindsorInstaller[] installers = WindsorContainerUtilities.GetAssemblies("Energix.Persons.");
            container.Install(installers);

            IHandlerSelector[] handlerSelectors = container.ResolveAll<IHandlerSelector>();
            foreach (IHandlerSelector handlerSelector in handlerSelectors)
            {
                container
                    .Kernel
                    .AddHandlerSelector(handlerSelector);
            }

            ITypeConverter[] typeConverters = container.ResolveAll<ITypeConverter>();
            IConversionManager conversionManager = container.Kernel.GetConversionManager();
            foreach (ITypeConverter converter in typeConverters)
            {
                conversionManager.Add(converter);
                container.Release(converter);
            }
        }
    }
}
