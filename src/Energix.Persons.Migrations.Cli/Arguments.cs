using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli
{
    public class Arguments : IArguments
    {
        public Arguments([NotNull] ProgramArguments programArguments)
        {
            Version = programArguments.Version == 0 ? (long?)null : programArguments.Version;
            ShowSql = programArguments.ShowSql;
            PreviewOnly = programArguments.PreviewOnly;
        }

        /// <inheritdoc />
        public long? Version { get; }

        /// <inheritdoc />
        public bool PreviewOnly { get; }

        /// <inheritdoc />
        public bool ShowSql { get; }
    }
}
