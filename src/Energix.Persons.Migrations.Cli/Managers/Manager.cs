using Castle.Core.Logging;

using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Managers
{
    public abstract class Manager
    {
        private ILogger _logger = NullLogger.Instance;

        [UsedImplicitly]
        public ILogger Logger
        {
            get => _logger;
            set
            {
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                if (value != null)
                {
                    _logger = value;
                }
            }
        }
    }
}
