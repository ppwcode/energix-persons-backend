using System;
using Energix.Persons.Migrations.Components;
using FluentMigrator.Runner;
using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Managers
{
#pragma warning disable CA1063
    [UsedImplicitly]
    public class MigrationManager
        : Manager,
          IMigrationManager,
          IDisposable
    {
        private readonly CustomMigrationRunner _migrationRunner;

        public MigrationManager(
            [NotNull] IMigrationRunner migrationRunner,
            [NotNull] IVersionLoader versionLoader)
        {
            _migrationRunner = new CustomMigrationRunner(migrationRunner, versionLoader);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _migrationRunner?.Dispose();
        }

        /// <inheritdoc />
        public void ListMigrations()
        {
            _migrationRunner.ListMigrations();
        }

        /// <inheritdoc />
        public void UpgradeToLatest()
        {
            _migrationRunner.UpgradeToLatest();
        }

        /// <inheritdoc />
        public void RollbackToPointZero()
        {
            _migrationRunner.RollbackToPointZero();
        }

        /// <inheritdoc />
        public void UpgradeToVersion(long version)
        {
            _migrationRunner.UpgradeToVersion(version);
        }

        /// <inheritdoc />
        public void DowngradeToVersion(long version)
        {
            _migrationRunner.DowngradeToVersion(version);
        }

        /// <inheritdoc />
        public void ForceRegisterVersion(long version)
        {
            _migrationRunner.ForceRegisterVersion(version);
        }
    }
}
