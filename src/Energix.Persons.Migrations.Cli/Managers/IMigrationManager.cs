namespace Energix.Persons.Migrations.Cli.Managers
{
    public interface IMigrationManager : IManager
    {
        void ListMigrations();

        void UpgradeToLatest();

        void RollbackToPointZero();

        void UpgradeToVersion(long version);

        void DowngradeToVersion(long version);

        void ForceRegisterVersion(long version);
    }
}
