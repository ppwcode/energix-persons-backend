using Castle.Core.Logging;

using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Managers
{
    public interface IManager
    {
        [NotNull]
        ILogger Logger { get; }
    }
}
