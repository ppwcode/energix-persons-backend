namespace Energix.Persons.Migrations.Cli
{
    public interface IArguments
    {
        long? Version { get; }

        bool PreviewOnly { get; }

        bool ShowSql { get; }
    }
}
