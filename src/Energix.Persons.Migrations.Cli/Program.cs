using System;
using Castle.Core.Logging;
using Castle.Windsor;
using Energix.Persons.Migrations.Cli.Actions;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using PPWCode.Host.Core;

namespace Energix.Persons.Migrations.Cli
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            ProgramArguments programArguments = new ProgramArguments();
            if (Parser.ParseArgumentsWithUsage(args, programArguments))
            {
                ActionType actionType =
                    (ActionType)Enum.Parse(typeof(ActionType), programArguments.Action.ToUpperInvariant());
                switch (actionType)
                {
                    case ActionType.REQUESTED:
                    case ActionType.LIST:
                    case ActionType.UP:
                    case ActionType.DOWN:
                    case ActionType.REGISTER:
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(nameof(Action));
                }

                IWindsorContainer container = CreateWindsorContainer(programArguments);
                ILogger logger = container.Resolve<ILogger>();
                try
                {
                    ExecuteRequestedAction(container, actionType);
                }
                catch (Exception e)
                {
                    logger.Fatal(e.Message, e);
                }
                finally
                {
                    logger.Info("Closing and going home ...");
                    container.Dispose();
                }
            }
        }

        private static void ExecuteRequestedAction(IWindsorContainer container, ActionType actionType)
        {
            IActionFactory actionFactory = container.Resolve<IActionFactory>();
            try
            {
                IAction action = actionFactory.CreateRequestedAction(actionType);
                try
                {
                    action.Execute();
                }
                finally
                {
                    actionFactory.Release(action);
                }
            }
            finally
            {
                container.Kernel.ReleaseComponent(actionFactory);
            }
        }

        [NotNull]
        private static IWindsorContainer CreateWindsorContainer(ProgramArguments programArguments)
        {
            PPWWindsorServiceProviderFactory spf = new PPWWindsorServiceProviderFactory(null);
            IServiceCollection serviceCollection = new ServiceCollection();
            Startup startup = new Startup();
            startup.ConfigureServices(serviceCollection, programArguments);

            IWindsorContainer container = spf.CreateBuilder(serviceCollection);
            startup.ConfigureContainer(container, programArguments);
            spf.CreateServiceProvider(container);

            return container;
        }
    }
}
