using Energix.Persons.Migrations.Cli.Managers;
using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Actions
{
    [Action(ActionType.UP)]
    [UsedImplicitly]
    public class MigrateUpAction : BaseAction
    {
        public MigrateUpAction(
            [NotNull] IArguments arguments,
            [NotNull] IMigrationManager migrationManager)
        {
            Arguments = arguments;
            MigrationManager = migrationManager;
        }

        [NotNull]
        public IArguments Arguments { get; }

        [NotNull]
        public IMigrationManager MigrationManager { get; }

        /// <inheritdoc />
        public override void Execute()
        {
            if (Arguments.Version == null)
            {
                Logger.Info($"Executing {ActionType.UP} command with version (latest)");
                MigrationManager.UpgradeToLatest();
            }
            else
            {
                Logger.Info($"Executing {ActionType.UP} command with version {Arguments.Version}");
                MigrationManager.UpgradeToVersion(Arguments.Version.Value);
            }
        }
    }
}
