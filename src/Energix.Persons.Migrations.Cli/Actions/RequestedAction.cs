﻿using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Actions
{
    [Action(ActionType.REQUESTED)]
    [UsedImplicitly]
    public class RequestedAction : BaseAction
    {
        public RequestedAction(
            ActionType actionType,
            [NotNull] IActionFactory actionFactory)
        {
            ActionType = actionType;
            ActionFactory = actionFactory;
        }

        [NotNull]
        public IActionFactory ActionFactory { get; }

        public ActionType ActionType { get; }

        public override void Execute()
        {
            IAction action = ActionFactory.Create(ActionType);
            try
            {
                action.Execute();
            }
            finally
            {
                ActionFactory.Release(action);
            }
        }
    }
}
