using Energix.Persons.Migrations.Cli.Managers;
using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Actions
{
    [Action(ActionType.LIST)]
    [UsedImplicitly]
    public class ListAction : BaseAction
    {
        public ListAction(
            [NotNull] IMigrationManager migrationManager)
        {
            MigrationManager = migrationManager;
        }

        [NotNull]
        public IMigrationManager MigrationManager { get; }

        /// <inheritdoc />
        public override void Execute()
        {
            Logger.Info($"Executing {ActionType.LIST} command");
            MigrationManager.ListMigrations();
        }
    }
}
