﻿using Castle.Core.Logging;

using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Actions
{
    public abstract class BaseAction
        : IAction
    {
        [UsedImplicitly]
        [NotNull]
        public ILogger Logger { get; set; } = new NullLogger();

        public abstract void Execute();
    }
}
