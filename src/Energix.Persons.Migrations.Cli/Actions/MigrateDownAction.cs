using Energix.Persons.Migrations.Cli.Managers;
using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Actions
{
    [Action(ActionType.DOWN)]
    [UsedImplicitly]
    public class MigrateDownAction : BaseAction
    {
        public MigrateDownAction(
            [NotNull] IArguments arguments,
            [NotNull] IMigrationManager migrationManager)
        {
            Arguments = arguments;
            MigrationManager = migrationManager;
        }

        [NotNull]
        public IArguments Arguments { get; }

        [NotNull]
        public IMigrationManager MigrationManager { get; }

        /// <inheritdoc />
        public override void Execute()
        {
            if (Arguments.Version == null)
            {
                Logger.Info($"Executing {ActionType.DOWN} command with version (point zero)");
                MigrationManager.RollbackToPointZero();
            }
            else
            {
                Logger.Info($"Executing {ActionType.DOWN} command with version {Arguments.Version}");
                MigrationManager.DowngradeToVersion(Arguments.Version.Value);
            }
        }
    }
}
