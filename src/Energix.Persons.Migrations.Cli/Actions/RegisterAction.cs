using Energix.Persons.Migrations.Cli.Managers;
using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Actions
{
    [Action(ActionType.REGISTER)]
    [UsedImplicitly]
    public class RegisterAction : BaseAction
    {
        public RegisterAction(
            [NotNull] IArguments arguments,
            [NotNull] IMigrationManager migrationManager)
        {
            Arguments = arguments;
            MigrationManager = migrationManager;
        }

        [NotNull]
        public IArguments Arguments { get; }

        [NotNull]
        public IMigrationManager MigrationManager { get; }

        /// <inheritdoc />
        public override void Execute()
        {
            if (Arguments.Version == null)
            {
                Logger.Warn($"No version given for the {ActionType.REGISTER} command. Aborting.");
                return;
            }

            Logger.Info($"Executing {ActionType.REGISTER} command with version {Arguments.Version}");
            MigrationManager.ForceRegisterVersion(Arguments.Version.Value);
        }
    }
}
