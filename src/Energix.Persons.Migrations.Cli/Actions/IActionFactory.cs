﻿using System;

using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Actions
{
    public interface IActionFactory : IDisposable
    {
        [NotNull]
        IAction CreateRequestedAction(ActionType actionType);

        [NotNull]
        IAction Create(ActionType actionType);

        void Release([NotNull] IAction action);
    }
}
