﻿using System;

namespace Energix.Persons.Migrations.Cli.Actions
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ActionAttribute : Attribute
    {
        public ActionAttribute(ActionType actionType)
        {
            ActionType = actionType;
        }

        public ActionType ActionType { get; }
    }
}
