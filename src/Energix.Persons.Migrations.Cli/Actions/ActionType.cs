﻿using System;

namespace Energix.Persons.Migrations.Cli.Actions
{
    [Flags]
    public enum ActionType
    {
        REQUESTED = 0x0001,
        LIST = 0x0002,
        UP = 0x0004,
        DOWN = 0x0008,
        REGISTER = 0x0010
    }
}
