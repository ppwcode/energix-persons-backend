﻿namespace Energix.Persons.Migrations.Cli.Actions
{
    public interface IAction
    {
        void Execute();
    }
}
