﻿using System;
using System.Reflection;

using Castle.Facilities.TypedFactory;

using Energix.Persons.Migrations.Cli.Actions;

namespace Energix.Persons.Migrations.Cli.Bootstrap.Selectors
{
    public class ActionSelector : DefaultTypedFactoryComponentSelector
    {
        protected override string GetComponentName(MethodInfo method, object[] arguments)
        {
            if ((method.Name == nameof(IActionFactory.CreateRequestedAction)) && (arguments.Length > 0))
            {
                return $"{typeof(ActionType).FullName}_{ActionType.REQUESTED}";
            }

            if (method.Name == nameof(IActionFactory.Create))
            {
                ActionType argument = (ActionType)arguments[0];
                return $"{typeof(ActionType).FullName}_{argument}";
            }

            throw new InvalidOperationException("Unable to determine the component name.");
        }
    }
}
