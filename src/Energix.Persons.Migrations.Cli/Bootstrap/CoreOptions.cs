using System.Linq;

using JetBrains.Annotations;

using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Migrations.Cli.Bootstrap
{
    public class CoreOptions : CivilizedObject
    {
        [UsedImplicitly]
        public string Tags { get; set; }

        [UsedImplicitly]
        public string[] TagValues
            => string.IsNullOrWhiteSpace(Tags)
                   ? new string[0]
                   : Tags.Split().Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
    }
}
