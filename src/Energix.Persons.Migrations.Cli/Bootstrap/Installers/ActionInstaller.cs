﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Castle.Core.Internal;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Energix.Persons.Migrations.Cli.Actions;
using Energix.Persons.Migrations.Cli.Bootstrap.Selectors;
using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Bootstrap.Installers
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class ActionInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Assembly thisAssembly = typeof(ActionInstaller).Assembly;

            container.Register(
                Classes
                    .FromAssembly(thisAssembly)
                    .BasedOn<IAction>()
                    .ConfigureIf(
                        r => Attribute.IsDefined(r.Implementation, typeof(ActionAttribute)),
                        c => c.Named($"{typeof(ActionType).FullName}_{c.Implementation.GetAttribute<ActionAttribute>().ActionType}").LifestyleTransient()),
                Component
                    .For<ActionSelector>()
                    .LifestyleSingleton(),
                Component
                    .For<IActionFactory>()
                    .AsFactory(c => c.SelectedWith<ActionSelector>())
                    .LifestyleSingleton());
        }
    }
}
