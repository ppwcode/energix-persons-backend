﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Energix.Persons.Migrations.Cli.Managers;
using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Cli.Bootstrap.Installers
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class ManagerInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Assembly thisAssembly = typeof(ManagerInstaller).Assembly;

            container
                .Register(
                    Classes
                        .FromAssembly(thisAssembly)
                        .BasedOn(typeof(IManager))
                        .WithService.AllInterfaces()
                        .LifestyleSingleton());
        }
    }
}
