﻿using System.Diagnostics.CodeAnalysis;

namespace Energix.Persons.Migrations.Cli
{
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "This how the library works.")]
    public class ProgramArguments
    {
        [Argument(
            ArgumentType.AtMostOnce,
            DefaultValue = "LIST",
            HelpText = "Actions: LIST (list migrations), UP (upgrade database), DOWN (downgrade database),"
                       + " REGISTER (register a migration as executed without executing it)",
            LongName = "action",
            ShortName = "a")]
        public string Action;

        [Argument(
            ArgumentType.AtMostOnce,
            DefaultValue = 0,
            HelpText = "Database version to migrate to, used for actions UP and DOWN.  Note that version 0 is a special"
                       + " value: it will upgrade to the latest version or downgrade to point zero (empty database).",
            LongName = "version",
            ShortName = "v")]
        public int Version;

        [Argument(
            ArgumentType.AtMostOnce,
            DefaultValue = false,
            HelpText = "Show executed sql statements.",
            LongName = "sql",
            ShortName = "s")]
        public bool ShowSql;

        [Argument(
            ArgumentType.AtMostOnce,
            DefaultValue = false,
            HelpText = "Preview only: show what will be done, but do not execute",
            LongName = "preview",
            ShortName = "p")]
        public bool PreviewOnly;
    }
}
