﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;

using Energix.Persons.Server.Models.PPW;
using Energix.Persons.Server.Models.Tests.Common.Sut;

using NHibernate.Mapping.ByCode;

using NUnit.Framework;

using PPWCode.Vernacular.NHibernate.III;

using DatabaseManager = Energix.Persons.Hosts.API.Bootstrap.DatabaseManager;

namespace Energix.Persons.Server.Models.Tests.Bootstrap
{
    public class NHibernateMappingTests : SutTests<HbmMapping>
    {
        protected override IWindsorContainer OnCreateContainer
        {
            get
            {
                IWindsorContainer container = base.OnCreateContainer;

                container
                    .Register(
                        Component
                            .For<IMappingAssemblies>()
                            .ImplementedBy<MappingAssemblies>()
                            .LifestyleSingleton());

                container.Register(
                    Component
                        .For<DatabaseManager>()
                        .ImplementedBy<DatabaseManager>()
                        .LifestyleTransient());
                return container;
            }
        }

        [Test]
        public void XmlMapping()
        {
            NHibernate.Cfg.MappingSchema.HbmMapping hbmMapping = Sut.HbmMapping;
            Assert.IsNotEmpty(hbmMapping.AsString());
        }
    }
}
