﻿using System;
using System.Reflection;

using Castle.MicroKernel.Registration;
using Castle.Windsor;

using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models.PPW;
using Energix.Persons.Server.Models.Tests.Common.Sut;

using NHibernate;
using NHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;

using NUnit.Framework;

using PPWCode.Vernacular.NHibernate.III;
using PPWCode.Vernacular.NHibernate.III.CastleWindsor;
using PPWCode.Vernacular.Persistence.IV;

using Component = Castle.MicroKernel.Registration.Component;

namespace Energix.Persons.Server.Models.Tests.Bootstrap;

public class EnergixNhConfigurationTests : SutTests<EnergixNhConfiguration>
{
    protected override IWindsorContainer OnCreateContainer
    {
        get
        {
            var container = base.OnCreateContainer;

            container
                .Register(
                    Component
                        .For<IPpwHbmMapping>()
                        .ImplementedBy<HbmMapping>()
                        .LifestyleSingleton());

            container
                .Register(
                    Component
                        .For<IMappingAssemblies>()
                        .ImplementedBy<MappingAssemblies>()
                        .LifestyleSingleton());
            container
                .Register(
                    Component
                        .For<INhProperties>()
                        .ImplementedBy<NhProperties>()
                        .LifestyleSingleton());

            container
                .Register(
                    Component
                        .For<IIdentityProvider>()
                        .ImplementedBy<IdentityProvider>()
                        .LifestyleSingleton());
            container
                .Register(
                    Component
                        .For<ITimeProvider>()
                        .ImplementedBy<TimeProvider>()
                        .LifestyleSingleton());
            container
                .Register(
                    Component
                        .For<PPWCode.Vernacular.NHibernate.III.INhInterceptor>()
                        .ImplementedBy<TestInterceptor>()
                        .LifestyleSingleton());

            container
                .Register(
                    Component
                        .For<IRegisterEventListener>()
                        .ImplementedBy<CivilizedEventListener>()
                        .LifeStyle.Singleton);

            return container;
        }
    }

    [Test]
    public void ConfigurationExport()
    {
        SchemaExport schemaExport = new SchemaExport(Sut.GetConfiguration());
        schemaExport.Create(true, false);
        Assert.True(true);
    }

    private class TestInterceptor : PPWCode.Vernacular.NHibernate.III.INhInterceptor
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IIdentityProvider _identityProvider;
        private readonly ITimeProvider _timeProvider;

        public TestInterceptor(
            IServiceProvider serviceProvider,
            IIdentityProvider identityProvider,
            ITimeProvider timeProvider)
        {
            _serviceProvider = serviceProvider;
            _identityProvider = identityProvider;
            _timeProvider = timeProvider;
        }

        /// <inheritdoc />
        public IInterceptor GetInterceptor()
            => new AuditInterceptorWithDi(_serviceProvider, _identityProvider, _timeProvider);
    }
}
