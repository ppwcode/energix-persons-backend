using System;
using System.Collections;

using Energix.Persons.Server.Models.PPW.Components;
using Energix.Persons.Server.Models.Tests.Common;
using NUnit.Framework;

namespace Energix.Persons.Server.Models.Tests.Components
{
    public class IntervalTests : BaseTests
    {
        public static IEnumerable CivilizedCases
        {
            get
            {
                // Positive
                {
                    DateTime? start = new DateTime(2020, 5, 3);
                    DateTime? end = new DateTime(2020, 7, 4);
                    yield return
                        new TestCaseData(
                                new IntervalBuilder()
                                    .Start(start)
                                    .End(end)
                                    .Build())
                            .Returns(true)
                            .SetName("Civilized: Positive");
                }

                {
                    // Negative
                    DateTime end = new DateTime(2020, 7, 4);
                    yield return
                        new TestCaseData(
                                new IntervalBuilder()
                                    .Start(null)
                                    .End(end)
                                    .Build())
                            .Returns(false)
                            .SetName("Civilized: No start date");
                }

                {
                    DateTime? start = new DateTime(2020, 5, 4);
                    DateTime? end = new DateTime(2020, 2, 4);
                    Interval interval =
                        new IntervalBuilder()
                            .Start(start)
                            .End(end)
                            .Build();
                    yield return
                        new TestCaseData(interval)
                            .Returns(false)
                            .SetName("WildExceptions: End.Value <= Start");
                }
            }
        }

        public static IEnumerable MemberCasesPositive
        {
            get
            {
                {
                    DateTime? start = new DateTime(2020, 5, 3);
                    DateTime? end = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(start)
                        .End(end)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalA,
                                new Func<Interval, bool>(f => f.Equals(intervalA)))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}.{nameof(Interval.Equals)}()");
                }

                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 5, 3);
                    DateTime? endB = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f.Contains(intervalB)))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}.{nameof(Interval.Contains)}(IInterval interval)");
                }

                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalA,
                                new Func<Interval, bool>(f => (intervalA.Start != null) && f.Contains(intervalA.Start.Value)))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}.{nameof(Interval.Contains)}(DateTime dt)");
                }

                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalA,
                                new Func<Interval, bool>(f => (intervalA.Start != null) && f.Contains(intervalA.Start.Value)))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}.{nameof(Interval.Contains)}(DateTime dt)");
                }

                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 7, 3);
                    DateTime? endB = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f.IsClosed()))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}.{nameof(Interval.IsClosed)}()");
                }

                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 5, 3);
                    DateTime? endB = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f.Overlaps(intervalA)))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}.{nameof(Interval.Overlaps)}(intervalA), intervalA in intervalB");
                }

                {
                    DateTime? startA = new DateTime(2020, 4, 4);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 5, 3);
                    DateTime? endB = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f.Overlaps(intervalA)))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}.{nameof(Interval.Overlaps)}(intervalA), intervalA partially in intervalB");
                }

                {
                    DateTime? startA = new DateTime(2020, 4, 4);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 5, 3);
                    DateTime? endB = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f == intervalB))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}==intervalB, intervalB == intervalB");
                }

                {
                    DateTime? startA = new DateTime(2020, 4, 4);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 4, 4);
                    DateTime? endB = new DateTime(2020, 6, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f == intervalA))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}==intervalA, intervalB == intervalA");
                }

                {
                    DateTime? startA = new DateTime(2020, 4, 4);
                    DateTime? endA = new DateTime(2020, 6, 2);
                    DateTime? startB = new DateTime(2020, 5, 4);
                    DateTime? endB = new DateTime(2020, 7, 9);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f != intervalA))
                            .Returns(true)
                            .SetName($"Positive: {nameof(Interval)}!=intervalA, intervalB != intervalA");
                }
            }
        }

        public static IEnumerable MemberCasesNegative
        {
            get
            {
                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    DateTime? startB = new DateTime(2020, 4, 3);
                    DateTime? endB = new DateTime(2020, 6, 4);
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalA,
                                new Func<Interval, bool>(f => f.Equals(intervalB)))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}.{nameof(Interval.Equals)}()");
                }

                {
                    DateTime? startB = new DateTime(2020, 4, 3);
                    DateTime? endB = new DateTime(2020, 6, 4);
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                null,
                                intervalB,
                                new Func<Interval, bool>(f => f.Equals(null)))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}.{nameof(Interval.Equals)}(null)");
                }

                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 7, 3);
                    DateTime? endB = new DateTime(2020, 8, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f.Contains(intervalA)))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}.{nameof(Interval.Contains)}()");
                }

                {
                    DateTime? startB = new DateTime(2020, 5, 3);
                    DateTime? endB = new DateTime(2020, 7, 4);
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                null,
                                intervalB,
                                new Func<Interval, bool>(f => f.Contains(null)))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}.{nameof(Interval.Contains)}(null)");
                }

                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 7, 3);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(null)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f.IsClosed()))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}.{nameof(Interval.IsClosed)}()");
                }

                {
                    DateTime? startA = new DateTime(2020, 5, 3);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 7, 3);
                    DateTime? endB = new DateTime(2020, 7, 4);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f.Overlaps(null)))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}.{nameof(Interval.Overlaps)}(null)");
                }

                {
                    DateTime? startA = new DateTime(2020, 4, 4);
                    DateTime? endA = new DateTime(2020, 6, 4);
                    DateTime? startB = new DateTime(2020, 7, 3);
                    DateTime? endB = new DateTime(2020, 9, 8);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f == intervalA))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}==intervalA, intervalB == intervalA");
                }

                {
                    DateTime? startB = new DateTime(2020, 7, 3);
                    DateTime? endB = new DateTime(2020, 9, 8);
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                null,
                                intervalB,
                                new Func<Interval, bool>(f => f == null))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}==intervalA, intervalB == null");
                }

                {
                    DateTime? startB = new DateTime(2020, 7, 3);
                    DateTime? endB = new DateTime(2020, 9, 8);
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                null,
                                intervalB,
                                new Func<Interval, bool>(f => f != null))
                            .Returns(true)
                            .SetName($"Negative: {nameof(Interval)}==intervalA, intervalB != null");
                }

                {
                    DateTime? startA = new DateTime(2020, 4, 4);
                    DateTime? endA = new DateTime(2020, 6, 2);
                    DateTime? startB = new DateTime(2020, 5, 4);
                    DateTime? endB = new DateTime(2020, 7, 9);
                    Interval intervalA = new IntervalBuilder()
                        .Start(startA)
                        .End(endA)
                        .Build();
                    Interval intervalB = new IntervalBuilder()
                        .Start(startB)
                        .End(endB)
                        .Build();
                    yield return
                        new TestCaseData(
                                intervalA,
                                intervalB,
                                new Func<Interval, bool>(f => f != intervalB))
                            .Returns(false)
                            .SetName($"Negative: {nameof(Interval)}!=intervalA, intervalB != intervalB");
                }
            }
        }

        [Test]
        [TestCaseSource(nameof(CivilizedCases))]
        public bool Civilized_cases(Interval interval)
            => interval.IsCivilized;

        [Test]
        [TestCaseSource(nameof(MemberCasesNegative))]
        public bool Test_Members_Negative(Interval intervalA, Interval intervalB, Func<Interval, bool> lambda)
        {
            if (intervalA != null)
            {
                Assert.That(intervalA.IsCivilized, Is.EqualTo(true));
            }

            if (intervalB != null)
            {
                Assert.That(intervalB.IsCivilized, Is.EqualTo(true));
            }

            return lambda(intervalB);
        }

        [Test]
        [TestCaseSource(nameof(MemberCasesPositive))]
        public bool Test_Members_Positive(Interval intervalA, Interval intervalB, Func<Interval, bool> lambda)
        {
            Assert.That(intervalA.IsCivilized, Is.EqualTo(true));
            Assert.That(intervalB.IsCivilized, Is.EqualTo(true));
            return lambda(intervalB);
        }

        [Test]
        public void Test_uncivilized_interval_contains()
        {
            // Arrange
            Interval interval =
                new IntervalBuilder()
                    .Start(null)
                    .End(null);

            DateTime datetime =
                new DateTime(1890, 2, 3);

            // Act
            AssertInternalProgrammingErrorException(
                () => interval.Contains(interval),
                "Interval parameter: Validation of interval contains can only be done on Civilized objects");

            // Act
            AssertInternalProgrammingErrorException(
                () => interval.Contains(datetime),
                "DateTime parameter: Validation of interval contains can only be done on Civilized objects");
        }

        [Test]
        public void Test_uncivilized_interval_overlaps()
        {
            // Arrange
            Interval interval =
                new IntervalBuilder()
                    .Start(null)
                    .End(null);

            // Act
            AssertInternalProgrammingErrorException(
                () => interval.Overlaps(interval),
                "Validation of interval overlap can only be done on Civilized objects");
        }
    }
}
