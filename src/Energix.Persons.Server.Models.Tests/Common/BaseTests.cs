﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using JetBrains.Annotations;

using NUnit.Framework;

using PPWCode.API.Core.Exceptions;
using PPWCode.Vernacular.Exceptions.IV;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Models.Tests.Common
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public abstract class BaseTests
    {
        [SetUp]
        public void Setup()
        {
            OnSetup();
        }

        [TearDown]
        public void TearDown()
        {
            OnTearDown();
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            OnOneTimeSetup();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            OnOneTimeTearDown();
        }

        private static int _nextId = 1000000;

        protected static int NextId
            => Interlocked.Increment(ref _nextId);

        protected virtual void OnOneTimeTearDown()
        {
        }

        protected virtual void OnOneTimeSetup()
        {
        }

        protected virtual void OnSetup()
        {
        }

        protected virtual void OnTearDown()
        {
        }

        protected virtual void CheckCivilized(params ICivilizedObject[] civilizedObjects)
        {
            if (civilizedObjects != null)
            {
                CheckCivilized(civilizedObjects.AsEnumerable());
            }
        }

        protected virtual void CheckCivilized(
            [System.Diagnostics.CodeAnalysis.NotNull]
            IEnumerable<ICivilizedObject> civilizedObjects)
        {
            CompoundSemanticException cse = new CompoundSemanticException();
            foreach (ICivilizedObject civilizedObject in civilizedObjects.Where(co => !co.IsCivilized))
            {
                cse.AddElement(
                    new SemanticException(
                        $"Each test requires that objects of type, {civilizedObject.GetType().FullName}, are civilized.",
                        civilizedObject.WildExceptions()));
            }

            if (!cse.IsEmpty)
            {
                throw cse;
            }
        }

        protected void AssertInternalProgrammingErrorExceptionAsync([NotNull] AsyncTestDelegate lambda, [CanBeNull] string message, params object[] parmas)
            => Assert.ThrowsAsync<InternalProgrammingError>(lambda, message, parmas);

        protected void AssertInternalProgrammingErrorException([NotNull] TestDelegate lambda, [CanBeNull] string message, params object[] parmas)
            => Assert.Throws<InternalProgrammingError>(lambda, message, parmas);

        protected virtual DateTime GetUtcTime(
            int year,
            int month,
            int day,
            int hour,
            int minute,
            int second)
            => new DateTime(
                year,
                month,
                day,
                hour,
                minute,
                second,
                DateTimeKind.Utc);

        protected virtual DateTime GetUtcDate(int year, int month, int day)
            => GetUtcTime(
                year,
                month,
                day,
                0,
                0,
                0);
    }
}
