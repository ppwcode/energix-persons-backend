﻿using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;

using Moq;

namespace Energix.Persons.Server.Models.Tests.Common
{
    public abstract class AbstractTestFacility : AbstractFacility
    {
        public MockBehavior? MockBehavior { get; set; }

        protected override void Init()
        {
            Kernel.Resolver.AddSubResolver(new CollectionResolver(Kernel, true));
            Kernel
                .Register(
                    Component
                        .For(typeof(Mock<>))
                        .DependsOn(Dependency.OnValue<MockBehavior>(MockBehavior))
                        .LifestyleSingleton());
        }
    }
}
