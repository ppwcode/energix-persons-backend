﻿using Castle.MicroKernel;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Castle.Windsor.Proxy;

using Moq;

using PPWCode.Host.Core;

namespace Energix.Persons.Server.Models.Tests.Common.Sut
{
    /// <summary>
    ///     Based on <see href="http://blog.ploeh.dk/2013/03/11/auto-mocking-container/" />
    /// </summary>
    /// <typeparam name="T">The type of the system under test</typeparam>
    public abstract class SutTests<T> : BaseTests
        where T : class
    {
        private T _sut;

        protected T Sut
        {
            get
            {
                if ((_sut == null) && (Container != null))
                {
                    _sut = (T)Container.Resolve(typeof(T));
                }

                return _sut;
            }
        }

        protected override void OnSetup()
        {
            Container = OnCreateContainer;
            OnAfterCreateContainer(Container);
        }

        protected virtual IWindsorContainer OnCreateContainer
            => new WindsorContainer(
                    new DefaultKernel(
                        new InlineDependenciesPropagatingDependencyResolver(),
                        new DefaultProxyFactory()),
                    new DefaultComponentInstaller())
                .AddFacility<SutTestFacility>(
                    f =>
                    {
                        f.SutType = typeof(T);
                        f.MockBehavior = MockBehavior.Strict;
                    });

        protected virtual void OnAfterCreateContainer(IWindsorContainer container)
        {
            OnRegisterMocks(container);
        }

        protected virtual void OnRegisterMocks(IWindsorContainer container)
        {
            // setup service mocks
            OnSetupMocks(container);
        }

        protected virtual void OnSetupMocks(IWindsorContainer container)
        {
        }

        protected override void OnTearDown()
        {
            if (_sut != null)
            {
                Container.Release(_sut);
                _sut = null;
            }

            if (Container != null)
            {
                Container.Dispose();
                Container = null;
            }
        }

        public IWindsorContainer Container { get; private set; }
    }
}
