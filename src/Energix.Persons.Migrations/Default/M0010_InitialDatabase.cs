using System.Diagnostics.CodeAnalysis;

using FluentMigrator;
using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Default
{
    [UsedImplicitly]
    [Migration(10, "Initial database, can be dropped an recreated in the beginning of project for dev speed")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "valid")]
    public class M0010_InitialDatabase : Migration
    {
        public override void Up()
        {
            // DB is created by NH domain model (see CanCreateDatabase in appsettings.development.json)
            // some demo data is in this script
            Execute.EmbeddedScript("M0010_CreateDatabase.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("M0010_TearDownDatabase.sql");
        }
    }
}
