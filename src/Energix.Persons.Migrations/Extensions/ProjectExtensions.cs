using Energix.Persons.Migrations.Configuration;
using FluentMigrator.Builders.Alter.Table;
using FluentMigrator.Builders.Create.Table;

namespace Energix.Persons.Migrations.Extensions
{
    public static class ProjectExtensions
    {
        public static ICreateTableColumnOptionOrWithColumnSyntax WithInsertAuditColumns(
            this ICreateTableWithColumnSyntax rootSyntax)
            => rootSyntax
                .WithColumn(Columns.CreatedAt).AsDateTime2().NotNullable()
                .WithColumn(Columns.CreatedBy).AsString(128).NotNullable();

        public static IAlterTableColumnOptionOrAddColumnOrAlterColumnSyntax AddInsertAuditColumns(
            this IAlterTableAddColumnOrAlterColumnSyntax rootSyntax)
            => rootSyntax
                .AddColumn(Columns.CreatedAt).AsDateTime2().NotNullable()
                .AddColumn(Columns.CreatedBy).AsString(128).NotNullable();

        public static ICreateTableColumnOptionOrWithColumnSyntax WithAuditColumns(
            this ICreateTableWithColumnSyntax rootSyntax)
            => rootSyntax
                .WithInsertAuditColumns()
                .WithColumn(Columns.LastModifiedAt).AsDateTime2().Nullable()
                .WithColumn(Columns.LastModifiedBy).AsString(128).Nullable();

        public static IAlterTableColumnOptionOrAddColumnOrAlterColumnSyntax AddAuditColumns(
            this IAlterTableAddColumnOrAlterColumnSyntax rootsSyntax)
            => rootsSyntax
                .AddInsertAuditColumns()
                .AddColumn(Columns.LastModifiedAt).AsDateTime2().Nullable()
                .AddColumn(Columns.LastModifiedBy).AsString(128).Nullable();
    }
}
