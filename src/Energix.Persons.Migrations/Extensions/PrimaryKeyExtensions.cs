using FluentMigrator.Builders.Alter.Table;
using FluentMigrator.Builders.Create.Table;

namespace Energix.Persons.Migrations.Extensions
{
    public static class PrimaryKeyExtensions
    {
        public static ICreateTableWithColumnSyntax WithPrimaryKeyColumn(
            this ICreateTableWithColumnSyntax rootSyntax,
            string thisTableName)
            => rootSyntax
                .WithColumn($"{thisTableName}Id")
                .AsInt64()
                .NotNullable()
                .PrimaryKey($"PK_{thisTableName}Id");

        public static IAlterTableColumnOptionOrAddColumnOrAlterColumnSyntax AddPrimaryKeyColumn(
            this IAlterTableAddColumnOrAlterColumnSyntax rootSyntax,
            string thisTableName)
            => rootSyntax
                .AddColumn($"{thisTableName}Id")
                .AsInt64()
                .NotNullable()
                .PrimaryKey($"PK_{thisTableName}Id");
    }
}
