namespace Energix.Persons.Migrations.Configuration
{
    public static class Tables
    {
        /*
         * Static class with constants for the Table names.
         */

        public const string Affiliate = nameof(Affiliate);
        public const string AffiliateProperties = nameof(AffiliateProperties);
    }
}
