namespace Energix.Persons.Migrations.Configuration
{
    public static class Columns
    {
        /*
         * Static class for the column names.
         */

        // InsertAuditable
        public const string CreatedAt = nameof(CreatedAt);
        public const string CreatedBy = nameof(CreatedBy);

        // Auditable
        public const string LastModifiedAt = nameof(LastModifiedAt);
        public const string LastModifiedBy = nameof(LastModifiedBy);

        // Affiliates & related
        public const string Id = nameof(Id);
        public const string AffiliateId = nameof(AffiliateId);
    }
}
