using System.Diagnostics.CodeAnalysis;

namespace Energix.Persons.Migrations.Configuration
{
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Justified")]
    public static class Tags
    {
        // Tags to filter migrations based on the environment
        // - LOCAL: development machine, config Development
        // - INT: integration environment, config Integration
        // - TEST: test environment, config Staging
        // -PROD: production environment, config Production
        public const string LOCAL = nameof(LOCAL);
        public const string INT = nameof(INT);
        public const string TEST = nameof(TEST);
        public const string PROD = nameof(PROD);
    }
}
