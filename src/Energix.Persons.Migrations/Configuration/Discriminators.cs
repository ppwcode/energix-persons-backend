using System.Diagnostics.CodeAnalysis;

namespace Energix.Persons.Migrations.Configuration
{
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "valid")]
    public static class Discriminators
    {
        /*
         * Static class for the discriminator values.
         */

        // Discriminator column
        public const string Discriminator = nameof(Discriminator);
    }
}
