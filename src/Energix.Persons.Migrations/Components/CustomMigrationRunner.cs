using System;
using System.Collections.Generic;

using FluentMigrator.Infrastructure;
using FluentMigrator.Runner;

using JetBrains.Annotations;

namespace Energix.Persons.Migrations.Components
{
#pragma warning disable CA1063
    public class CustomMigrationRunner : IDisposable
    {
        public CustomMigrationRunner(
            [NotNull] IMigrationRunner migrationRunner,
            [NotNull] IVersionLoader versionLoader)
        {
            MigrationRunner = migrationRunner;
            VersionLoader = versionLoader;
        }

        [NotNull]
        public IMigrationRunner MigrationRunner { get; }

        [NotNull]
        public IVersionLoader VersionLoader { get; }

        public void Dispose()
        {
            // migration runner no longer in use, free associated resources
            // this will close the db connection
            MigrationRunner.Processor.Dispose();
        }

        public void ListMigrations()
        {
            MigrationRunner.ListMigrations();
        }

        public void UpgradeToLatest()
        {
            MigrationRunner.MigrateUp();
        }

        public void RollbackToPointZero()
        {
            MigrationRunner.MigrateDown(0);
        }

        public void UpgradeToVersion(long version)
        {
            MigrationRunner.MigrateUp(version);
        }

        public void DowngradeToVersion(long version)
        {
            MigrationRunner.MigrateDown(version);
        }

        public void ForceRegisterVersion(long version)
        {
            SortedList<long, IMigrationInfo> migrationsList = MigrationRunner.MigrationLoader.LoadMigrations();
            if (!migrationsList.ContainsKey(version))
            {
                Console.WriteLine($"Migration with version {version} is not known.");
                return;
            }

            IMigrationInfo migrationInfo = migrationsList[version];

            if (!VersionLoader.VersionInfo.HasAppliedMigration(version))
            {
                using (IMigrationScope scope = MigrationRunner.BeginScope())
                {
                    try
                    {
                        VersionLoader
                            .UpdateVersionInfo(migrationInfo.Version, migrationInfo.Description ?? migrationInfo.Migration.GetType().Name);
                        scope.Complete();
                    }
                    catch (Exception)
                    {
                        if (scope.IsActive)
                        {
                            scope.Cancel();
                        }

                        throw;
                    }
                }
            }
        }
    }
}
