using Energix.Persons.Migrations.Configuration;
using FluentMigrator;

namespace Energix.Persons.Migrations.Components
{
    /// <summary>
    ///     Class with helper methods to support dealing with ids assigned through
    ///     the hi-lo generator.
    /// </summary>
    public static class HiLoHelper
    {
        // NEVER CHANGE THIS - must be identical to nHibernate configuration
        public const string GeneratorTableName = "HIBERNATE_HI_LO";
        public const string GeneratorNextHiColumnName = "NEXT_HI";
        public const string GeneratorNextIdColumnName = "NEXT_ID";
        public const string GeneratorEntityNameColumnName = "ENTITY_NAME";
        public const string GeneratorTableNameColumnName = "TABLE_NAME";
        public const int GeneratorMaxLo = 999;
        public const string MigrationOwner = "FluentMigrator";

        /// <summary>
        ///     This must be run at the very beginning of a migration that contains inserts.
        /// </summary>
        /// <param name="migration">Current migration</param>
        public static void InitializeHiLo(this Migration migration)
        {
            migration
                .Execute
                .Sql(
                    $"update {Schemas.Dbo}.{GeneratorTableName}"
                    + $"   set {GeneratorNextIdColumnName} ="
                    + $"          case {GeneratorNextHiColumnName}"
                    + "             when 0 then 1"
                    + $"             else {GeneratorNextHiColumnName} * {GeneratorMaxLo + 1}"
                    + "          end"
                    + $" where {GeneratorNextIdColumnName} is null;");
        }

        /// <summary>
        ///     This must be run at the very end of a migration that contains inserts.
        /// </summary>
        /// <param name="migration">Current migration</param>
        public static void SynchronizeHiLo(this Migration migration)
        {
            migration
                .Execute
                .Sql(
                    $"update {Schemas.Dbo}.{GeneratorTableName}"
                    + $"   set {GeneratorNextHiColumnName} ="
                    + $"          case {GeneratorNextIdColumnName}"
                    + "             when 1 then 0"
                    + $"             else (({GeneratorNextIdColumnName} - 1) / {GeneratorMaxLo + 1}) + 1"
                    + "          end,"
                    + $"       {GeneratorNextIdColumnName} = null"
                    + $" where {GeneratorNextIdColumnName} is not null;");
        }

        /// <summary>
        ///     Generate raw sql for getting the Next available id (inline select).
        /// </summary>
        /// <param name="tableName">the table name (unquoted)</param>
        /// <returns>sql in the form of <see cref="RawSql" /></returns>
        public static RawSql GetNextId(string tableName)
            => RawSql.Insert(
                $"(select {GeneratorNextIdColumnName}"
                + $"  from {Schemas.Dbo}.{GeneratorTableName}"
                + $" where {GeneratorTableNameColumnName} = '{tableName}')");

        /// <summary>
        ///     Generate raw sql for getting the last assigned id (inline select).
        /// </summary>
        /// <param name="tableName">the table name (unquoted)</param>
        /// <returns>sql in the form of <see cref="RawSql" /></returns>
        public static RawSql LastAssignedId(string tableName)
            => RawSql.Insert(
                $"(select ({GeneratorNextIdColumnName} - 1)"
                + $"  from {Schemas.Dbo}.{GeneratorTableName}"
                + $" where {GeneratorTableNameColumnName} = '{tableName}')");

        /// <summary>
        ///     Generate raw sql for getting the <paramref name="index" /> last
        ///     assigned id (inline select).
        /// </summary>
        /// <param name="tableName">the table name (unquoted)</param>
        /// <param name="index">the n-th last assigned id</param>
        /// <returns>sql in the form of <see cref="RawSql" /></returns>
        public static RawSql LastAssignedId(string tableName, int index)
            => RawSql.Insert(
                $"(select ({GeneratorNextIdColumnName} - {index})"
                + $"  from {Schemas.Dbo}.{GeneratorTableName}"
                + $" where {GeneratorTableNameColumnName} = '{tableName}')");

        /// <summary>
        ///     Generate raw sql update statement for bumping the Next available id
        ///     (after assigning the id).
        /// </summary>
        /// <param name="tableName">the table name (unquoted)</param>
        /// <returns>sql in the form of <see cref="RawSql" /></returns>
        public static RawSql BumpNextId(string tableName)
            => RawSql.Insert(
                $"update {Schemas.Dbo}.{GeneratorTableName}"
                + $"   set {GeneratorNextIdColumnName} = {GeneratorNextIdColumnName} + 1"
                + $" where {GeneratorTableNameColumnName} = '{tableName}'");
    }
}
