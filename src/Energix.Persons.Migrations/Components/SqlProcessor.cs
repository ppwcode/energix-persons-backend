using System;
using System.Data.Common;

using FluentMigrator.Runner.Generators.SqlServer;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Energix.Persons.Migrations.Components
{
    public class SqlProcessor : SqlServer2016Processor
    {
        /// <inheritdoc cref="SqlServer2016Processor" />
        public SqlProcessor(
            ILogger<SqlProcessor> logger,
            SqlServer2008Quoter quoter,
            SqlGenerator generator,
            IOptionsSnapshot<ProcessorOptions> options,
            IConnectionStringAccessor connectionStringAccessor,
            IServiceProvider serviceProvider)
            : base(
                logger,
                quoter,
                generator,
                options,
                connectionStringAccessor,
                serviceProvider)
        {
        }

        /// <inheritdoc cref="SqlServer2016Processor" />
        protected SqlProcessor(
            DbProviderFactory factory,
            ILogger logger,
            SqlServer2008Quoter quoter,
            SqlGenerator generator,
            IOptionsSnapshot<ProcessorOptions> options,
            IConnectionStringAccessor connectionStringAccessor,
            IServiceProvider serviceProvider)
            : base(
                factory,
                logger,
                quoter,
                generator,
                options,
                connectionStringAccessor,
                serviceProvider)
        {
        }
    }
}
