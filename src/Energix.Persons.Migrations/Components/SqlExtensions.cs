using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;

using FluentMigrator.Builders.Insert;
using FluentMigrator.Infrastructure;

namespace Energix.Persons.Migrations.Components
{
    public static class SqlExtensions
    {
        public const string HiLoInsert = nameof(HiLoInsert);
        public const string Versioned = nameof(Versioned);
        public const string Auditable = nameof(Auditable);

        /// <summary>
        ///     Inserts data using the HiLo algorithm for primary key generation.
        /// </summary>
        /// <param name="expression">Current expression</param>
        /// <returns>New fluent expression</returns>
        public static IInsertDataSyntax WithHiLoInsert(this IInsertDataSyntax expression)
        {
            ISupportAdditionalFeatures castExpression = expression as ISupportAdditionalFeatures;
            if (castExpression == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(WithHiLoInsert)} must be called on an object that implements ISupportAdditionalFeatures.");
            }

            castExpression.AdditionalFeatures.Add(HiLoInsert, true);
            return expression;
        }

        /// <summary>
        ///     Initializes the 'PersistenceVersion' column correctly.
        /// </summary>
        /// <param name="expression">Current expression</param>
        /// <returns>New fluent expression</returns>
        public static IInsertDataSyntax WithVersioned(this IInsertDataSyntax expression)
        {
            ISupportAdditionalFeatures castExpression = expression as ISupportAdditionalFeatures;
            if (castExpression == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(WithVersioned)} must be called on an object that implements ISupportAdditionalFeatures.");
            }

            castExpression.AdditionalFeatures.Add(Versioned, true);
            return expression;
        }

        /// <summary>
        ///     Initializes the 'CreatedBy' and 'CreatedAt' columns.
        /// </summary>
        /// <param name="expression">Current expression</param>
        /// <returns>New fluent expression</returns>
        public static IInsertDataSyntax WithAuditable(this IInsertDataSyntax expression)
        {
            ISupportAdditionalFeatures castExpression = expression as ISupportAdditionalFeatures;
            if (castExpression == null)
            {
                throw new InvalidOperationException(
                    $"{nameof(WithAuditable)} must be called on an object that implements ISupportAdditionalFeatures.");
            }

            castExpression.AdditionalFeatures.Add(Auditable, true);
            return expression;
        }

        public static dynamic WithLastModified(this object obj)
        {
            dynamic expando = new ExpandoObject();
            IDictionary<string, object> result = expando as IDictionary<string, object>;

            if (obj != null)
            {
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(obj))
                {
                    result[property.Name] = property.GetValue(obj);
                }
            }

            result["LastModifiedBy"] = HiLoHelper.MigrationOwner;
            result["LastModifiedAt"] = DateTime.UtcNow;

            return result;
        }
    }
}
