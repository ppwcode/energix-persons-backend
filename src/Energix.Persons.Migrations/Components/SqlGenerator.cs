using System;
using System.Collections.Generic;

using FluentMigrator;
using FluentMigrator.Expressions;
using FluentMigrator.Model;
using FluentMigrator.Runner.Generators.SqlServer;

namespace Energix.Persons.Migrations.Components
{
    public class SqlGenerator : SqlServer2016Generator
    {
        private static bool IsUsingHiLoInsert(InsertDataExpression expression)
        {
            if (expression.AdditionalFeatures.ContainsKey(SqlExtensions.HiLoInsert))
            {
                return (bool)expression.AdditionalFeatures[SqlExtensions.HiLoInsert];
            }

            return false;
        }

        private static bool IsUsingVersioned(InsertDataExpression expression)
        {
            if (expression.AdditionalFeatures.ContainsKey(SqlExtensions.Versioned))
            {
                return (bool)expression.AdditionalFeatures[SqlExtensions.Versioned];
            }

            return false;
        }

        private static bool IsUsingAuditable(InsertDataExpression expression)
        {
            if (expression.AdditionalFeatures.ContainsKey(SqlExtensions.Auditable))
            {
                return (bool)expression.AdditionalFeatures[SqlExtensions.Auditable];
            }

            return false;
        }

        public override bool IsAdditionalFeatureSupported(string feature)
            => (feature == SqlExtensions.HiLoInsert)
               || (feature == SqlExtensions.Versioned)
               || (feature == SqlExtensions.Auditable)
               || base.IsAdditionalFeatureSupported(feature);

        public override string Generate(InsertDataExpression expression)
        {
            // versioned => add 'PersistenceVersion' field
            if (IsUsingVersioned(expression))
            {
                foreach (InsertionDataDefinition row in expression.Rows)
                {
                    row.Add(new KeyValuePair<string, object>("PersistenceVersion", 1));
                }
            }

            // auditable => add fields 'CreatedBy' and 'CreatedAt'
            if (IsUsingAuditable(expression))
            {
                foreach (InsertionDataDefinition row in expression.Rows)
                {
                    row.Add(new KeyValuePair<string, object>("CreatedBy", HiLoHelper.MigrationOwner));
                    row.Add(new KeyValuePair<string, object>("CreatedAt", RawSql.Insert("(now() at time zone 'utc')")));
                }
            }

            // HiLo => take NextId and bump it
            if (IsUsingHiLoInsert(expression))
            {
                List<string> columnNames = new List<string>();
                List<string> columnValues = new List<string>();
                List<string> sqlStrings = new List<string>();

                string primaryKeyColumnName = $"{expression.TableName}Id";

                RawSql primaryKeyNextId = HiLoHelper.GetNextId(expression.TableName);

                foreach (InsertionDataDefinition row in expression.Rows)
                {
                    // initialize
                    columnNames.Clear();
                    columnValues.Clear();

                    // add specified properties
                    foreach (KeyValuePair<string, object> item in row)
                    {
                        // throw an error if a value for primary key column is specified
                        if (string.Equals(item.Key, primaryKeyColumnName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            throw new InvalidOperationException(
                                $"Primary key column {primaryKeyColumnName} must not be initialized when " +
                                $"using {nameof(SqlExtensions.WithHiLoInsert)}");
                        }

                        columnNames.Add(Quoter.QuoteColumnName(item.Key));
                        columnValues.Add(Quoter.QuoteValue(item.Value));
                    }

                    // add the hilo value for the primary key
                    columnNames.Add(Quoter.QuoteColumnName(primaryKeyColumnName));
                    columnValues.Add(Quoter.QuoteValue(primaryKeyNextId));

                    // assemble for the insert statement
                    string columns = string.Join(", ", columnNames.ToArray());
                    string values = string.Join(", ", columnValues.ToArray());

                    // insert of the data
                    sqlStrings.Add(
                        string.Format(
                            InsertData,
                            Quoter.QuoteSchemaName(expression.SchemaName),
                            Quoter.QuoteTableName(expression.TableName),
                            columns,
                            values));

                    // update the hi-lo table
                    sqlStrings.Add(HiLoHelper.BumpNextId(expression.TableName).Value);
                }

                return string.Join("; ", sqlStrings.ToArray());
            }

            return base.Generate(expression);
        }
    }
}
