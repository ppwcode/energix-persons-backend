-- Use a script from hbm2ddl or another tool that create a DDL-script from a database


INSERT INTO dbo.affiliate(
    affiliate_id, created_at, created_by, last_modified_at, last_modified_by)
VALUES (1, NULL, NULL, NULL, NULL);

INSERT INTO dbo.affiliate_properties(
    affiliate_properties_id, created_at, created_by, last_modified_at, last_modified_by, first_name, last_name, inss, date_of_birth, mortality_table_gender, language, emails_private, emails_professional, tel, date_of_death, retirement_date, valid_from, valid_to, affiliate_id)
VALUES (1, NULL, NULL, NULL, NULL, N'tim', N'stu', N'123', '2002-01-01', N'Male', N'NlBe', N'tim@be', N'tim@be', N'', NULL, NULL, '1900-01-01', '2900-01-01', 1);

INSERT INTO dbo.partnerships(
    partnerships_id, created_at, created_by, last_modified_at, last_modified_by, valid_from, valid_to, affiliate_id)
VALUES (1, NULL, NULL, NULL, NULL, '1900-01-01', '2900-01-01', 1);

INSERT INTO dbo.partnership(
    partnership_id, created_at, created_by, last_modified_at, last_modified_by, interval_start, interval_end, date_of_birth, mortality_table_gender, civil_state, partnerships_id)
VALUES (1, NULL, NULL, NULL, NULL, '2002-01-01','2009-01-01','1988-01-01' , N'Male', N'Marriage',1);

INSERT INTO dbo.children(
    children_id, created_at, created_by, last_modified_at, last_modified_by, valid_from, valid_to, affiliate_id)
VALUES
    (1
    ,null
    ,null
    ,null
    ,null
    ,'1900-01-01'
    ,null
    ,1);

INSERT INTO dbo.child_life(
    child_life_id, created_at, created_by, last_modified_at, last_modified_by, children_id, interval_start, interval_end)
VALUES
    (1
    ,null
    ,null
    ,null
    ,null
    ,1
    ,'2019-02-20'
    ,null);


INSERT INTO dbo.employment_slice(
    employment_slice_id, created_at, created_by, last_modified_at, last_modified_by, affiliate_id, employer_uri)
VALUES
    (1
    ,null
    ,null
    ,null
    ,null
    ,1
    ,'gewoonFKplease');

INSERT INTO dbo.employment_slice_properties(
    employment_slice_properties_id, created_at, created_by, last_modified_at, last_modified_by, valid_from, valid_to, employment_category, work_fraction_numerator, work_fraction_denominator, projected_work_fraction, employment_slice_id)
VALUES
    (3
    ,null
    ,null
    ,null
    ,null
    ,'1900-01-01'
    ,'2900-01-01'
    ,'Employee'
    ,20
    ,40
    ,0.5
    ,1);