﻿using Energix.Persons.Server.Models;

namespace Energix.Persons.Server.BusinessLogic.ManagerResults;

public class CreateAffiliateResult
{
    public long? ExistingAffiliateId { get; set; }
    public AffiliateProperties CreatedAffiliate { get; set; }
}
