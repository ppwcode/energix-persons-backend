using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Energix.Persons.Server.Models;

using NHibernate;
using NHibernate.Linq;

using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces.Providers;

namespace Energix.Persons.Server.BusinessLogic;

public interface IAffiliateEmploymentManager : IManager
{
    Task<List<EmploymentSliceProperties>> GetEmploymentsForAffiliate(long affiliateId, DateTime at);
    Task<EmploymentSliceProperties> GetEmploymentProperties(long affiliateId, long employmentSliceId, DateTime at);
}

public class AffiliateEmploymentManager : IAffiliateEmploymentManager
{
    private readonly ISession _session;

    public AffiliateEmploymentManager(ISessionProviderAsync sessionProvider)
    {
        _session = sessionProvider.Session;
    }

    public async Task<List<EmploymentSliceProperties>> GetEmploymentsForAffiliate(long affiliateId, DateTime at)
    {
        var properties = await _session.Query<EmploymentSliceProperties>()
                             .WhereValid(at)
                             .Where(x => x.EmploymentSlice.Affiliate.Id == affiliateId)
                             .ToListAsync();

        return properties;
    }

    public async Task<EmploymentSliceProperties> GetEmploymentProperties(long affiliateId, long employmentSliceId, DateTime at)
    {
        var property = await _session.Query<EmploymentSliceProperties>()
                           .WhereValid(at)
                           .Where(x => x.EmploymentSlice.Id == employmentSliceId)
                           .Where(x => x.EmploymentSlice.Affiliate.Id == affiliateId)
                           .FirstOrDefaultAsync();

        return property;
    }
}
