﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Energix.Persons.Server.Models;

using NHibernate;
using NHibernate.Linq;

using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces.Providers;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.BusinessLogic;

public interface IAffiliateRecognisedInvalidityManager : IManager
{
    Task<RecognisedInvalidityInterval> GetRecognisedInvalidity(long id, DateTime at);
    Task<List<DateTime>> GetRecognisedInvalidityHistory(long id);
    Task<RecognisedInvalidityInterval> UpdateRecognisedInvalidity(long id, RecognisedInvalidityInterval model, Sourced sourced);
}

public class AffiliateRecognisedInvalidityManager : IAffiliateRecognisedInvalidityManager
{
    private readonly ISession _session;

    public AffiliateRecognisedInvalidityManager(ISessionProviderAsync sessionProvider)
    {
        _session = sessionProvider.Session;
    }

    public async Task<RecognisedInvalidityInterval> GetRecognisedInvalidity(long id, DateTime at)
    {
        var affiliate = await _session.Query<Affiliate>().SingleOrDefaultAsync(x => x.Id == id);
        if (affiliate == null)
        {
            throw new NotFoundException();
        }

        var currentRecognisedInvalidityInterval = await _session.Query<RecognisedInvalidityInterval>()
                                                    .Where(x => x.Affiliate.Id == id)
                                                    .WhereValid(at)
                                                    .OrderByDescending(x => x.ValidFrom)
                                                    .FirstOrDefaultAsync();

        return currentRecognisedInvalidityInterval;
    }

    public async Task<List<DateTime>> GetRecognisedInvalidityHistory(long id)
    {
        var affiliate = await _session.Query<Affiliate>().SingleOrDefaultAsync(x => x.Id == id);
        if (affiliate == null)
        {
            throw new NotFoundException();
        }

        var dates = await _session.Query<RecognisedInvalidityInterval>()
                        .Where(x => x.Affiliate.Id == id)
                        .Select(x => x.ValidFrom)
                        .ToListAsync();

        return dates;
    }

    public async Task<RecognisedInvalidityInterval> UpdateRecognisedInvalidity(long id, RecognisedInvalidityInterval model, Sourced sourced)
    {
        var affiliate = await _session.Query<Affiliate>().SingleOrDefaultAsync(x => x.Id == id);
        if (affiliate == null)
        {
            throw new NotFoundException();
        }

        var res = affiliate.AddHistoricallyWhenChanged(
            affiliate.RecognisedInvalidityIntervals,
            x => affiliate.AddRecognisedInvalidityInterval(x),
            model,
            sourced);
        await _session.SaveAsync(res);
        return res;
    }
}
