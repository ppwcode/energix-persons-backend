﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Energix.Persons.Server.Models;

using NHibernate;
using NHibernate.Linq;

using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces.Providers;

namespace Energix.Persons.Server.BusinessLogic;

public interface IHealthManager : IManager
{
    Task<bool> GetDatabaseHealth();
}

public class HealthManager : IHealthManager
{
    private readonly ILogger _logger;
    private readonly ISession _session;

    public HealthManager(ISessionProviderAsync sessionProvider, ILogger logger)
    {
        _logger = logger;
        _session = sessionProvider.Session;
    }

    public async Task<bool> GetDatabaseHealth()
    {
        try
        {
            var dummy = await _session.Query<Affiliate>().Select(x => x.Id).Take(1).SingleOrDefaultAsync();
            return dummy != -1;
        }
        catch (Exception e)
        {
            _logger.Error("Failed to query DB", e);
            return false;
        }
    }
}
