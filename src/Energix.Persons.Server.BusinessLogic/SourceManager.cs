﻿using System.Threading.Tasks;

using Energix.Persons.Server.Models;

using NHibernate;
using NHibernate.Linq;

using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces.Providers;

namespace Energix.Persons.Server.BusinessLogic;

public interface ISourceManager : IManager
{
    Task<Sourced> GetOrCreate(string source);
}

public class SourceManager : ISourceManager
{
    private readonly ISession _session;

    public SourceManager(ISessionProviderAsync sessionProvider)
    {
        _session = sessionProvider.Session;
    }

    public async Task<Sourced> GetOrCreate(string source)
    {
        var sourced = await _session.Query<Sourced>().SingleOrDefaultAsync(x => x.Source == source);
        if (sourced == null)
        {
            var s = new Sourced() { Source = source };
            await _session.SaveAsync(s);
            return s;
        }

        return sourced;
    }
}
