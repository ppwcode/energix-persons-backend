﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Energix.Persons.Server.BusinessLogic.ManagerResults;
using Energix.Persons.Server.Models;

using NHibernate;
using NHibernate.Linq;

using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces.Providers;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.BusinessLogic;

public interface IAffiliateManager : IManager
{
    Task<AffiliateProperties> GetById(long id, DateTime at);
    Task<List<DateTime>> GetHistoryById(long id);
    Task<Partnerships> GetAffiliatePartnerships(long id, DateTime at);
    Task<List<DateTime>> GetAffiliatePartnershipsHistory(long id);
    Task<Children> GetAffiliateChildren(long id, DateTime at);
    Task<List<DateTime>> GetAffiliateChildrenHistory(long id);
    Task<Absorption> GetAbsorptionForAbsorbedAffiliate(long id);
    Task<List<Absorption>> GetAbsorptionsByAffiliate(long id);

    Task<CreateAffiliateResult> Create(AffiliateProperties model, Sourced sourced);
    Task<AffiliateProperties> Update(long id, AffiliateProperties model, Sourced sourced);
    Task<Partnerships> UpdatePartnerShips(long id, Partnerships partnerships, Sourced sourced);
    Task<Children> UpdateChildren(long id, Children partnerships, Sourced sourced);
}

public class AffiliateManager : IAffiliateManager
{
    private readonly ISession _session;

    public AffiliateManager(ISessionProviderAsync sessionProvider)
    {
        _session = sessionProvider.Session;
    }

    public async Task<AffiliateProperties> GetById(long id, DateTime at)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);
        var currentProperties = await _session.Query<AffiliateProperties>()
                                    .Where(x => x.Affiliate.Id == affiliate.Id)
                                    .WhereValid(at)
                                    .OrderByDescending(x => x.ValidFrom)
                                    .FirstOrDefaultAsync();

        return currentProperties;
    }

    public async Task<List<DateTime>> GetHistoryById(long id)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var dates = await _session.Query<AffiliateProperties>()
                        .Where(x => x.Affiliate.Id == affiliate.Id)
                        .Select(x => x.ValidFrom)
                        .ToListAsync();

        return dates;
    }

    public async Task<Partnerships> GetAffiliatePartnerships(long id, DateTime at)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var partnerShips = await _session.Query<Partnerships>()
                               .Where(x => x.Affiliate.Id == affiliate.Id)
                               .WhereValid(at)
                               .SingleOrDefaultAsync();

        return partnerShips;
    }

    public async Task<Partnerships> UpdatePartnerShips(long id, Partnerships model, Sourced sourced)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var res = affiliate.AddHistoricallyWhenChanged(
            affiliate.Partnerships,
            x => affiliate.AddPartnership(x),
            model,
            sourced);
        await _session.SaveAsync(res);
        return res;
    }

    public async Task<List<DateTime>> GetAffiliatePartnershipsHistory(long id)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var dates = await _session.Query<Partnerships>()
                        .Where(x => x.Affiliate.Id == affiliate.Id)
                        .Select(x => x.ValidFrom)
                        .ToListAsync();
        return dates;
    }

    public async Task<Children> GetAffiliateChildren(long id, DateTime at)
    {
        await GetAffiliateOrThrowNotFound(id);

        var children = await _session.Query<Children>()
                           .Where(x => x.Affiliate.Id == id)
                           .WhereValid(at)
                           .SingleOrDefaultAsync();

        return children;
    }

    public async Task<Children> UpdateChildren(long id, Children model, Sourced sourced)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var res = affiliate.AddHistoricallyWhenChanged(
            affiliate.Children,
            x => affiliate.AddChildren(x),
            model,
            sourced);
        await _session.SaveAsync(res);
        await _session.SaveAsync(affiliate);
        return res;
    }

    public async Task<List<DateTime>> GetAffiliateChildrenHistory(long id)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var dates = await _session.Query<Children>()
                        .Where(x => x.Affiliate.Id == affiliate.Id)
                        .Select(x => x.ValidFrom)
                        .ToListAsync();
        return dates;
    }

    /// <inheritdoc />
    public async Task<Absorption> GetAbsorptionForAbsorbedAffiliate(long id)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var absorption = await _session.Query<Absorption>()
                             .Where(x => x.Absorbed.Id == affiliate.Id)
                             .SingleOrDefaultAsync(); // you can only be absorbed once

        return absorption;
    }

    public async Task<List<Absorption>> GetAbsorptionsByAffiliate(long id)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var absorptions = await _session.Query<Absorption>()
                              .Where(x => x.AbsorbedBy.Id == affiliate.Id)
                              .ToListAsync();

        return absorptions;
    }

    /// <inheritdoc />
    public async Task<CreateAffiliateResult> Create(AffiliateProperties model, Sourced sourced)
    {
        var existingAffiliate = await _session.Query<AffiliateProperties>()
                                    .Where(x => x.Inss == model.Inss)
                                    .FirstOrDefaultAsync();
        if (existingAffiliate != null)
        {
            return new CreateAffiliateResult
                   {
                       ExistingAffiliateId = existingAffiliate.Id
                   };
        }

        var affiliate = new Affiliate();
        model.ValidFrom = DateTime.UtcNow;
        model.AddSourced(sourced);
        affiliate.AddProperties(model);
        await _session.SaveAsync(affiliate);
        await _session.FlushAsync();
        return new CreateAffiliateResult
               {
                   CreatedAffiliate = affiliate.AffiliateProperties.First()
               };
    }

    public async Task<AffiliateProperties> Update(long id, AffiliateProperties model, Sourced sourced)
    {
        var affiliate = await GetAffiliateOrThrowNotFound(id);

        var res = affiliate.AddHistoricallyWhenChanged(
            affiliate.AffiliateProperties,
            x => affiliate.AddProperties(model),
            model,
            sourced);

        await _session.FlushAsync();
        return res;
    }

    private async Task<Affiliate> GetAffiliateOrThrowNotFound(long id)
    {
        var affiliate = await _session.Query<Affiliate>().SingleOrDefaultAsync(x => x.Id == id);
        if (affiliate == null)
        {
            throw new NotFoundException();
        }

        return affiliate;
    }
}
