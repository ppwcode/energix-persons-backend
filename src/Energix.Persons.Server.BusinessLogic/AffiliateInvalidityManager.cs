﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Energix.Persons.Server.Models;

using NHibernate;
using NHibernate.Linq;

using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces.Providers;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.BusinessLogic;

public interface IAffiliateInvalidityManager : IManager
{
    Task<EmployerInvalidityInterval> GetEmployerInvalidity(long id, DateTime at);
    Task<List<DateTime>> GetEmployerInvalidityHistory(long id);
    Task<EmployerInvalidityInterval> UpdateEmployerInvalidity(long id, EmployerInvalidityInterval employerInvalidityInterval, Sourced sourced);
}

public class AffiliateInvalidityManager : IAffiliateInvalidityManager
{
    private readonly ISession _session;

    public AffiliateInvalidityManager(ISessionProviderAsync sessionProvider)
    {
        _session = sessionProvider.Session;
    }

    public async Task<EmployerInvalidityInterval> GetEmployerInvalidity(long id, DateTime at)
    {
        var affiliate = await _session.Query<Affiliate>().SingleOrDefaultAsync(x => x.Id == id);
        if (affiliate == null)
        {
            throw new NotFoundException();
        }

        var currentEmployerInvalidityInterval = await _session.Query<EmployerInvalidityInterval>()
                                                    .Where(x => x.Affiliate.Id == id)
                                                    .WhereValid(at)
                                                    .OrderByDescending(x => x.ValidFrom)
                                                    .FirstOrDefaultAsync();

        return currentEmployerInvalidityInterval;
    }

    public async Task<List<DateTime>> GetEmployerInvalidityHistory(long id)
    {
        var affiliate = await _session.Query<Affiliate>().SingleOrDefaultAsync(x => x.Id == id);
        if (affiliate == null)
        {
            throw new NotFoundException();
        }

        var dates = await _session.Query<EmployerInvalidityInterval>()
                        .Where(x => x.Affiliate.Id == id)
                        .Select(x => x.ValidFrom)
                        .ToListAsync();

        return dates;
    }

    public async Task<EmployerInvalidityInterval> UpdateEmployerInvalidity(long id, EmployerInvalidityInterval model, Sourced sourced)
    {
        var affiliate = await _session.Query<Affiliate>().SingleOrDefaultAsync(x => x.Id == id);
        if (affiliate == null)
        {
            throw new NotFoundException();
        }

        var res = affiliate.AddHistoricallyWhenChanged(
            affiliate.EmployerInvalidityIntervals,
            x => affiliate.AddEmploymentInvalidityInterval(x),
            model,
            sourced);
        await _session.SaveAsync(res);
        return res;
    }
}
