using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc cref="IModelManager{TModel,TIdentity}" />
    public interface IModelManager<TModel> : IModelManager<TModel, long>
        where TModel : class, IPersistentObject
    {
    }
}
