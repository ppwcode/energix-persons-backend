using Energix.Persons.PPW.Extensions;

using JetBrains.Annotations;

using PPWCode.API.Core;
using PPWCode.Server.Core.Managers.Implementations;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc cref="ILinksManager{TModel,TDto,TContext}" />
    public abstract class LinksManager<TSource, TLinksDto, TContext>
        : PPWCode.Server.Core.Managers.Implementations.LinksManager<TSource, TLinksDto, TContext>,
          ILinksManager<TSource, TLinksDto, TContext>
        where TLinksDto : ILinksDto
        where TContext : LinksContext, new()
    {
        protected LinksManager([NotNull] IEnergixRequestContext requestContext)
            : base(requestContext)
        {
        }
    }
}
