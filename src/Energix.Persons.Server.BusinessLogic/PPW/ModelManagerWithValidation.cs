using Energix.Persons.Server.Repositories;

using JetBrains.Annotations;

using PPWCode.Server.Core.Managers.Implementations;
using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc cref="IModelManagerWithValidation{TModel}" />
    public abstract class ModelManagerWithValidation<TModel>
        : ModelManagerWithValidation<TModel, long>,
          IModelManagerWithValidation<TModel>
        where TModel : class, IPersistentObject
    {
        protected ModelManagerWithValidation(
            [NotNull] IRepository<TModel> repository,
            [NotNull] IModelValidator<TModel> modelValidator)
            : base(repository, modelValidator)
        {
        }
    }
}
