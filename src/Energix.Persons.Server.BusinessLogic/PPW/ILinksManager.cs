using PPWCode.API.Core;
using PPWCode.Server.Core.Managers.Implementations;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc />
    public interface ILinksManager<in TSource, in TLinksDto, in TContext>
        : PPWCode.Server.Core.Managers.Interfaces.ILinksManager<TSource, TLinksDto, TContext>
        where TLinksDto : ILinksDto
        where TContext : LinksContext, new()
    {
    }

    /// <inheritdoc />
    public interface ILinksManager<in TLinksDto, in TContext>
        : PPWCode.Server.Core.Managers.Interfaces.ILinksManager<TLinksDto, TContext>
        where TLinksDto : ILinksDto
        where TContext : LinksContext, new()
    {
    }
}
