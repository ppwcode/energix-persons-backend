using Energix.Persons.Server.Repositories;

using JetBrains.Annotations;

using PPWCode.Server.Core.Managers.Implementations;
using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc cref="IModelManager{TModel}" />
    public abstract class ModelManager<TModel>
        : ModelManager<TModel, long>,
          IModelManager<TModel>
        where TModel : class, IPersistentObject
    {
        protected ModelManager([NotNull] IRepository<TModel> repository)
            : base(repository)
        {
        }
    }
}
