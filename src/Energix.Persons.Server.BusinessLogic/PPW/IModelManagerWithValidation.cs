using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc cref="IModelManagerWithValidation{TModel,TIdentity}" />
    public interface IModelManagerWithValidation<TModel> : IModelManagerWithValidation<TModel, long>
        where TModel : class, IPersistentObject
    {
    }
}
