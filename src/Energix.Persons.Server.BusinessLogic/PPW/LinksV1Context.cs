using JetBrains.Annotations;

using Microsoft.AspNetCore.Mvc;

using PPWCode.Server.Core.Managers.Implementations;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    public class LinksV1Context : LinksContext
    {
        public static readonly ApiVersion ApiVersionV1 = new ApiVersion(1, 0);

        public LinksV1Context()
            : this(ApiVersionV1, DefaultApiVersionFormat)
        {
        }

        public LinksV1Context(
            [NotNull] ApiVersion apiVersion,
            [CanBeNull] string apiVersionFormat = null)
            : base(apiVersion, apiVersionFormat)
        {
        }
    }
}
