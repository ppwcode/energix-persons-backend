using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc cref="IModelValidator{TModel,TIdentity}" />
    public interface IModelValidator<in TModel> : IModelValidator<TModel, long>
        where TModel : class, IPersistentObject
    {
    }
}
