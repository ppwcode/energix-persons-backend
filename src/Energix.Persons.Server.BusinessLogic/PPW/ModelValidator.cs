using PPWCode.Server.Core.Managers.Implementations;
using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc cref="IModelValidator{TModel}" />
    public abstract class ModelValidator<TModel>
        : ModelValidator<TModel, long>,
          IModelValidator<TModel>
        where TModel : class, IPersistentObject
    {
    }
}
