using Energix.Persons.PPW.Extensions;

using JetBrains.Annotations;

using PPWCode.API.Core;
using PPWCode.Server.Core.Managers.Implementations;

namespace Energix.Persons.Server.BusinessLogic.PPW
{
    /// <inheritdoc cref="ILinksManager{TModel,TDto,TContext}" />
    public abstract class SimpleLinksManager<TLinksDto, TContext>
        : PPWCode.Server.Core.Managers.Implementations.SimpleLinksManager<TLinksDto, TContext>,
          ILinksManager<TLinksDto, TContext>
        where TLinksDto : ILinksDto
        where TContext : LinksContext, new()
    {
        protected SimpleLinksManager([NotNull] IEnergixRequestContext requestContext)
            : base(requestContext)
        {
        }
    }
}
