using System.Diagnostics.CodeAnalysis;
using System.Reflection;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using JetBrains.Annotations;

using PPWCode.Server.Core.Managers.Interfaces;

namespace Energix.Persons.Server.BusinessLogic.PPW.Bootstrap
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class ManagerInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Assembly thisAssembly = typeof(ManagerInstaller).Assembly;

            container.Register(
                Classes
                    .FromAssembly(thisAssembly)
                    .BasedOn<IManager>()
                    .WithService.FromInterface()
                    .LifestyleTransient(),
                Classes
                    .FromAssembly(thisAssembly)
                    .BasedOn<ISingletonManager>()
                    .WithService.FromInterface()
                    .LifestyleSingleton());
        }
    }
}
