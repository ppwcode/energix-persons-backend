using System.Diagnostics.CodeAnalysis;
using System.Reflection;

using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using JetBrains.Annotations;

using PPWCode.Server.Core.Utils;

namespace Energix.Persons.Server.BusinessLogic.PPW.Bootstrap
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class ValidatorInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Assembly thisAssembly = typeof(ValidatorInstaller).Assembly;

            container
                .RegisterOpenGenerics(
                    thisAssembly,
                    typeof(IModelValidator<>),
                    typeof(ModelValidator<>),
                    LifestyleType.Transient);
        }
    }
}
