﻿using System;

namespace Energix.Persons.Utils.Extensions
{
    /// <summary>
    ///     Helper class that provides extensions for DateTime.
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        ///     Whether a DateTime has a Time component or is just a Date.
        /// </summary>
        /// <param name="dt">The DateTime.</param>
        /// <returns>True or false</returns>
        public static bool IsDate(this DateTime? dt)
            => dt.HasValue && (dt.Value == dt.Value.Date);

        /// <summary>
        ///     Whether a DateTime has a Time component or is just a Date.
        /// </summary>
        /// <param name="dt">The DateTime.</param>
        /// <returns>True or false</returns>
        public static bool IsDate(this DateTime dt)
            => dt == dt.Date;

        /// <summary>
        ///     Adds quarters to a DateTime.
        ///     If the resulting day is not valid in the resulting month, the last valid day of the resulting month is used.
        /// </summary>
        /// <param name="dt">The DateTime you want to add quarters to.</param>
        /// <param name="quarters">The number of quarters you want to add.</param>
        /// <returns>The DateTime with the quarters added.</returns>
        public static DateTime AddQuarters(this DateTime dt, int quarters)
            => dt.AddMonths(3 * quarters);

        /// <summary>
        ///     Adds months to a given nullable DateTime.
        /// </summary>
        /// <param name="dt">The given DateTime.</param>
        /// <param name="months">The number of months you want to add.</param>
        /// <returns>A nullable DateTime with months added.</returns>
        public static DateTime? AddMonths(this DateTime? dt, int months)
            => dt?.AddMonths(months);

        /// <summary>
        ///     Returns a UTC DateTime, based on year, month, day, hours, minutes and seconds parameters.
        /// </summary>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <param name="day">day</param>
        /// ///
        /// <param name="hours">hours</param>
        /// ///
        /// <param name="minutes">minutes</param>
        /// ///
        /// <param name="seconds">seconds</param>
        /// <returns>A Datetime in UTC</returns>
        public static DateTime GetUtcDateTime(
            int year,
            int month,
            int day,
            int hours,
            int minutes,
            int seconds)
            => new DateTime(
                year,
                month,
                day,
                hours,
                minutes,
                seconds,
                DateTimeKind.Utc);

        /// <summary>
        ///     Returns a UTC Date, based on year, month and day parameters.
        /// </summary>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <param name="day">day</param>
        /// <returns>A Datetime in UTC</returns>
        public static DateTime GetUtcDate(int year, int month, int day)
            => GetUtcDateTime(
                year,
                month,
                day,
                0,
                0,
                0).Date;

        /// <summary>
        ///     Gets the first day of quarter of the given DateTime.
        /// </summary>
        /// <param name="dt">The given DateTime.</param>
        /// <returns>The first day of quarter.</returns>
        public static DateTime FirstDayOfQuarter(this DateTime dt)
        {
            int months = (((dt.Month - 1) / 3) * 3) + 1;
            DateTime result = new DateTime(
                dt.Year + (months / 12),
                months % 12,
                1,
                0,
                0,
                0,
                dt.Kind);
            return result;
        }

        /// <summary>
        ///     Whether DateTime is first day of quarter.
        /// </summary>
        /// <param name="dt">The DateTime.</param>
        /// <returns>True or false.</returns>
        public static bool IsFirstDayOfQuarter(this DateTime dt)
            => dt.IsDate() && (dt.Day == 1) && ((dt.Month - 1) % 3 == 0);

        /// <summary>
        ///     Gets the first day of quarter of the given nullable DateTime.
        /// </summary>
        /// <param name="dt">The given nullable DateTime.</param>
        /// <returns>The first day of quarter or null.</returns>
        public static DateTime? FirstDayOfQuarter(this DateTime? dt)
            => dt?.FirstDayOfQuarter();

        /// <summary>
        ///     Gets the first day of next quarter for given DateTime.
        /// </summary>
        /// <param name="dt">The given DateTime.</param>
        /// <returns>The first day of next quarter.</returns>
        public static DateTime FirstDayOfNextQuarter(this DateTime dt)
            => dt.FirstDayOfQuarter().AddQuarters(1);

        /// <summary>
        ///     Gets the first day of next quarter for given nullable DateTime.
        /// </summary>
        /// <param name="dt">The given DateTime.</param>
        /// <returns>The first day of next quarter.</returns>
        public static DateTime? FirstDayOfNextQuarter(this DateTime? dt)
            => dt?.FirstDayOfNextQuarter();

        /// <summary>
        ///     Whether given DateTime is first day of month.
        /// </summary>
        /// <param name="dt">The given DateTime.</param>
        /// <returns>True or false.</returns>
        public static bool IsFirstDayOfMonth(this DateTime dt)
            => dt.IsDate() && (dt.Day == 1);

        /// <summary>
        ///     Whether nullable DateTime is first day of month.
        /// </summary>
        /// <param name="dt">The given nullable DateTime.</param>
        /// <returns>True or false</returns>
        public static bool IsFirstDayOfMonth(this DateTime? dt)
            => dt.HasValue && dt.Value.IsDate() && (dt.Value.Day == 1);

        /// <summary>
        ///     Gets the first day of next month for given DateTime.
        /// </summary>
        /// <param name="dt">The given DateTime.</param>
        /// <returns>The first day of next month.</returns>
        public static DateTime FirstDayOfNextMonth(this DateTime dt)
        {
            DateTime ndt = dt.AddMonths(1);
            return new DateTime(
                ndt.Year,
                ndt.Month,
                1,
                0,
                0,
                0,
                dt.Kind);
        }

        /// <summary>
        ///     Gets the first day of next month for given nullable DateTime.
        /// </summary>
        /// <param name="dt">The given nullable DateTime.</param>
        /// <returns>The first day of next month or null.</returns>
        public static DateTime? FirstDayOfNextMonth(this DateTime? dt)
            => dt?.FirstDayOfNextMonth();

        /// <summary>
        ///     Gets the first day of month for the given DateTime.
        /// </summary>
        /// <param name="dt">The given DateTime.</param>
        /// <returns>The first day of month.</returns>
        public static DateTime FirstDayOfMonth(this DateTime dt)
            => new DateTime(
                dt.Year,
                dt.Month,
                1,
                0,
                0,
                0,
                dt.Kind);

        /// <summary>
        ///     Gets the first day of month for the given nullable DateTime.
        /// </summary>
        /// <param name="dt">The given nullable DateTime.</param>
        /// <returns>The first day of month or null.</returns>
        public static DateTime? FirstDayOfMonth(this DateTime? dt)
            => dt?.FirstDayOfMonth();
    }
}
