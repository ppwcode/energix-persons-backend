using System;
using System.Collections.Generic;
using System.Linq;

namespace Energix.Persons.Utils.Extensions
{
    public static class EnumExtensions
    {
        public static IEnumerable<TEnum> GetIndividualFlags<TEnum>(this TEnum value)
            where TEnum : Enum
            => Enum
                .GetValues(typeof(TEnum))
                .OfType<TEnum>()
                .Where(
                    e =>
                    {
                        long i = Convert.ToInt64(e);
                        return ((i & (i - 1)) == 0) && value.HasFlag(e);
                    });
    }
}
