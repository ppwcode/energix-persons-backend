using System.Runtime.Serialization;

namespace Energix.Persons.API;

[DataContract]
public class FractionDto
{
    [DataMember]
    public int Numerator { get; set; }
    [DataMember]
    public int Denominator { get; set; }
}
