using System.Collections.Generic;
using System.Runtime.Serialization;

using PPWCode.API.Core;

namespace Energix.Persons.API;

[DataContract]
public class HistoryDto : Dto
{
    [DataMember]
    public List<string> Items { get; set; }
}
