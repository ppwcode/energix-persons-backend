using System.Runtime.Serialization;

using PPWCode.API.Core;

namespace Energix.Persons.API;

[DataContract]
public class EmploymentSliceDto : Dto
{
    [DataMember]
    public EmploymentCategory EmploymentCategory { get; set; }

    [DataMember]
    public FractionDto WorkFraction { get; set; }

    [DataMember]
    public decimal ProjectedWorkFraction { get; set; }
}
