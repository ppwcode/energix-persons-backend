using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Energix.Persons.API;

[DataContract]
public class EmployersDto
{
    [DataMember]
    public List<string> Hrefs { get; set; }
}