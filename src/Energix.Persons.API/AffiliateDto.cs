﻿using System;
using System.Runtime.Serialization;

using Energix.Persons.API.Common;
using Energix.Persons.API.Enums;

using PPWCode.API.Core;

using Swashbuckle.AspNetCore.Annotations;

namespace Energix.Persons.API;

[DataContract]
public class AffiliateDto : Dto
{
    [DataMember]
    public long Id { get; set; }

    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public string Inss { get; set; }

    [DataMember]
    public DateOnly DateOfBirth { get; set; }

    [DataMember]
    public Gender MortalityTableGender { get; set; }

    [DataMember]
    public Language Language { get; set; }

    [DataMember]
    public Address Address { get; set; }

    [DataMember]
    [SwaggerSchema(ReadOnly = true)]
    public AffiliateHref Href { get; set; }

    [DataMember]
    [SwaggerSchema(WriteOnly = true)]
    public string Sourced { get; set; }
}

[DataContract]
[SwaggerSchema(ReadOnly = true)]
public class AffiliateHref
{
    [DataMember]
    public string History { get; set; }

    [DataMember]
    public string Partnerships { get; set; }

    [DataMember]
    public string Children { get; set; }

    [DataMember]
    public string Absorbed { get; set; }

    [DataMember]
    public string Absorptions { get; set; }

    [DataMember]
    public string Sources { get; set; }
}
