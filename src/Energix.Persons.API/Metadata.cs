﻿using System;
using System.Collections.Generic;

using PPWCode.API.Core;

namespace Energix.Persons.API
{
    public class Metadata : ILinksDto
    {
        public string ApplicationName { get; set; }
        public string EnvironmentName { get; set; }
        public string ContentRootPath { get; set; }
        public string ServerVersion { get; set; }

        /// <inheritdoc />
        public IDictionary<string, IDictionary<string, object>> Links { get; set; }

        /// <inheritdoc />
        public Uri HRef { get; set; }
    }
}
