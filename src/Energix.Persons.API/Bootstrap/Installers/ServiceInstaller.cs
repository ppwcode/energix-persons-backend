﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using JetBrains.Annotations;

using PPWCode.API.Core.Services;

namespace Energix.Persons.API.Bootstrap.Installers
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class ServiceInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Assembly coreApiAssembly = typeof(IService).Assembly;
            Assembly thisAssembly = typeof(ServiceInstaller).Assembly;

            container
                .Register(
                    Classes
                        .FromAssembly(coreApiAssembly)
                        .BasedOn(typeof(IService))
                        .WithService.AllInterfaces()
                        .LifestyleSingleton(),
                    Classes
                        .FromAssembly(thisAssembly)
                        .BasedOn(typeof(IService))
                        .WithService.AllInterfaces()
                        .LifestyleSingleton());
        }
    }
}
