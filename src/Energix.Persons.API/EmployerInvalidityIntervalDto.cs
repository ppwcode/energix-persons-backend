using System.Collections.Generic;
using System.Runtime.Serialization;

using PPWCode.API.Core;

using Swashbuckle.AspNetCore.Annotations;

namespace Energix.Persons.API;

[DataContract]
public class EmployerInvalidityIntervalDto : Dto
{
    [DataMember]
    public List<EmployerInvalidityDto> Items { get; set; }

    [DataMember]
    [SwaggerSchema(ReadOnly = true)]
    public HistoryHrefDto Href { get; set; }

    [DataMember]
    [SwaggerSchema(WriteOnly = true)]
    public string Sourced { get; set; }
}

[DataContract]
public class EmployerInvalidityDto : Dto
{
    [DataMember]
    public IntervalDto Interval { get; set; }

    [DataMember]
    public decimal Degree { get; set; }
}
