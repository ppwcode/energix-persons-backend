namespace Energix.Persons.API
{
    public static class Routes
    {
        private const string ApiVersion = "v{version:apiVersion}/";

        // Root paths
        public const string Root = ApiVersion;

        // sub path
        public const string Metadata = "metadata";
        public const string Affiliate = ApiVersion + "Affiliate";

        public const string Health = ApiVersion + "Health";
    }
}
