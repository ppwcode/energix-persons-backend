using System.Runtime.Serialization;

using PPWCode.API.Core;

namespace Energix.Persons.API;

[DataContract]
public class ChildLifeDto : Dto
{
    [DataMember]
    public IntervalDto Interval { get; set; }
}
