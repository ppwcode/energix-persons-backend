using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

using JetBrains.Annotations;

using PPWCode.API.Core;

namespace Energix.Persons.API.Common
{
    [DataContract]
    public abstract class PagedSearchCriteria : Dto
    {
        /// <summary>
        ///     Page number that should be retrieved.
        /// </summary>
        [DataMember]
        [Required]
        [Range(1, int.MaxValue)]
        [CanBeNull]
        public int? Page { get; set; }

        /// <summary>
        ///     Number of records in each page.
        /// </summary>
        [DataMember]
        [Required]
        [Range(1, 50)]
        [CanBeNull]
        public int? PageSize { get; set; }
    }
}
