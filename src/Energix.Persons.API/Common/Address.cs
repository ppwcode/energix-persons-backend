using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

using JetBrains.Annotations;

using PPWCode.API.Core;

namespace Energix.Persons.API.Common
{
    [DataContract]
    public class Address : Dto
    {
        /// <summary>
        ///     Street name.
        /// </summary>
        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [CanBeNull]
        [StringLength(FieldLengths.AddressStreetLength)]
        public string Street { get; set; }

        /// <summary>
        ///     House number.
        /// </summary>
        [DataMember]
        [CanBeNull]
        [StringLength(FieldLengths.AddressHouseNumberLength)]
        public string HouseNumber { get; set; }

        /// <summary>
        ///     Mailbox.
        /// </summary>
        [DataMember]
        [CanBeNull]
        [StringLength(FieldLengths.AddressBoxLength)]
        public string Box { get; set; }

        /// <summary>
        ///     Zip code.
        /// </summary>
        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [CanBeNull]
        [StringLength(FieldLengths.AddressZipCodeLength)]
        public string ZipCode { get; set; }

        /// <summary>
        ///     City name.
        /// </summary>
        [DataMember]
        [CanBeNull]
        [StringLength(FieldLengths.AddressCityLength)]
        public string City { get; set; }

        /// <summary>
        ///     <para>
        ///         An indication of the country for this address.The country is always indicated in the form of a string and can
        ///         contain 2 kinds of representations.
        ///     </para>
        ///     <para>
        ///         For official addresses that originate from CentralDB(through LES - Legal Entity Service), the field contains a
        ///         NIS
        ///         code in the format 'NisDDD' with 'DDD' the NIS code.When the field matches this format, the NIS code must be
        ///         converted into a country name when used to display to the user or in a print file.Note that official addresses
        ///         are
        ///         always used read-only and the user will never have to update the field in that case.
        ///     </para>
        ///     <para>Examples of this type of country are: 'Nis150', 'Nis105', ...</para>
        ///     <para>
        ///         For delivery addresses(either of the permanent or the one-time type), the field contains a country name.The
        ///         field
        ///         must always be used as-is and no conversion is needed: nor for display to the user, nor for use in a print
        ///         file.
        ///     </para>
        ///     <para>Examples of this type of country are: 'België', 'Belgique', 'Deutschland', 'France', ...</para>
        /// </summary>
        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [CanBeNull]
        [StringLength(FieldLengths.CountryLength)]
        public string Country { get; set; }
    }
}
