namespace Energix.Persons.API
{
    public static class FieldLengths
    {
        // address related
        public const int AddressZipCodeLength = 10;
        public const int AddressCityLength = 100;
        public const int AddressStreetLength = 100;
        public const int AddressHouseNumberLength = 10;
        public const int AddressBoxLength = 10;
        public const int CountryLength = 50;

        // Person
        public const int NameLength = 150;
        public const int EmailLength = 150;
        public const int SourceLength = 50;
    }
}
