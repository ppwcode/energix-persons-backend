using System.Collections.Generic;
using System.Runtime.Serialization;

using PPWCode.API.Core;

using Swashbuckle.AspNetCore.Annotations;

namespace Energix.Persons.API;

public class PartnershipsDto : Dto
{
    [DataMember]
    public List<PartnershipDto> Items { get; set; }

    [DataMember]
    [SwaggerSchema(ReadOnly = true)]
    public HistoryHrefDto Href { get; set; }

    [DataMember]
    [SwaggerSchema(WriteOnly = true)]
    public string Sourced { get; set; }
}
