using System.Collections.Generic;
using System.Runtime.Serialization;

using Energix.Persons.API.Enums;

using PPWCode.API.Core;

using Swashbuckle.AspNetCore.Annotations;

namespace Energix.Persons.API;

[DataContract]
public class RecognisedInvalidityIntervalDto : Dto
{
    [DataMember]
    public List<RecognisedInvalidityDto> Items { get; set; }

    [DataMember]
    [SwaggerSchema(ReadOnly = true)]
    public HistoryHrefDto Href { get; set; }

    [DataMember]
    [SwaggerSchema(WriteOnly = true)]
    public string Sourced { get; set; }
}

[DataContract]
public class RecognisedInvalidityDto : Dto
{
    [DataMember]
    public IntervalDto Interval { get; set; }

    [DataMember]
    public decimal Degree { get; set; }

    [DataMember]
    public InsuranceBenefitTypeGroup Type { get; set; }
}
