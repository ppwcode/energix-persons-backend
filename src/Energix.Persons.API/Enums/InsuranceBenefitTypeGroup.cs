namespace Energix.Persons.API.Enums;

public enum InsuranceBenefitTypeGroup
{
    Death = 1,
    Invalidity = 2
}
