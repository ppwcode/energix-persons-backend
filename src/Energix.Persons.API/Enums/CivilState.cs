namespace Energix.Persons.API.Enums;

public enum CivilState
{
    Marriage = 1,
    LegalCohabitation = 2,
    RecognizedFactualCohabitation = 3
}