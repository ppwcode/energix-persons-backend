namespace Energix.Persons.API.Enums;

public enum Language
{
    NlBe = 1,
    FrBe = 2,
    EnUs = 3
}