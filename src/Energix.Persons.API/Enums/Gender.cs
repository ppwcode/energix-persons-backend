namespace Energix.Persons.API.Enums;

public enum Gender
{
    Male = 1,
    Female = 2,
    X = 3
}