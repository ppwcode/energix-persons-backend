using System.Runtime.Serialization;

using Swashbuckle.AspNetCore.Annotations;

namespace Energix.Persons.API;

[DataContract]
[SwaggerSchema(ReadOnly = true)]
public class HistoryHrefDto
{
    [DataMember]
    public string History { get; set; }
}
