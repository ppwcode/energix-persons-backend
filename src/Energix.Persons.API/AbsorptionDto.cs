﻿using System.Runtime.Serialization;

using PPWCode.API.Core;

namespace Energix.Persons.API;

[DataContract]
public class AbsorptionDto : Dto
{
    public string Absorbed { get; set; }
    public string AbsorbedBy { get; set; }
}
