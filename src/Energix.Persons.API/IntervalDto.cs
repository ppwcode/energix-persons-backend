using System;
using System.Runtime.Serialization;

namespace Energix.Persons.API;

[DataContract]
public class IntervalDto
{
    [DataMember]
    public DateTime? Start { get; set; }
    [DataMember]
    public DateTime? End { get; set; }
}
