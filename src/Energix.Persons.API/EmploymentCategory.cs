namespace Energix.Persons.API;

public enum EmploymentCategory
{
    Employee = 1,
    Manager = 2,
    Executive = 3
}
