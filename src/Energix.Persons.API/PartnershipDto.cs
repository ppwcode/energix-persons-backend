using System;
using System.Runtime.Serialization;

using Energix.Persons.API.Enums;

using PPWCode.API.Core;

namespace Energix.Persons.API;

[DataContract]
public class PartnershipDto : Dto
{
    [DataMember]
    public Gender Gender { get; set; }

    [DataMember]
    public DateTime DateOfBirth { get; set; }

    [DataMember]
    public CivilState CivilState { get; set; }

    [DataMember]
    public IntervalDto Interval { get; set; }
}