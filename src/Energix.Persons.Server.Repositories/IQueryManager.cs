using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Repositories
{
    /// <inheritdoc />
    // ReSharper disable once UnusedTypeParameter
    public interface IQueryManager<TModel> : PPWCode.Server.Core.Repositories.Interfaces.IQueryManager<long>
        where TModel : class, IPersistentObject
    {
    }
}
