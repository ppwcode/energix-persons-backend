using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Repositories
{
    public class QueryManager<TModel>
        : IQueryManager<TModel>
        where TModel : class, IPersistentObject
    {
    }
}
