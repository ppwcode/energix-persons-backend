﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;

using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using JetBrains.Annotations;

using PPWCode.Server.Core.Utils;

namespace Energix.Persons.Server.Repositories.Bootstrap
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class RepositoryInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Assembly thisAssembly = typeof(RepositoryInstaller).Assembly;

            container
                .RegisterOpenGenerics(
                    thisAssembly,
                    typeof(IRepository<>),
                    typeof(Repository<>),
                    LifestyleType.Transient)
                .RegisterOpenGenerics(
                    thisAssembly,
                    typeof(IQueryManager<>),
                    typeof(QueryManager<>),
                    LifestyleType.Transient)
                .Register(
                    Component
                        .For<IRelationEntityRepository>()
                        .ImplementedBy<RelationEntityRepository>()
                        .LifeStyle.Transient);
        }
    }
}
