﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using JetBrains.Annotations;

using NHibernate.Linq;

using PPWCode.Server.Core.Models;
using PPWCode.Vernacular.NHibernate.III.Async.Implementations;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces.Providers;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Repositories
{
    [UsedImplicitly]
    public class Repository<TModel>
        : LinqRepositoryAsync<TModel, long>,
          IRepository<TModel>
        where TModel : class, IPersistentObject
    {
        public Repository(
            [NotNull] ISessionProviderAsync sessionProvider,
            [NotNull] IQueryManager<TModel> queryManager)
            : base(sessionProvider)
        {
            QueryManager = queryManager;
        }

        [NotNull]
        public IQueryManager<TModel> QueryManager { get; }

        [NotNull]
        public virtual Task<int> CountAsync<TProjection>(
            [NotNull] Func<IQueryable<TModel>, IQueryable<TProjection>> lambda,
            CancellationToken cancellationToken)
            => ExecuteAsync(
                nameof(CountAsync),
                can => CountInternalAsync(() => lambda(CreateQueryable()), can),
                cancellationToken);

        [NotNull]
        protected virtual async Task<int> CountInternalAsync<TProjection>(
            [NotNull] Func<IQueryable<TProjection>> lambda,
            CancellationToken cancellationToken)
        {
            try
            {
                return
                    await lambda()
                        .CountAsync(cancellationToken)
                        .ConfigureAwait(false);
            }
            catch (EmptyResultException)
            {
                return 0;
            }
        }
    }
}
