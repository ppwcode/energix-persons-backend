using JetBrains.Annotations;

using PPWCode.Server.Core.Repositories.Implementations;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces.Providers;

namespace Energix.Persons.Server.Repositories
{
    /// <inheritdoc cref="IRelationEntityRepository" />
    public class RelationEntityRepository
        : RelationEntityRepository<long>,
          IRelationEntityRepository
    {
        public RelationEntityRepository(
            [NotNull] ISessionProviderAsync sessionProvider)
            : base(sessionProvider)
        {
        }
    }
}
