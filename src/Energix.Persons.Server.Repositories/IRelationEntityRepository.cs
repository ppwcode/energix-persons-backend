using PPWCode.Server.Core.Repositories.Interfaces;

namespace Energix.Persons.Server.Repositories
{
    /// <inheritdoc />
    public interface IRelationEntityRepository : IRelationEntityRepository<long>
    {
    }
}
