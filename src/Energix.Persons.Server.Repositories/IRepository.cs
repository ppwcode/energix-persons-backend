﻿using PPWCode.Server.Core.Models;
using PPWCode.Vernacular.NHibernate.III.Async.Interfaces;

namespace Energix.Persons.Server.Repositories
{
    /// <inheritdoc cref="IRepositoryAsync{TRoot,TId}" />
    public interface IRepository<TModel> : IRepositoryAsync<TModel, long>
        where TModel : class, IPersistentObject
    {
    }
}
