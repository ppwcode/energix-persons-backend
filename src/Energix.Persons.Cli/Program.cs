﻿using System.IO;

using Microsoft.Extensions.Configuration;

namespace Energix.Persons.Cli
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            IConfigurationRoot configuration =
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

            // Write your stuff here
        }
    }
}
