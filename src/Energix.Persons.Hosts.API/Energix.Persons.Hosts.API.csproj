<Project Sdk="Microsoft.NET.Sdk.Web">

  <Import Project="..\common.props" />

  <!-- https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-6.0#basic-tests-with-the-default-webapplicationfactory --> 
  <ItemGroup>
    <InternalsVisibleTo Include="Energix.Persons.Host.API.Tests" />
  </ItemGroup>
  
  <PropertyGroup>
    <!-- Stylecop -->
    <CodeAnalysisRuleSet>..\StyleCop.ruleset</CodeAnalysisRuleSet>

    <!-- do not generate api documents at build time (otherwise error) -->
    <OpenApiGenerateDocuments>false</OpenApiGenerateDocuments>
  </PropertyGroup>

  <ItemGroup>
    <None Remove="appsettings.json" />
    <Content Include="appsettings.json">
      <TransformOnBuild>true</TransformOnBuild>
      <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
    </Content>
    <None Update="appsettings.Development.json">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>appsettings.json</DependentUpon>
    </None>
    <None Update="appsettings.Production.json">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>appsettings.json</DependentUpon>
    </None>
    <None Update="appsettings.Staging.json">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>appsettings.json</DependentUpon>
    </None>
    <None Update="appsettings.Integration.json">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>appsettings.json</DependentUpon>
    </None>
  </ItemGroup>

  <ItemGroup>
    <None Remove="hibernate.cfg.xml" />
    <Content Include="hibernate.cfg.xml">
      <TransformOnBuild>true</TransformOnBuild>
      <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
    </Content>
    <None Update="hibernate.cfg.Development.xml">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>hibernate.cfg.xml</DependentUpon>
    </None>
    <None Update="hibernate.cfg.Staging.xml">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>hibernate.cfg.xml</DependentUpon>
    </None>
    <None Update="hibernate.cfg.Production.xml">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>hibernate.cfg.xml</DependentUpon>
    </None>
    <None Update="hibernate.cfg.Integration.xml">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>hibernate.cfg.xml</DependentUpon>
    </None>
  </ItemGroup>

  <ItemGroup>
    <None Remove="log4net.config" />
    <Content Include="log4net.config">
      <TransformOnBuild>true</TransformOnBuild>
      <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
    </Content>
    <None Update="log4net.Development.config">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>log4net.config</DependentUpon>
    </None>
    <None Update="log4net.Staging.config">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>log4net.config</DependentUpon>
    </None>
    <None Update="log4net.Production.config">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>log4net.config</DependentUpon>
    </None>
    <None Update="log4net.Integration.config">
      <IsTransformFile>True</IsTransformFile>
      <DependentUpon>log4net.config</DependentUpon>
    </None>
  </ItemGroup>

  <ItemGroup>
    <!-- ASP.Net Core / System -->
    <PackageReference Include="DateOnlyTimeOnly.AspNet.Swashbuckle" Version="1.0.3" />
    <PackageReference Include="System.IdentityModel.Tokens.Jwt" Version="6.16.0" />
    <PackageReference Include="Microsoft.AspNetCore.Mvc.Versioning" Version="5.0.0" />
    <PackageReference Include="Microsoft.AspNetCore.Mvc.Versioning.ApiExplorer" Version="5.0.0" />
    <PackageReference Include="Microsoft.Extensions.Configuration.Json" Version="6.0.0" />

    <!-- ASP.Net Core / System -->
    <PackageReference Include="ProxyKit.HttpOverrides" Version="1.1.0" />

    <!-- PPWCode -->
    <PackageReference Include="PPWCode.Vernacular.NHibernate.III.PostgreSQL" Version="[1.1.1, 2)" />
    <PackageReference Include="PPWCode.Host.Core" Version="[1.2.0, 2)" />
    <PackageReference Include="PPWCode.Host.Core.WebApi" Version="[1.2.1, 2)" />
    <PackageReference Include="PPWCode.Server.Core" Version="[1.2.0, 2)" />
    <PackageReference Include="PPWCode.Vernacular.NHibernate.III.CastleWindsor" Version="[1.1.1, 2)" />
    <PackageReference Include="PPWCode.Log4Net.Adapter" Version="1.0.0" />

    <!-- NHibernate -->
    <PackageReference Include="NHibernate.Caches.CoreMemoryCache" Version="5.7.0" />

    <!-- Logging -->
    <PackageReference Include="Common.Logging" Version="3.4.1" />
    <PackageReference Include="Microsoft.Extensions.Logging.Log4Net.AspNetCore" Version="6.1.0" />
    <PackageReference Include="Castle.LoggingFacility" Version="5.1.1" />
    <PackageReference Include="Castle.Core-log4net" Version="4.4.1" />

    <!-- SwashBuckle -->
    <PackageReference Include="Swashbuckle.AspNetCore" Version="6.2.3" />
    <PackageReference Include="Swashbuckle.AspNetCore.Newtonsoft" Version="6.2.3" />

    <!-- SlowCheetah -->
    <PackageReference Include="Microsoft.VisualStudio.SlowCheetah" Version="4.0.8" />

  </ItemGroup>

  <!-- target framework dependent libraries  -->
  <Choose>
    <When Condition=" '$(TargetFramework)' == 'netcoreapp3.1' ">
      <ItemGroup>
        <PackageReference Include="Microsoft.AspNetCore.Authentication.JwtBearer" Version="3.1.21" />
      </ItemGroup>
    </When>
    <When Condition=" '$(TargetFramework)' == 'net5.0' ">
      <ItemGroup>
        <PackageReference Include="Microsoft.AspNetCore.Authentication.JwtBearer" Version="5.0.12" />
      </ItemGroup>
    </When>
    <When Condition=" '$(TargetFramework)' == 'net6.0' ">
      <ItemGroup>
        <PackageReference Include="Microsoft.AspNetCore.Authentication.JwtBearer" Version="6.0.0" />
      </ItemGroup>
    </When>
  </Choose>

  <ItemGroup>
    <ProjectReference Include="..\Energix.Persons.Server\Energix.Persons.Server.csproj" />
  </ItemGroup>

  <Target Name="SampleDevelopmentConfigs" BeforeTargets="Build">
    <Copy Condition="!Exists('appsettings.Development.json')" SourceFiles="..\_DevelopmentSampleConfigs\API\appsettings.Development.json" DestinationFiles="appsettings.Development.json" />
    <Copy Condition="!Exists('log4net.Development.config')" SourceFiles="..\_DevelopmentSampleConfigs\API\log4net.Development.config" DestinationFiles="log4net.Development.config" />
    <Copy Condition="!Exists('hibernate.cfg.Development.xml')" SourceFiles="..\_DevelopmentSampleConfigs\API\hibernate.cfg.Development.xml" DestinationFiles="hibernate.cfg.Development.xml" />
  </Target>

  <!--
      !!! IMPORTANT NOTICE !!!
      For some reason, MSBuild does not handle SlowCheetah correctly when the tasks are not made framework specific.
      Once they are made specific for each target framework (with completely identical content), the build runs fine,
      and the tasks run both for the .net core 3.1 and the .net 6.0 framework.
  -->
  <Target Name="AppConfigGenerationAfterBuild-31" AfterTargets="Build" Condition=" '$(TargetFramework)' == 'netcoreapp3.1' ">
    <!-- initialize configs -->
    <ItemGroup>
      <ConfigToBuild Include="$(Configurations)" />
    </ItemGroup>

    <!-- Create config directories -->
    <MakeDir Directories="$(OutputPath)\configs\%(ConfigToBuild.Identity)" />

    <!-- hibernate.cfg.xml -->
    <SlowCheetah.TransformTask Source="appsettings.json" Transform="appsettings.%(ConfigToBuild.Identity).json" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\appsettings.json" />

    <!-- hibernate.cfg.xml -->
    <SlowCheetah.TransformTask Source="hibernate.cfg.xml" Transform="hibernate.cfg.%(ConfigToBuild.Identity).xml" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\hibernate.cfg.xml" />

    <!-- log4net.config -->
    <SlowCheetah.TransformTask Source="log4net.config" Transform="log4net.%(ConfigToBuild.Identity).config" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\log4net.config" />
  </Target>

  <Target Name="AppConfigGenerationAfterBuild-50" AfterTargets="Build" Condition=" '$(TargetFramework)' == 'net5.0' ">
    <!-- initialize configs -->
    <ItemGroup>
      <ConfigToBuild Include="$(Configurations)" />
    </ItemGroup>

    <!-- Create config directories -->
    <MakeDir Directories="$(OutputPath)\configs\%(ConfigToBuild.Identity)" />

    <!-- hibernate.cfg.xml -->
    <SlowCheetah.TransformTask Source="appsettings.json" Transform="appsettings.%(ConfigToBuild.Identity).json" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\appsettings.json" />

    <!-- hibernate.cfg.xml -->
    <SlowCheetah.TransformTask Source="hibernate.cfg.xml" Transform="hibernate.cfg.%(ConfigToBuild.Identity).xml" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\hibernate.cfg.xml" />

    <!-- log4net.config -->
    <SlowCheetah.TransformTask Source="log4net.config" Transform="log4net.%(ConfigToBuild.Identity).config" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\log4net.config" />
  </Target>

  <Target Name="AppConfigGenerationAfterBuild-60" AfterTargets="Build" Condition=" '$(TargetFramework)' == 'net6.0' ">
    <!-- initialize configs -->
    <ItemGroup>
      <ConfigToBuild Include="$(Configurations)" />
    </ItemGroup>

    <!-- Create config directories -->
    <MakeDir Directories="$(OutputPath)\configs\%(ConfigToBuild.Identity)" />

    <!-- hibernate.cfg.xml -->
    <SlowCheetah.TransformTask Source="appsettings.json" Transform="appsettings.%(ConfigToBuild.Identity).json" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\appsettings.json" />

    <!-- hibernate.cfg.xml -->
    <SlowCheetah.TransformTask Source="hibernate.cfg.xml" Transform="hibernate.cfg.%(ConfigToBuild.Identity).xml" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\hibernate.cfg.xml" />

    <!-- log4net.config -->
    <SlowCheetah.TransformTask Source="log4net.config" Transform="log4net.%(ConfigToBuild.Identity).config" Destination="$(OutputPath)\configs\%(ConfigToBuild.Identity)\log4net.config" />
  </Target>

  <Target Name="AppConfigGenerationAfterPublish-31" AfterTargets="Publish" Condition=" '$(TargetFramework)' == 'netcoreapp3.1' ">
    <!-- initialize configs to copy -->
    <ItemGroup>
      <ConfigToBuild Include="$(Configurations)" />
    </ItemGroup>

    <!-- copy the transformed files from the build folder -->
    <!-- the active configuration -->
    <!-- appsettings.json -->
    <Copy SourceFiles="$(OutputPath)\appsettings.json" DestinationFolder="$(PublishDir)" />

    <!-- hibernate.cfg.xml -->
    <Copy SourceFiles="$(OutputPath)\hibernate.cfg.xml" DestinationFolder="$(PublishDir)" />

    <!-- log4net.config -->
    <Copy SourceFiles="$(OutputPath)\log4net.config" DestinationFolder="$(PublishDir)" />

    <!-- the generated configurations -->
    <!-- Create config directories -->
    <MakeDir Directories="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- appsettings.json -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\appsettings.json" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- hibernate.cfg.xml -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\hibernate.cfg.xml" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- log4net.config -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\log4net.config" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />
  </Target>

  <Target Name="AppConfigGenerationAfterPublish-50" AfterTargets="Publish" Condition=" '$(TargetFramework)' == 'net5.0' ">
    <!-- initialize configs to copy -->
    <ItemGroup>
      <ConfigToBuild Include="$(Configurations)" />
      <ActiveConfigToBuild Include="$(Configuration)" />
    </ItemGroup>

    <!-- copy the transformed files from the build folder -->
    <!-- the active configuration -->
    <!-- appsettings.json -->
    <Copy SourceFiles="$(OutputPath)\appsettings.json" DestinationFolder="$(PublishDir)" />

    <!-- hibernate.cfg.xml -->
    <Copy SourceFiles="$(OutputPath)\hibernate.cfg.xml" DestinationFolder="$(PublishDir)" />

    <!-- log4net.config -->
    <Copy SourceFiles="$(OutputPath)\log4net.config" DestinationFolder="$(PublishDir)" />

    <!-- the generated configurations -->
    <!-- Create config directories -->
    <MakeDir Directories="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- appsettings.json -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\appsettings.json" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- hibernate.cfg.xml -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\hibernate.cfg.xml" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- log4net.config -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\log4net.config" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />
  </Target>

  <Target Name="AppConfigGenerationAfterPublish-60" AfterTargets="Publish" Condition=" '$(TargetFramework)' == 'net6.0' ">
    <!-- initialize configs to copy -->
    <ItemGroup>
      <ConfigToBuild Include="$(Configurations)" />
      <ActiveConfigToBuild Include="$(Configuration)" />
    </ItemGroup>

    <!-- copy the transformed files from the build folder -->
    <!-- the active configuration -->
    <!-- appsettings.json -->
    <Copy SourceFiles="$(OutputPath)\appsettings.json" DestinationFolder="$(PublishDir)" />

    <!-- hibernate.cfg.xml -->
    <Copy SourceFiles="$(OutputPath)\hibernate.cfg.xml" DestinationFolder="$(PublishDir)" />

    <!-- log4net.config -->
    <Copy SourceFiles="$(OutputPath)\log4net.config" DestinationFolder="$(PublishDir)" />

    <!-- the generated configurations -->
    <!-- Create config directories -->
    <MakeDir Directories="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- appsettings.json -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\appsettings.json" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- hibernate.cfg.xml -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\hibernate.cfg.xml" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />

    <!-- log4net.config -->
    <Copy SourceFiles="$(OutputPath)\configs\%(ConfigToBuild.Identity)\log4net.config" DestinationFolder="$(PublishDir)\configs\%(ConfigToBuild.Identity)" />
  </Target>

</Project>
