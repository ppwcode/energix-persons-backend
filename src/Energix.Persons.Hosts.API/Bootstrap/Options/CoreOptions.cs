using System;

using JetBrains.Annotations;

using Microsoft.AspNetCore.Builder;

namespace Energix.Persons.Hosts.API.Bootstrap.Options
{
    public class CoreOptions
    {
        public CoreOptions()
        {
            CorsOrigins = Array.Empty<string>();
        }

        [UsedImplicitly]
        public bool UseNHibernateProfiler { get; set; }

        [UsedImplicitly]
        public bool SuppressProfilingWhileCreatingSchema { get; set; }

        [UsedImplicitly]
        public bool CanCreateDatabase { get; set; }

        [UsedImplicitly]
        public bool CanAskAcknowledge { get; set; }

        [UsedImplicitly]
        public bool UseNHibernateSchemaExport { get; set; }

        [UsedImplicitly]
        public bool UseCors { get; set; }

        [NotNull]
        [ItemNotNull]
        [UsedImplicitly]
        public string[] CorsOrigins { get; set; }

        [UsedImplicitly]
        public ForwardedHeadersOptions ForwardedHeaders { get; set; } = new ForwardedHeadersOptions();

        [UsedImplicitly]
        public string PathMatch { get; set; }

        [UsedImplicitly]
        public bool ExecuteDatabaseResetOnly { get; set; }

        [UsedImplicitly]
        public class ForwardedHeadersOptions
        {
            [UsedImplicitly]
            public bool Active { get; set; }

            [UsedImplicitly]
            public ForwardedHeadersWithPathBaseOptions Options { get; set; } = new ForwardedHeadersWithPathBaseOptions();
        }
    }
}
