using System.Diagnostics.CodeAnalysis;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using Energix.Persons.PPW.Extensions;

using JetBrains.Annotations;

using PPWCode.Host.Core.WebApi;
using PPWCode.Server.Core.API.Exceptions;

namespace Energix.Persons.Hosts.API.Bootstrap.Installers
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class WebApiInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Register(
                    Component
                        .For<ModeFilter>()
                        .LifestyleSingleton(),
                    Component
                        .For<TransactionFilter>()
                        .LifestyleSingleton(),
                    Component
                        .For<ValidateModelActionFilter>()
                        .LifestyleSingleton(),
                    Component
                        .For<GlobalExceptionFilter>()
                        .LifestyleSingleton());

            // Chain of responsibility, order of registration is imported !
            container
                .Register(
                    Component
                        .For<IExceptionHandler>()
                        .ImplementedBy<NotImplementedExceptionHandler>()
                        .LifeStyle.Singleton,
                    Component
                        .For<IExceptionHandler>()
                        .ImplementedBy<OperationCancelledExceptionHandler>()
                        .LifeStyle.Singleton,
                    Component
                        .For<IExceptionHandler>()
                        .ImplementedBy<NotFoundExceptionHandler>()
                        .LifeStyle.Singleton,
                    Component
                        .For<IExceptionHandler>()
                        .ImplementedBy<ExternalErrorExceptionHandler>()
                        .LifeStyle.Singleton,
                    Component
                        .For<IExceptionHandler>()
                        .ImplementedBy<BadRequestExceptionHandler>()
                        .LifeStyle.Singleton,
                    Component
                        .For<IExceptionHandler>()
                        .ImplementedBy<ContractViolationExceptionHandler>()
                        .LifeStyle.Singleton,
                    Component
                        .For<IExceptionHandler>()
                        .ImplementedBy<ProgrammingErrorExceptionHandler>()
                        .LifeStyle.Singleton,
                    Component
                        .For<IExceptionHandler>()
                        .ImplementedBy<SemanticExceptionExceptionHandler>()
                        .LifeStyle.Singleton);
        }
    }
}
