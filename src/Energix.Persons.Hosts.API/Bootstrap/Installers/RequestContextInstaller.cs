using System.Diagnostics.CodeAnalysis;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using Energix.Persons.PPW.Extensions;

using JetBrains.Annotations;

namespace Energix.Persons.Hosts.API.Bootstrap.Installers
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class RequestContextInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IEnergixRequestContext, EnergixWebApiRequestContext>()
                    .ImplementedBy<EnergixWebApiRequestContext>()
                    .LifestyleScoped());
        }
    }
}
