using System.Diagnostics.CodeAnalysis;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using JetBrains.Annotations;

using PPWCode.Server.Core.Managers.Interfaces;

namespace Energix.Persons.Hosts.API.Bootstrap.Installers
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class DatabaseManagerInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IDatabaseManager>()
                    .ImplementedBy<DatabaseManager>()
                    .LifestyleTransient());
        }
    }
}
