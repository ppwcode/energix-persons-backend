using JetBrains.Annotations;

using PPWCode.Vernacular.NHibernate.III;

namespace Energix.Persons.Hosts.API.Bootstrap
{
    [UsedImplicitly]
    public class DatabaseManager : PPWCode.Server.Core.Managers.Implementations.DatabaseManager
    {
        public DatabaseManager(
            [NotNull] INHibernateSessionFactory nHibernateSessionFactory,
            [NotNull] INhConfiguration nhConfiguration,
            [NotNull] IPpwHbmMapping ppwHbmMapping)
            : base(nHibernateSessionFactory, nhConfiguration, ppwHbmMapping)
        {
        }

        /// <inheritdoc />
        protected override string GetDropAllScript()
        {
            // wat aangepast aan https://stackoverflow.com/questions/3327312/how-can-i-drop-all-the-tables-in-a-postgresql-database
            return @"
DO $$ DECLARE
    r RECORD;
BEGIN
                FOR r IN (SELECT tablename, schemaname FROM pg_tables WHERE schemaname not in ('pg_toast','pg_catalog','public','information_schema')) LOOP
                EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.schemaname)  || '.' || quote_ident(r.tablename) || ' CASCADE';
            END LOOP;
            END $$;
";
        }
    }
}
