using System.Reflection;
using Castle.Windsor;
using Common.Logging;
using Energix.Persons.Hosts.API.Bootstrap.Options;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PPWCode.Log4Net.Adapter;
using PPWCode.Server.Core.Managers.Interfaces;

namespace Energix.Persons.Hosts.API
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            IHost host = CreateHostBuilder(args);
            if (OnAfterBuildHost(host))
            {
                host.Run();
            }
        }

        public static bool OnAfterBuildHost([NotNull] IHost host)
        {
            IWindsorContainer container = host.Services.GetService<IWindsorContainer>();
            CoreOptions coreOptions = host.Services.GetService<IOptions<CoreOptions>>().Value;

            IDatabaseManager databaseManager = container.Resolve<IDatabaseManager>();
            try
            {
                databaseManager.Create(
                    coreOptions.CanCreateDatabase,
                    coreOptions.CanAskAcknowledge && !coreOptions.ExecuteDatabaseResetOnly,
                    coreOptions.UseNHibernateProfiler,
                    coreOptions.SuppressProfilingWhileCreatingSchema,
                    coreOptions.UseNHibernateSchemaExport);
            }
            finally
            {
                container.Release(databaseManager);
            }

            return !coreOptions.ExecuteDatabaseResetOnly;
        }

        public static IHost CreateHostBuilder(string[] args)
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
            LogManager.Adapter = new Log4NetLoggerFactoryAdapter(entryAssembly, null);

            return
                Microsoft.Extensions.Hosting.Host
                    .CreateDefaultBuilder(args)
                    .UsePPWWindsorContainerServiceProvider()
                    .ConfigureLogging(
                        loggingBuilder =>
                        {
                            loggingBuilder.ClearProviders();
                            Log4NetProviderOptions options =
                                new Log4NetProviderOptions
                                {
                                    Watch = true
                                };
                            loggingBuilder.AddLog4Net(options);
                        })
                    .ConfigureWebHostDefaults(hostBuilder => { hostBuilder.UseStartup<Startup>(); })
                    .Build();
        }
    }
}
