using System.Diagnostics.CodeAnalysis;
using System.Net;

using Energix.Persons.Server.PPW.Swagger;

using JetBrains.Annotations;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Hosts.API.Swagger
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class AddSemanticFaultResponseCodes : BaseResponseCodes
    {
        /// <inheritdoc />
        public override void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            OpenApiResponse response = new OpenApiResponse { Description = "Parameter validation failed or some business rules were not fulfilled." };
            ConditionalAddResponse(operation, $"{HttpStatusCode.BadRequest:D}", response);
        }
    }
}
