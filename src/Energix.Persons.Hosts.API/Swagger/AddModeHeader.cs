using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.PPW.Swagger;

using JetBrains.Annotations;

using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Hosts.API.Swagger;

/// <inheritdoc />
/// <summary>
///     Operation filter to add the custom header: <see cref="EnergixWebApiRequestContext.ModeHeader" />.
///     Header will be used to simulate tenants of the application
/// </summary>
[ExcludeFromCodeCoverage]
[UsedImplicitly]
public class AddModeHeader : BaseResponseCodes
{
    /// <inheritdoc />
    public override void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        if (IsPost(context) || IsPut(context) || IsDelete(context) || IsGet(context))
        {
            operation.Parameters ??= new List<OpenApiParameter>();
            operation.Parameters.Add(
                new OpenApiParameter
                {
                    Name = EnergixWebApiRequestContext.ModeHeader,
                    Description = "mode",
                    In = ParameterLocation.Header,
                    Schema =
                        new OpenApiSchema
                        {
                            Type = "string"
                        },
                    Required = true,
                });
        }
    }
}
