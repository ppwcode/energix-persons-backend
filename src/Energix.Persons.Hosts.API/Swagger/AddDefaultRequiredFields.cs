using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Energix.Persons.Server.PPW.Swagger;

using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Hosts.API.Swagger
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class AddDefaultRequiredFields : BaseResponseCodes
    {
        private static readonly IList<DefaultRequiredField> _requiredFieldsWithDefaultValues =
            new List<DefaultRequiredField>();

        static AddDefaultRequiredFields()
        {
            DefaultRequiredField defaultRequiredField =
                new DefaultRequiredField(
                    typeof(ApiVersion),
                    new[]
                    {
                        ((Func<ApiParameterDescription, string>, IOpenApiAny))(pd => $"{pd.ParameterDescriptor.Name}", new OpenApiString($"{Startup.DefaultApiVersion.ToString(Startup.ApiVersionFormat)}")),
                    });
            _requiredFieldsWithDefaultValues.Add(defaultRequiredField);
        }

        /// <inheritdoc />
        public override void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
            {
                return;
            }

            ReadOnlyCollection<ApiParameterDescription> parameters =
                GetParameters(context.ApiDescription)
                    .ToList()
                    .AsReadOnly();
            foreach (DefaultRequiredField defaultRequiredField in _requiredFieldsWithDefaultValues)
            {
                ApiParameterDescription parameter =
                    parameters
                        .SingleOrDefault(
                            p => (p.ParameterDescriptor != null)
                                 && defaultRequiredField.Type.IsAssignableFrom(p.ParameterDescriptor.ParameterType));
                if (parameter == null)
                {
                    continue;
                }

                foreach ((Func<ApiParameterDescription, string>, IOpenApiAny) tuple in defaultRequiredField.ParamLambdas)
                {
                    SetParamValue(operation, tuple.Item1.Invoke(parameter), tuple.Item2);
                }
            }
        }

        protected void SetParamValue(OpenApiOperation operation, string name, IOpenApiAny defaultValue)
        {
            OpenApiParameter param =
                operation
                    .Parameters
                    .SingleOrDefault(p => string.Equals(p.Name, name, StringComparison.OrdinalIgnoreCase));
            if (param is { Required: true })
            {
                param.Example = defaultValue;
            }
        }

        public class DefaultRequiredField
        {
            public DefaultRequiredField([JetBrains.Annotations.NotNull] Type type, (Func<ApiParameterDescription, string>, IOpenApiAny)[] paramLambdas)
            {
                Type = type;
                ParamLambdas = paramLambdas;
            }

            [JetBrains.Annotations.NotNull]
            public Type Type { get; }

            public (Func<ApiParameterDescription, string>, IOpenApiAny)[] ParamLambdas { get; }
        }
    }
}
