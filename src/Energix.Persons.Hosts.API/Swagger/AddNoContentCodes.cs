using System.Diagnostics.CodeAnalysis;
using System.Net;

using Energix.Persons.Server.PPW.Swagger;

using JetBrains.Annotations;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Hosts.API.Swagger
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class AddNoContentCodes : BaseResponseCodes
    {
        /// <inheritdoc />
        public override void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (IsDelete(context))
            {
                OpenApiResponse response =
                    new OpenApiResponse
                    {
                        Description = $"A {GetControllerName(context)} identified by ({GetIdentifications(context.ApiDescription)}), is found and deleted."
                    };
                ForceAddResponse(operation, $"{HttpStatusCode.NoContent:D}", response);
                RemoveResponse(operation, $"{HttpStatusCode.OK:D}");
            }
        }
    }
}
