using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;

using Energix.Persons.Server.PPW.Swagger;

using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Hosts.API.Swagger
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class AddNotFoundResponseCodes : BaseResponseCodes
    {
        /// <inheritdoc />
        public override void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            ApiDescription apiDescription = context.ApiDescription;

            bool ParameterSelector(ApiDescription description, ApiParameterDescription parameterDescription)
                => (parameterDescription.Source == BindingSource.Path)
                   && (parameterDescription.ParameterDescriptor != null)
                   && (parameterDescription.ParameterDescriptor.ParameterType != typeof(ApiVersion));

            if (GetParameters(apiDescription, ParameterSelector).Any())
            {
                string identifications = GetIdentifications(apiDescription, ParameterSelector);
                ApiResponseType responseType =
                    apiDescription
                        .SupportedResponseTypes
                        .FirstOrDefault(srt => srt.Type != typeof(void));
                string responseTypeName = responseType?.Type.Name ?? GetControllerName(context);
                OpenApiResponse response = new OpenApiResponse { Description = $"A(n) {responseTypeName} identified by ({identifications}), is not found." };
                ConditionalAddResponse(operation, $"{HttpStatusCode.NotFound:D}", response);
            }
        }
    }
}
