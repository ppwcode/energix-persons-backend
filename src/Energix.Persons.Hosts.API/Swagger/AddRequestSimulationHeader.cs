using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Energix.Persons.Server.PPW.Swagger;

using JetBrains.Annotations;
using Microsoft.OpenApi.Models;
using PPWCode.Host.Core.WebApi;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Hosts.API.Swagger
{
    /// <inheritdoc />
    /// <summary>
    ///     Operation filter to add the custom header: <see cref="TransactionFilter.RequestSimulation" />.
    ///     If this header is added, the request executed completely, but the persistent store will not be updated.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class AddRequestSimulationHeader : BaseResponseCodes
    {
        /// <inheritdoc />
        public override void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (IsPost(context) || IsPut(context) || IsDelete(context))
            {
                operation.Parameters ??= new List<OpenApiParameter>();
                operation.Parameters.Add(
                    new OpenApiParameter
                    {
                        Name = TransactionFilter.RequestSimulation,
                        Description = "Simulate Request?",
                        In = ParameterLocation.Header,
                        Schema =
                            new OpenApiSchema
                            {
                                Type = "string"
                            },
                        Required = false,
                    });
            }
        }
    }
}
