using System.Collections.Generic;

using JetBrains.Annotations;

using Microsoft.Extensions.Configuration;

using NHibernate;
using NHibernate.Cfg;

using PPWCode.Vernacular.NHibernate.III;

namespace Energix.Persons.Hosts.API.NHibernate
{
    [UsedImplicitly]
    public class NhProperties : INhProperties
    {
        public NhProperties([NotNull] IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IEnumerable<KeyValuePair<string, string>> GetProperties(Configuration configuration)
        {
            if (configuration.Properties.TryGetValue(Environment.ConnectionStringName, out string connectionStringName))
            {
                string connectionString = Configuration.GetConnectionString(connectionStringName);
                if (connectionString != null)
                {
                    yield return new KeyValuePair<string, string>(Environment.ConnectionStringName, null);
                    yield return new KeyValuePair<string, string>(Environment.ConnectionString, connectionString);
                }
            }

            yield return new KeyValuePair<string, string>(Environment.BatchSize, 0.ToString());
            yield return new KeyValuePair<string, string>(Environment.DefaultFlushMode, FlushMode.Auto.ToString());
        }
    }
}
