using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Castle.Core;
using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Conversion;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;

using Energix.Persons.Hosts.API.Bootstrap;
using Energix.Persons.Hosts.API.Bootstrap.Options;
using Energix.Persons.Hosts.API.NHibernate;
using Energix.Persons.Hosts.API.Swagger;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.API.I;
using Energix.Persons.Server.Models.PPW;
using Energix.Persons.Server.PPW.Security;
using Energix.Persons.Server.PPW.Swagger;

using HibernatingRhinos.Profiler.Appender.NHibernate;

using JetBrains.Annotations;

using log4net;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

using PPWCode.API.Core.NewtonSoft.Json;
using PPWCode.Host.Core.Extensions;
using PPWCode.Host.Core.WebApi;
using PPWCode.Server.Core.API.Exceptions;
using PPWCode.Server.Core.Managers.Implementations;
using PPWCode.Server.Core.RequestContext.Implementations;
using PPWCode.Vernacular.NHibernate.III.CastleWindsor;

using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Hosts.API
{
    public class Startup
    {
        public const string ApiName = "Energix.Persons";
        public const string ApiDescription = ApiName + " description";
        public static readonly ApiVersion DefaultApiVersion = new ApiVersion(1, 0);
        public static readonly string ApiVersionFormat = LinksContext.DefaultApiVersionFormat;
        public static readonly string ApiVersionGroupNameFormat = @"'v'" + ApiVersionFormat;
        public static readonly string CorsPolicy = nameof(CorsPolicy);

        public Startup(
            [NotNull] IConfiguration configuration,
            [NotNull] IHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
            HostEnvironment = hostEnvironment;
        }

        [NotNull]
        public IConfiguration Configuration { get; }

        [NotNull]
        public IHostEnvironment HostEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // This is executed when calling IHostBuilder.Build()
        [UsedImplicitly]
        public void ConfigureServices(IServiceCollection services)
        {
            // Check and register our options
            IConfigurationSection coreSection = Configuration.GetSection("Core");
            services.Configure<CoreOptions>(coreSection);
            CoreOptions coreOptions = new CoreOptions();
            coreSection.Bind(coreOptions);

            // setup profiling
            if (coreOptions.UseNHibernateProfiler)
            {
                NHibernateProfiler.Initialize(
                    new NHibernateAppenderConfiguration
                    {
                        IgnoreConnectionStrings = true
                    });
                AppDomain.CurrentDomain.ProcessExit += (sender, e) => NHibernateProfiler.Shutdown();
            }

            // Log4Net global vars
            GlobalContext.Properties["environment"] = HostEnvironment.EnvironmentName;
            GlobalContext.Properties["application-name"] = HostEnvironment.ApplicationName;

            // Setup our Windsor container
            IWindsorContainer container = services.CreatePPWContainer();
            services
                .AddSingleton<IControllerActivator>(new ControllerActivator(container));

            // Make HttpContext accessible using IHttpContextAccessor (singleton service)
            services.AddHttpContextAccessor();

            services
                .AddApiVersioning(options => { options.DefaultApiVersion = DefaultApiVersion; })
                .AddVersionedApiExplorer(options => options.GroupNameFormat = ApiVersionGroupNameFormat)
                .AddControllers(
                    o =>
                    {
                        o.Filters.Add(new TransactionFilterProxy<TransactionFilter>(container, 0));
                        o.Filters.Add(new ExceptionFilterProxy<GlobalExceptionFilter>(container, 0));
                        o.Filters.Add(new ModeFilterProxy<ModeFilter>(container, 1));
                    })
                .AddNewtonsoftJson(
                    options =>
                    {
                        options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                        options.SerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
                        options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Error;
                        options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                        options.SerializerSettings.Converters.Add(new ByteArrayConverter());
                        options.SerializerSettings.Converters.Add(new StringEnumConverter { AllowIntegerValues = false });
                        options.SerializerSettings.Converters.Add(new DateOnlyJsonConverter());
                    })
                .AddApplicationPart(typeof(RootController).Assembly);

            services.Configure<ApiBehaviorOptions>(
                options =>
                {
                    options.SuppressModelStateInvalidFilter = false;
                    options.SuppressMapClientErrors = true;
                });

            if (coreOptions.UseCors)
            {
                services.AddCors(
                    options =>
                    {
                        options
                            .AddPolicy(
                                CorsPolicy,
                                policyBuilder =>
                                {
                                    policyBuilder
                                        .AllowAnyHeader()
                                        .AllowAnyMethod()
                                        .AllowCredentials()
                                        .WithExposedHeaders(
                                            "Perspective",
                                            "Content-Disposition");

                                    ISet<string> origins =
                                        coreOptions
                                            .CorsOrigins
                                            .ToHashSet(StringComparer.InvariantCultureIgnoreCase);
                                    policyBuilder
                                        .SetIsOriginAllowed(origin => origins.Contains(origin));
                                });
                    });
            }

            // Authentication
            services
                .AddAuthentication(
                    options =>
                    {
                        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                .AddJwtBearer(
                    options =>
                    {
                        options.RequireHttpsMetadata = true;
                        options.SaveToken = true;
                    });
            services
                .AddScoped<IClaimsTransformation, ClaimsTransformation>();

            // Authorization Policies
            services.AddAuthorization();

            // Swagger
            services
                .AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>()
                .AddSwaggerGen()
                .AddSwaggerGenNewtonsoftSupport();
        }

        [UsedImplicitly]
        public void ConfigureContainer([NotNull] IWindsorContainer container)
        {
            container
                .AddFacilityConditionally<LoggingFacility>(
                    facility =>
                        facility
                            .LogUsing<ExtendedLog4netFactory>()
                            .ConfiguredExternally())
                .AddFacilityConditionally<TypedFactoryFacility>()
                .AddFacilityConditionally<NHibernateFacility>(
                    facility =>
                        facility
                            .UseLifestyleTypeForSessions(LifestyleType.Scoped)
                            .UseMappingAssemblies<MappingAssemblies>()
                            .UseHbmMapping<HbmMapping>()
                            .UseNhProperties<NhProperties>()
                            .UseInterceptor<AuditInterceptorWithDi>()
                            .UseTimeProvider<RequestTimeProvider>()
                            .UseConfiguration<EnergixNhConfiguration>()
                            .UseIdentityProvider<RequestIdentityProvider>())
                .AddSubResolverConditionally(c => new WebApiEnergixRequestContextResolver(c.Kernel));

            IWindsorInstaller[] installers = WindsorContainerUtilities.GetAssemblies("Energix.Persons.");
            container.Install(installers);

            IHandlerSelector[] handlerSelectors = container.ResolveAll<IHandlerSelector>();
            foreach (IHandlerSelector handlerSelector in handlerSelectors)
            {
                container
                    .Kernel
                    .AddHandlerSelector(handlerSelector);
            }

            ITypeConverter[] typeConverters = container.ResolveAll<ITypeConverter>();
            IConversionManager conversionManager = container.Kernel.GetConversionManager();
            foreach (ITypeConverter converter in typeConverters)
            {
                conversionManager.Add(converter);
                container.Release(converter);
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // This is executed when calling IHost.Run()
        [UsedImplicitly]
        public void Configure(
            [NotNull] IApplicationBuilder applicationBuilder,
            [NotNull] IWebHostEnvironment env,
            [NotNull] IApiVersionDescriptionProvider apiVersionDescriptionProvider,
            [NotNull] IOptions<CoreOptions> coreOptions)
        {
            // ReSharper disable once InconsistentNaming
            applicationBuilder.Map(
                new PathString(coreOptions.Value.PathMatch),
                innerApplicationBuilder =>
                {
                    if (coreOptions.Value.ForwardedHeaders.Active)
                    {
                        ForwardedHeadersWithPathBaseOptions options = coreOptions.Value.ForwardedHeaders.Options;

                        // Only loopback proxies are allowed by default.
                        // Clear that restriction because forwarders are enabled by explicit
                        // configuration.
                        options.KnownNetworks.Clear();
                        options.KnownProxies.Clear();
                        innerApplicationBuilder.UseForwardedHeadersWithPathBase(options);
                    }

                    if (env.IsDevelopment())
                    {
                        innerApplicationBuilder.UseDeveloperExceptionPage();
                    }

                    innerApplicationBuilder
                        .UseSwagger()
                        .UseSwaggerUI(
                            options =>
                            {
                                foreach (ApiVersionDescription description in apiVersionDescriptionProvider.ApiVersionDescriptions)
                                {
                                    options.SwaggerEndpoint(
                                        $"{coreOptions.Value.PathMatch}/swagger/{description.GroupName}/swagger.json",
                                        $"{ApiName} API {description.GroupName}");
                                }
                            });

                    innerApplicationBuilder.UseRouting();

                    if (coreOptions.Value.UseCors)
                    {
                        innerApplicationBuilder.UseCors(CorsPolicy);
                    }

                    innerApplicationBuilder
                        .UseAuthentication()
                        .UseAuthorization();

                    innerApplicationBuilder.UseEndpoints(endpoints => { endpoints.MapControllers(); });
                });
        }

        public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
        {
            private readonly IApiVersionDescriptionProvider _provider;

            public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider)
            {
                _provider = provider;
            }

            public void Configure(SwaggerGenOptions options)
            {
                options.IgnoreObsoleteActions();
                options.IgnoreObsoleteProperties();
                options.EnableAnnotations();

                options.OperationFilter<AddRequestSimulationHeader>();
                options.OperationFilter<AddModeHeader>();
                options.OperationFilter<AddNotFoundResponseCodes>();
                options.OperationFilter<AddNoContentCodes>();
                options.OperationFilter<AddDefaultRequiredFields>();
                options.OperationFilter<AddSemanticFaultResponseCodes>();
                options.OperationFilter<AddTransactionalInformation>();

                options.UseDateOnlyTimeOnlyStringConverters();

                Assembly root = Assembly.GetExecutingAssembly();
                if (root.FullName != null)
                {
                    string baseName = root.FullName.Substring(0, root.FullName.IndexOf('.'));

                    // Set the comments path for the Swagger JSON and UI.
                    List<string> xmlCommentFiles =
                        AppDomain
                            .CurrentDomain
                            .GetAssemblies()
                            .Select(a => a.GetName().Name)
                            .Where(n => n != null)
                            .Where(n => n.StartsWith(baseName, StringComparison.OrdinalIgnoreCase))
                            .Where(n => n.Contains("Server") || n.Contains("API"))
                            .Select(n => n + ".xml")
                            .ToList();
                    foreach (string xmlCommentFile in xmlCommentFiles)
                    {
                        string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlCommentFile);
                        options.IncludeXmlComments(xmlPath);
                    }
                }

                foreach (ApiVersionDescription description in _provider.ApiVersionDescriptions)
                {
                    options
                        .SwaggerDoc(
                            description.GroupName,
                            new OpenApiInfo
                            {
                                Version = description.ApiVersion.ToString(),
                                Title = $"{ApiName} API {description.ApiVersion}",
                                Description = $"{ApiDescription}",
                                Contact =
                                    new OpenApiContact
                                    {
                                        Name = "Peopleware",
                                        Email = "info@peopleware.be",
                                        Url = new Uri("https://www.peopleware.be")
                                    }
                            });
                }

                options
                    .AddSecurityDefinition(
                        JwtBearerDefaults.AuthenticationScheme,
                        new OpenApiSecurityScheme
                        {
                            In = ParameterLocation.Header,
                            Description = $"Insert JWT, prefixed with {JwtBearerDefaults.AuthenticationScheme}",
                            Name = "Authorization",
                            Type = SecuritySchemeType.ApiKey
                        });
                options
                    .AddSecurityRequirement(
                        new OpenApiSecurityRequirement
                        {
                            {
                                new OpenApiSecurityScheme
                                {
                                    Reference =
                                        new OpenApiReference
                                        {
                                            Type = ReferenceType.SecurityScheme,
                                            Id = JwtBearerDefaults.AuthenticationScheme,
                                        },
                                    Scheme = "oauth2",
                                    Name = JwtBearerDefaults.AuthenticationScheme,
                                    In = ParameterLocation.Header,
                                },
                                new string[] { }
                            },
                        });
            }
        }
    }
}
