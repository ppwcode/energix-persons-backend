﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.BusinessLogic.Tests.Common;
using Energix.Persons.Server.Models;
using Energix.Persons.Server.Models.PPW.Components;

using NUnit.Framework;

using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.BusinessLogic.Tests;

public class FunctionallyTheSameTests : BaseTests
{
    private Type _isFunctionallyTheSame = null;

    public FunctionallyTheSameTests()
    {
        _isFunctionallyTheSame = typeof(IFunctionalEqualityComparer<>);
    }

    /// <summary>
    /// creates dummy objects with two value variants for each property
    /// check if comparing all the variants of the properties, the FunctionalEquality still holds
    /// </summary>
    [Test]
    public void CheckAllPropertiesForAllClassesAreOk()
    {
        var functionallyTheSameClasses = typeof(AffiliateProperties).Assembly
            .GetTypes()
            .Where(x => !x.IsInterface)
            .Where(x => !x.IsAbstract)
            .Where(x => x.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == _isFunctionallyTheSame))
            .Where(x => x.GetInterfaces().All(i => !(i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IFunctionalEqualityItemsContainer<>))));

        Assert.IsNotEmpty(functionallyTheSameClasses);
        foreach (Type entity in functionallyTheSameClasses)
        {
            TestContext.WriteLine($"Checking {entity}");

            var instance1 = Activator.CreateInstance(entity);
            SetPropertiesWithValue(instance1, true);

            var methodInfo = entity.GetMethod(nameof(IFunctionalEqualityComparer<bool>.IsFunctionallyTheSame));
            var sameObject = methodInfo.Invoke(instance1, new object[] { instance1 }) as bool?;
            Assert.IsTrue(sameObject);

            var instance2 = Activator.CreateInstance(entity);
            SetPropertiesWithValue(instance2, false);

            var diffObject = methodInfo.Invoke(instance1, new object[] { instance2 }) as bool?;
            Assert.IsFalse(diffObject);

            foreach (var propertyInfo in GetRelevantPropertyInfos(instance1))
            {
                try
                {
                    var individualProp = Activator.CreateInstance(entity);
                    SetPropertiesWithValue(individualProp, true); // fill all props with default
                    var safeValue = GetValue(false, propertyInfo, out var canBeIgnored);
                    if (canBeIgnored)
                    {
                        continue;
                    }

                    propertyInfo?.SetValue(individualProp, safeValue); // change one prop
                    var diffPropObject = methodInfo.Invoke(instance1, new object[] { individualProp }) as bool?;
                    if (diffPropObject ?? true)
                    {
                        TestContext.WriteLine($"Something wrong with {entity} and property {propertyInfo.Name}");
                    }

                    Assert.IsFalse(diffPropObject);
                }
                catch (Exception)
                {
                    TestContext.WriteLine($"Something wrong with {entity} and property {propertyInfo.Name}");
                    throw;
                }
            }
        }
    }

    private List<PropertyInfo> GetRelevantPropertyInfos(object e)
    {
        var toIgnorePropNames = new List<string> { nameof(IHistoricPersistentObject.ValidFrom), nameof(IHistoricPersistentObject.ValidTo) };
        toIgnorePropNames.AddRange(typeof(AuditablePersistentObject<>).GetProperties().Select(x => x.Name).ToList());
        var props = e.GetType().GetProperties();

        if (e is IMode)
        {
            toIgnorePropNames.Add(nameof(IMode.Mode));
        }

        return props.Where(p => !toIgnorePropNames.Contains(p.Name)).ToList();
    }

    private void SetPropertiesWithValue(object e, bool value)
    {
        var relevantProps = GetRelevantPropertyInfos(e);

        foreach (var propertyInfo in relevantProps)
        {
            try
            {
                var safeValue = GetValue(value, propertyInfo, out var _);
                propertyInfo?.SetValue(e, safeValue);
            }
            catch (Exception)
            {
                TestContext.WriteLine($"Failed to set {propertyInfo.Name} for {e.GetType().Name} to {value}");
                throw;
            }
        }
    }

    private object GetValue(bool value, PropertyInfo propertyInfo, out bool canBeIgnored)
    {
        Type t = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;

        object safeValue = null;
        var boolDate = new DateTime(2022, 1, value ? 1 : 2);
        var defaultValueForTypeMapping = new Dictionary<Type, Action>
                                         {
                                             { typeof(Interval), () => safeValue = new Interval(boolDate, boolDate) },
                                             { typeof(string), () => safeValue = value ? "1" : "2" },
                                             { typeof(DateTime), () => safeValue = boolDate },
                                             { typeof(DateOnly), () => safeValue = new DateOnly(2022, 1, value ? 1 : 2) }
                                         };
        if (defaultValueForTypeMapping.TryGetValue(t, out var setValueAction))
        {
            setValueAction();
            canBeIgnored = false;
            return safeValue;
        }

        if (t.IsEnum)
        {
            var enumValues = Enum.GetValues(t);
            Assert.True(enumValues.Length > 1);
            safeValue = value ? enumValues.GetValue(0) : enumValues.GetValue(1);
            canBeIgnored = false;
            return safeValue;
        }

        if (t.GetInterfaces()
            .Where(x => x.IsGenericType)
            .Select(x => x.GetGenericTypeDefinition())
            .Any(x => x == _isFunctionallyTheSame || x == typeof(IEquatable<>)))
        {
            canBeIgnored = true;
            return null;
        }

        if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(ISet<>))
        {
            // ignore nested, create empty list
            var d1 = typeof(HashSet<>);
            Type[] typeArgs = t.GenericTypeArguments;
            var makeme = d1.MakeGenericType(typeArgs);
            object o = Activator.CreateInstance(makeme);
            canBeIgnored = true;
            return o;
        }

        safeValue = Convert.ChangeType(value, t);
        canBeIgnored = false;
        return safeValue;
    }
}
