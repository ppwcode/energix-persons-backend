﻿using Energix.Persons.Server.BusinessLogic.Tests.Common.Sut;
using JetBrains.Annotations;
using NUnit.Framework;
using PPWCode.API.Core.Exceptions;

namespace Energix.Persons.Server.BusinessLogic.Tests.Common
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public abstract class BaseTests
    {
        [SetUp]
        public void Setup()
        {
            OnSetup();
        }

        [TearDown]
        public void TearDown()
        {
            OnTearDown();
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            OnOneTimeSetup();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            OnOneTimeTearDown();
        }

        protected long NextId
            => IdGenerator.NextId;

        protected virtual void OnOneTimeTearDown()
        {
        }

        protected virtual void OnOneTimeSetup()
        {
        }

        protected virtual void OnSetup()
        {
        }

        protected virtual void OnTearDown()
        {
        }

        protected void AssertInternalProgrammingErrorExceptionAsync([NotNull] AsyncTestDelegate lambda, [CanBeNull] string message, params object[] parmas)
            => Assert.ThrowsAsync<InternalProgrammingError>(lambda, message, parmas);

        protected void AssertInternalProgrammingErrorException([NotNull] TestDelegate lambda, [CanBeNull] string message, params object[] parmas)
            => Assert.Throws<InternalProgrammingError>(lambda, message, parmas);
    }
}
