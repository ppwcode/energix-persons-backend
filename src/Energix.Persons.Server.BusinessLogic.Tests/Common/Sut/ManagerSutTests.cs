using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Energix.Persons.Server.Repositories;
using JetBrains.Annotations;
using Moq;
using NUnit.Framework;
using PPWCode.Server.Core.Managers.Interfaces;
using PPWCode.Server.Core.Models;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.BusinessLogic.Tests.Common.Sut
{
    public abstract class ManagerSutTests<TManager> : SutTests<TManager>
        where TManager : class, IManager
    {
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();

        /// <inheritdoc />
        protected override void OnTearDown()
        {
            _repositories.Clear();

            base.OnTearDown();
        }

        protected void CheckPersistentObject([NotNull] IPersistentObject<long> persistentObject)
        {
            Assert.That(persistentObject.IsTransient, Is.False);
            CheckCivilized(persistentObject);
        }

        [NotNull]
        protected Mock<TRepository> CheckAndRegisterPersistentObject<TRepository, TModel>([CanBeNull] TModel model)
            where TRepository : class, IRepository<TModel>
            where TModel : class, IPersistentObject
        {
            if (model != null)
            {
                CheckPersistentObject(model);
                GetRepositoryMock<TRepository, TModel>()
                    .Setup(r => r.GetByIdAsync(model.Id, CancellationToken.None))
                    .Returns(Task.FromResult(model));
            }

            return Container.Resolve<Mock<TRepository>>();
        }

        protected Mock<TRepository> GetRepositoryMock<TRepository, TModel>()
            where TRepository : class, IRepository<TModel>
            where TModel : class, IPersistentObject
        {
            Mock<TRepository> result;
            if (!_repositories.ContainsKey(typeof(TRepository)))
            {
                result = Container.Resolve<Mock<TRepository>>();
                _repositories.Add(typeof(TRepository), result);
            }
            else
            {
                result = (Mock<TRepository>)_repositories[typeof(TRepository)];
            }

            return result;
        }

        protected virtual string TestUserName
            => "test-user";

        protected void SetIdAndAuditFields(IPersistentObject<long> persistentObject, long? id = null)
            => persistentObject.SetIdAndAuditFields(UtcNow, TestUserName, id);
    }
}
