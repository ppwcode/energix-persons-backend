﻿using System;

using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.ModelBuilder;
using Castle.MicroKernel.Registration;

using JetBrains.Annotations;

namespace Energix.Persons.Server.BusinessLogic.Tests.Common.Sut
{
    public class SutTestFacility : AbstractTestFacility
    {
        public Type SutType { get; set; }
        public bool RegisterSutType { get; set; }
        public LifestyleType? ScopedTo { get; set; }

        protected override void Init()
        {
            base.Init();

            Kernel.ComponentModelBuilder.AddContributor(new LifestyleChanger(ScopedTo));
            Kernel.Resolver.AddSubResolver(new AutoMoqResolver(Kernel));
            if ((SutType != null) && RegisterSutType)
            {
                Kernel.Register(Component.For(SutType).LifestyleSingleton());
            }
        }

        public class LifestyleChanger : IContributeComponentModelConstruction
        {
            public LifestyleChanger(LifestyleType? scopedTo)
            {
                ScopedTo = scopedTo;
            }

            public LifestyleType? ScopedTo { get; }

            public void ProcessModel([NotNull] IKernel kernel, [NotNull] ComponentModel model)
            {
                if ((model.LifestyleType == LifestyleType.Scoped) && (ScopedTo != null))
                {
                    model.LifestyleType = ScopedTo.Value;
                }
            }
        }
    }
}
