using System;
using System.Reflection;
using Energix.Persons.Server.BusinessLogic.Tests.Common.Sut;
using JetBrains.Annotations;
using PPWCode.API.Core.Exceptions;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.BusinessLogic.Tests.Common
{
    public static class PersistentObjectUtils
    {
        private static void SetField([NotNull] object target, [NotNull] string fieldName, [NotNull] object value)
        {
            Type type = target.GetType();
            FieldInfo fieldInfo = null;

            while (type != null)
            {
                fieldInfo = type.GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic);

                if (fieldInfo != null)
                {
                    break;
                }

                type = type.BaseType;
            }

            if (fieldInfo != null)
            {
                fieldInfo.SetValue(target, value);
            }
        }

        /// <summary>
        ///     Changes the given object's (<paramref name="persistentObject" />) persistence id
        ///     using the given value (<paramref name="value" />).
        /// </summary>
        /// <param name="persistentObject">the object that must be changed</param>
        /// <param name="value">the value for the persistence id</param>
        /// <remarks>
        ///     If the given object <paramref name="persistentObject" /> is null, the
        ///     method will throw a <see cref="InternalProgrammingError" />.
        /// </remarks>
        public static void SetPersistentId(this IPersistentObject<long> persistentObject, long value)
        {
            if (persistentObject == null)
            {
                throw new InternalProgrammingError("Cannot set persistence id on null object.");
            }

            SetField(persistentObject, "_id", value);
        }

        public static void SetIdAndAuditFields(
            this IPersistentObject<long> persistentObject,
            DateTime utcNow,
            [NotNull] string userName,
            [CanBeNull] long? id)
        {
            if (persistentObject != null)
            {
                if (persistentObject.IsTransient)
                {
                    persistentObject.SetPersistentId(id ?? IdGenerator.NextId);
                    if (persistentObject is IInsertAuditable insertAuditable)
                    {
                        insertAuditable.CreatedAt = utcNow;
                        insertAuditable.CreatedBy = userName;
                    }
                }
                else
                {
                    if (persistentObject is IUpdateAuditable updateAuditable)
                    {
                        updateAuditable.LastModifiedAt = utcNow;
                        updateAuditable.LastModifiedBy = userName;
                    }
                }
            }
        }
    }
}
