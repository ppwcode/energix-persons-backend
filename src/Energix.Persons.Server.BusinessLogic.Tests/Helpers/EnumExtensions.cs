using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using PPWCode.Vernacular.Exceptions.IV;

namespace Energix.Persons.Server.BusinessLogic.Tests.Helpers
{
    public static class EnumExtensions
    {
        public static T Parse<T>([NotNull] this string value, bool ignoreCase = false)
            where T : struct
            => (T)Enum.Parse(typeof(T), value, ignoreCase);

        public static T Parse<T>(this int value)
            where T : struct
        {
            if (Enum.IsDefined(typeof(T), value))
            {
                return (T)Enum.ToObject(typeof(T), value);
            }

            throw new ProgrammingError($"{typeof(T).FullName} is not a defined enumeration type.");
        }

        public static T? TryParse<T>(this string value, bool ignoreCase, T? fallbackValue = null)
            where T : struct
        {
            if (typeof(T).IsEnum)
            {
                return
                    value != null
                        ? Enum.TryParse(value, ignoreCase, out T @enum) ? @enum : fallbackValue
                        : fallbackValue;
            }

            throw new ProgrammingError($"{typeof(T).FullName} is not a defined enumeration type.");
        }

        public static T? TryParse<T>(this string value, T? fallbackValue)
            where T : struct
            => value.TryParse(true, fallbackValue);

        public static T? TryParse<T>(this int value, T? fallbackValue)
            where T : struct
            => Enum.IsDefined(typeof(T), value) ? (T?)Enum.ToObject(typeof(T), value) : fallbackValue;

        public static IEnumerable<T> GetValues<T>()
            where T : struct
            => Enum.GetValues(typeof(T)).Cast<T>();
    }
}
