﻿using System;

using Castle.MicroKernel.Registration;

namespace Energix.Persons.Server.Repositories.Tests.Common.Sut
{
    public class SutTestFacility : AbstractTestFacility
    {
        public Type SutType { get; set; }

        protected override void Init()
        {
            base.Init();

            Kernel.Resolver.AddSubResolver(new AutoMoqResolver(Kernel));
            if (SutType != null)
            {
                Kernel.Register(Component.For(SutType).LifestyleSingleton());
            }
        }
    }
}
