using System;

using Energix.Persons.Server.Models.PPW.Components;

namespace Energix.Persons.Server.Models.PPW.Utils
{
    public static class DateTimeExtensions
    {
        /// <summary>
        ///     Helper method to work with <see cref="IInterval" />.
        ///     If the <see cref="IInterval.Start" /> is <code>null</code> that
        ///     represents minus infinity. For calculations that value can
        ///     be mapped to <see cref="DateTime.MinValue" /> using this method.
        /// </summary>
        /// <param name="datetime">the given time</param>
        /// <returns>
        ///     original <paramref name="datetime" /> if not <code>null</code>,
        ///     and <see cref="DateTime.MinValue" /> otherwise
        /// </returns>
        public static DateTime CoalesceStartValue(this DateTime? datetime)
            => datetime ?? DateTime.MinValue;

        /// <summary>
        ///     Helper method to work with <see cref="IInterval" />.
        ///     If the <see cref="IInterval.End" /> is <code>null</code> that
        ///     represents plus infinity. For calculations that value can
        ///     be mapped to <see cref="DateTime.MaxValue" /> using this method.
        /// </summary>
        /// <param name="datetime">the given time</param>
        /// <returns>
        ///     original <paramref name="datetime" /> if not <code>null</code>,
        ///     and <see cref="DateTime.MaxValue" /> otherwise
        /// </returns>
        public static DateTime CoalesceEndValue(this DateTime? datetime)
            => datetime ?? DateTime.MaxValue;
    }
}
