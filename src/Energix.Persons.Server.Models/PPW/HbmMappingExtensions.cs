using System.Diagnostics.CodeAnalysis;

using JetBrains.Annotations;

namespace Energix.Persons.Server.Models.PPW
{
    [ExcludeFromCodeCoverage]
    public static class HbmMappingExtensions
    {
        private static readonly HbmMapping _hbmMapping =
            new HbmMapping(new MappingAssemblies());

        public static string GetIdentifier([CanBeNull] this string identifier, bool? quoteIdentifier = null)
            => string.IsNullOrEmpty(identifier)
                   ? null
                   : _hbmMapping.ConditionalQuoteIdentifier(_hbmMapping.GetIdentifier(identifier), quoteIdentifier);

        public static string GetPrimaryColumnName([CanBeNull] this string tableName, bool? quoteIdentifier = null)
            => string.IsNullOrEmpty(tableName)
                   ? null
                   : _hbmMapping.GetPrimaryColumnName(tableName, quoteIdentifier);

        public static string GetForeignColumnName([CanBeNull] this string tableName, bool? quoteIdentifier = null)
            => string.IsNullOrEmpty(tableName)
                   ? null
                   : _hbmMapping.GetForeignColumnName(tableName, quoteIdentifier);
    }
}
