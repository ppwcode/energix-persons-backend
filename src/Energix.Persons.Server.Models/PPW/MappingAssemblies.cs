using System.Collections.Generic;
using System.Reflection;

using JetBrains.Annotations;

using PPWCode.Vernacular.NHibernate.III;

namespace Energix.Persons.Server.Models.PPW
{
    [UsedImplicitly]
    public class MappingAssemblies : IMappingAssemblies
    {
        public IEnumerable<Assembly> GetAssemblies()
        {
            yield return GetType().Assembly;
        }
    }
}
