using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models.CustomMapping;

using JetBrains.Annotations;

using NHibernate.Dialect;
using NHibernate.Linq;
using NHibernate.Mapping.ByCode;

using PPWCode.Util.Validation.III;
using PPWCode.Vernacular.NHibernate.III;
using PPWCode.Vernacular.NHibernate.III.MappingByCode;

namespace Energix.Persons.Server.Models.PPW
{
    [UsedImplicitly]
    [ExcludeFromCodeCoverage]
    public class HbmMapping : SimpleModelMapper
    {
        // NEVER CHANGE THIS
        public const string GeneratorTableName = "HIBERNATE_HI_LO";
        public const string GeneratorNextHiColumnName = "NEXT_HI";
        public const string GeneratorEntityNameColumnName = "ENTITY_NAME";
        public const string GeneratorTableNameColumnName = "TABLE_NAME";
        public const int GeneratorMaxLo = 999;
        public const int BatchSize = 320;

        public HbmMapping([JetBrains.Annotations.NotNull] IMappingAssemblies mappingAssemblies)
            : base(mappingAssemblies)
        {
        }

        /// <inheritdoc />
        protected override int? ClassBatchSize
            => BatchSize;

        /// <inheritdoc />
        protected override int? CollectionBatchSize
            => ClassBatchSize;

        /// <inheritdoc />
        protected override bool DynamicInsert
            => false;

        /// <inheritdoc />
        protected override bool DynamicUpdate
            => true;

        /// <inheritdoc />
        protected override bool AdjustColumnForForeignGenerator
            => true;

        /// <inheritdoc />
        public override bool UseCamelCaseUnderScoreForDbObjects
            => true;

        /// <inheritdoc />
        public override string CamelCaseToUnderscore(string camelCase)
        {
            // fix the HI LO special tables ...
            if (camelCase.All(x => char.IsUpper(x) || x == '_'))
            {
                return camelCase.ToLowerInvariant();
            }

            // IMHO the default implementation didn't changed Affiliate to affiliate, making it required for using quotes in postgreSQL
            var res = string.Concat(camelCase.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString())).ToLower();
            return res;
        }

        /// <inheritdoc />
        public override bool QuoteIdentifiers
            => true;

        /// <inheritdoc />
        protected override string DefaultSchemaName
            => "dbo";

        /// <inheritdoc />
        protected override bool MapDateTimeAsUtc
            => true;

        /// <inheritdoc />
        protected override bool MapEnumsAsString
            => true;

        /// <inheritdoc />
        protected override bool UseEnumNamesLength
            => true;

        /// <inheritdoc />
        protected override void OnBeforeMapClass(IModelInspector modelInspector, Type type, IClassAttributesMapper classCustomizer)
        {
            base.OnBeforeMapClass(modelInspector, type, classCustomizer);

            classCustomizer
                .Id(
                    idMapper =>
                    {
                        idMapper.Generator(
                            Generators.HighLow,
                            generatorMapper =>
                                generatorMapper.Params(
                                    new
                                    {
                                        table = GeneratorTableName,
                                        column = GeneratorNextHiColumnName,
                                        max_lo = GeneratorMaxLo,
                                        where = $"{GeneratorEntityNameColumnName} = '{type.FullName}'"
                                    }));
                    });

            // Energix specific
            if (typeof(IMode).IsAssignableFrom(type))
            {
                classCustomizer.Filter(NhModeFilterHelper.Name, mapper => { });
            }
        }

        /// <inheritdoc />
        protected override void OnBeforeMapProperty(IModelInspector modelInspector, PropertyPath member, IPropertyMapper propertyCustomizer)
        {
            base.OnBeforeMapProperty(modelInspector, member, propertyCustomizer);

            Type memberType = member.MemberType();
            if (typeof(AbstractIdentification).IsAssignableFrom(memberType))
            {
                AbstractIdentification identification = Activator.CreateInstance(memberType, string.Empty) as AbstractIdentification;
                if (identification != null)
                {
                    propertyCustomizer.Length(identification.StandardMaxLength);
                }
            }

            // Energix specific
            if (member.LocalMember.GetPropertyOrFieldType() == typeof(DateOnly))
            {
                propertyCustomizer.Type<DateOnlyUserType>();
            }
        }

        public string GetPrimaryColumnName(string tableName, bool? quoteIdentifiers = null)
            => GetKeyColumnName(PrimaryKeyType, tableName, quoteIdentifiers ?? QuoteIdentifiers);

        public string GetForeignColumnName(string tableName, bool? quoteIdentifiers = null)
            => GetKeyColumnName(ForeignKeyType, tableName, quoteIdentifiers ?? QuoteIdentifiers);
    }

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "NHibernate")]
    [UsedImplicitly]
    [ExcludeFromCodeCoverage]
    public class AbvvNewHighLowPerTableAuxiliaryDatabaseObject
        : HighLowPerTableAuxiliaryDatabaseObject
    {
        public AbvvNewHighLowPerTableAuxiliaryDatabaseObject(IPpwHbmMapping ppwHbmMapping)
            : base(ppwHbmMapping)
        {
        }

        protected override string GeneratorTableName
            => HbmMapping.GeneratorTableName;

        protected override string GeneratorEntityNameColumnName
            => HbmMapping.GeneratorEntityNameColumnName;

        protected override string GeneratorNextHiColumnName
            => HbmMapping.GeneratorNextHiColumnName;

        protected override string GeneratorTableNameColumnName
            => HbmMapping.GeneratorTableNameColumnName;

        /// <inheritdoc />
        protected override int GeneratorEntityNameColumnLength(Dialect dialect)
            => 255;

        /// <inheritdoc />
        protected override int GeneratorTableNameColumnLength(Dialect dialect)
            => dialect.MaxAliasLength;
    }
}
