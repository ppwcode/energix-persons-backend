using System.Diagnostics.CodeAnalysis;

using JetBrains.Annotations;

using PPWCode.Vernacular.NHibernate.III.MappingByCode;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Models.PPW
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class AuditLog : AuditLog<long>
    {
        [UsedImplicitly]
        public class AuditLogMapper : AuditLogMapper<AuditLog, long>
        {
        }
    }
}
