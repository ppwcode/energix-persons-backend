using System;
using System.Diagnostics;

using JetBrains.Annotations;

namespace Energix.Persons.Server.Models.PPW.Components
{
    public class IntervalBuilder
    {
        private DateTime? _end;
        private DateTime? _start;

        public IntervalBuilder Merge([CanBeNull] IInterval interval)
        {
            if (interval != null)
            {
                _start = interval.Start;
                _end = interval.End;
            }

            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public IntervalBuilder Start(DateTime? start)
        {
            _start = start;
            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public IntervalBuilder End(DateTime? end)
        {
            _end = end;
            return this;
        }

        public Interval Build()
            => this;

        [DebuggerStepThrough]
        [NotNull]
        public static implicit operator Interval(IntervalBuilder builder)
            => new Interval(builder._start, builder._end);
    }
}
