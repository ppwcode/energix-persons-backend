﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Text;

using Energix.Persons.API;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode.Conformist;

using PPWCode.Vernacular.NHibernate.III;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Models.PPW.Components
{
    public class Address
        : CivilizedObject,
          IEquatable<Address>
    {
        private readonly string _box;
        private readonly string _city;
        private readonly string _country;
        private readonly string _houseNumber;
        private readonly string _street;
        private readonly string _zipCode;

        [UsedImplicitly]
        private Address()
        {
        }

        public Address(
            string street,
            string houseNumber,
            string box,
            string zipCode,
            string city,
            string country)
        {
            _street = street;
            _houseNumber = houseNumber;
            _box = box;
            _zipCode = zipCode;
            _city = city;
            _country = country;
        }

        [Required]
        [CanBeNull]
        [StringLength(FieldLengths.AddressStreetLength)]
        public virtual string Street
            => _street;

        [Required]
        [CanBeNull]
        [StringLength(FieldLengths.AddressHouseNumberLength)]
        public virtual string HouseNumber
            => _houseNumber;

        [CanBeNull]
        [StringLength(FieldLengths.AddressBoxLength)]
        public virtual string Box
            => _box;

        [Required]
        [CanBeNull]
        [StringLength(FieldLengths.AddressZipCodeLength)]
        public virtual string ZipCode
            => _zipCode;

        [Required]
        [CanBeNull]
        [StringLength(FieldLengths.AddressCityLength)]
        public virtual string City
            => _city;

        [CanBeNull]
        [StringLength(FieldLengths.CountryLength)]
        public virtual string Country
            => _country;

        public virtual string FullStreet
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(Street))
                {
                    sb.Append(Street.Trim());
                }

                if (!string.IsNullOrWhiteSpace(HouseNumber))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" ");
                    }

                    sb.Append(HouseNumber.Trim());
                }

                if (!string.IsNullOrWhiteSpace(Box))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" ");
                    }

                    sb.Append(" ").Append(Box.Trim());
                }

                return sb.ToString();
            }
        }

        public virtual string FullCity
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(ZipCode))
                {
                    sb.Append(ZipCode.Trim());
                }

                if (!string.IsNullOrWhiteSpace(City))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(" ");
                    }

                    sb.Append(City.Trim());
                }

                return sb.ToString();
            }
        }

        public bool Equals(Address other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return (GetType() == other.GetType())
                   && string.Equals(Street, other.Street)
                   && string.Equals(HouseNumber, other.HouseNumber)
                   && string.Equals(Box, other.Box)
                   && string.Equals(ZipCode, other.ZipCode)
                   && string.Equals(City, other.City)
                   && (Country == other.Country);
        }

        public IEnumerable<PpwAuditLog> GetMultiLogs(string propertyName)
        {
            yield return new PpwAuditLog($"{propertyName}.{nameof(Street)}", Street);
            yield return new PpwAuditLog($"{propertyName}.{nameof(HouseNumber)}", HouseNumber);
            yield return new PpwAuditLog($"{propertyName}.{nameof(Box)}", Box);
            yield return new PpwAuditLog($"{propertyName}.{nameof(ZipCode)}", ZipCode);
            yield return new PpwAuditLog($"{propertyName}.{nameof(City)}", City);
            yield return new PpwAuditLog($"{propertyName}.{nameof(Country)}", Country);
        }

        public PpwAuditLog GetSingleLog(string propertyName)
            => null;

        public bool IsMultiLog
            => true;

        public override bool Equals(object obj)
            => Equals(obj as Address);

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Street != null ? Street.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (HouseNumber != null ? HouseNumber.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Box != null ? Box.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ZipCode != null ? ZipCode.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (City != null ? City.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Country != null ? Country.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Address left, Address right)
            => Equals(left, right);

        public static bool operator !=(Address left, Address right)
            => !Equals(left, right);

        [ExcludeFromCodeCoverage]
        [UsedImplicitly]
        public class AddressComponentMapper : ComponentMapping<Address>
        {
            public AddressComponentMapper()
            {
                Property(a => a.Street);
                Property(a => a.HouseNumber);
                Property(a => a.Box);
                Property(a => a.ZipCode);
                Property(a => a.City);
                Property(p => p.Country);
            }
        }
    }
}
