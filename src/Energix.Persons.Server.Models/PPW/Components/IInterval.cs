using System;

using JetBrains.Annotations;

using PPWCode.API.Core.Exceptions;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Models.PPW.Components
{
    /// <summary>
    ///     Represents an period in time, specified using a <see cref="Start" />
    ///     time (inclusive) and a <see cref="End" /> time (exclusive).
    /// </summary>
    public interface IInterval : ICivilizedObject
    {
        /// <summary>
        ///     The start date of the period. The start is always inclusive.
        /// </summary>
        /// <remarks>The value <code>null</code> means infinity in the past.</remarks>
        [CanBeNull]
        DateTime? Start { get; }

        /// <summary>
        ///     The end date of the period. The end is always exclusive.
        /// </summary>
        /// <remarks>The value <code>null</code> means infinity in the future. </remarks>
        [CanBeNull]
        DateTime? End { get; }

        /// <summary>
        ///     Check whether a given period overlaps with the current one.
        /// </summary>
        /// <param name="other">The period to check any overlaps for.</param>
        /// <returns>
        ///     If other == null 'false' will be returned,
        ///     otherwise the period will be checked for overlapping.
        /// </returns>
        /// <exception cref="InternalProgrammingError">
        ///     When one or both Periods are not 'civilized'.
        /// </exception>
        bool Overlaps([CanBeNull] IInterval other);

        /// <summary>
        ///     Check whether a given period lies completely with the current one.
        /// </summary>
        /// <param name="other">The period to check.</param>
        /// <returns>
        ///     If other == null, 'false' will be returned,
        ///     otherwise the period will be checked for overlapping.
        /// </returns>
        /// <exception cref="InternalProgrammingError">
        ///     When one or both Periods are not 'civilized'.
        /// </exception>
        bool Contains([CanBeNull] IInterval other);

        /// <summary>
        ///     Check if <paramref name="dt" /> is within our period.
        /// </summary>
        /// <param name="dt">Point in time to check.</param>
        /// <returns>
        ///     <para><c>true</c> if <paramref name="dt" /> is within our period.</para>
        ///     <para><c>false</c> if <paramref name="dt" /> is <c>not</c> within our period.</para>
        /// </returns>
        /// <exception cref="InternalProgrammingError">
        ///     When the Interval is not 'civilized'.
        /// </exception>
        bool Contains(DateTime dt);

        /// <summary>
        ///     A closed interval, is an interval were <see cref="End" /> is not equal to <c>null</c>.
        /// </summary>
        /// <returns>
        ///     Returns <c>true</c> if the interval is closed.
        /// </returns>
        bool IsClosed();
    }
}
