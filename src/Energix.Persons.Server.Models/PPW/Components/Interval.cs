using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

using Energix.Persons.Server.Models.PPW.Utils;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode.Conformist;

using PPWCode.API.Core.Exceptions;
using PPWCode.Vernacular.Exceptions.IV;
using PPWCode.Vernacular.NHibernate.III;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Models.PPW.Components
{
    public class Interval
        : CivilizedObject,
          IPpwAuditLog,
          IEquatable<Interval>,
          IInterval
    {
        private readonly DateTime? _end;
        private readonly DateTime? _start;

        [UsedImplicitly]
        protected Interval()
        {
        }

        public Interval(DateTime? start, DateTime? end)
        {
            _start = start;
            _end = end;
        }

        public virtual string TimeFormatMask
            => "s";

        public bool Equals(Interval other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return (GetType() == other.GetType())
                   && Start.Equals(other.Start)
                   && End.Equals(other.End);
        }

        [Required]
        public DateTime? Start
            => _start;

        public DateTime? End
            => _end;

        public override CompoundSemanticException WildExceptions()
        {
            CompoundSemanticException cse = base.WildExceptions();

            if ((End != null) && (End.Value <= Start))
            {
                cse.AddElement(new SemanticException("End should be less or equal then Start => [start, end["));
            }

            return cse;
        }

        /// <inheritdoc />
        public bool Contains(DateTime dt)
        {
            if (!IsCivilized)
            {
                throw new InternalProgrammingError("Validation of interval contains can only be done on Civilized objects");
            }

            return (Start.CoalesceStartValue() <= dt) && (dt < End.CoalesceEndValue());
        }

        public bool Contains(IInterval other)
        {
            if (other == null)
            {
                return false;
            }

            if (!IsCivilized || !other.IsCivilized)
            {
                throw new InternalProgrammingError("Validation of interval contains can only be done on Civilized objects");
            }

            return (Start.CoalesceStartValue() <= other.Start.CoalesceStartValue())
                   && (other.End.CoalesceEndValue() <= End.CoalesceEndValue());
        }

        /// <inheritdoc cref="IInterval" />
        public bool Overlaps(IInterval other)
        {
            if (other == null)
            {
                return false;
            }

            if (!IsCivilized || !other.IsCivilized)
            {
                throw new InternalProgrammingError("Validation of interval overlap can only be done on Civilized objects");
            }

            return (Start.CoalesceStartValue() < other.End.CoalesceEndValue())
                   && (other.Start.CoalesceStartValue() < End.CoalesceEndValue());
        }

        /// <inheritdoc cref="IInterval" />
        public bool IsClosed()
            => End != null;

        /// <inheritdoc />
        [ExcludeFromCodeCoverage]
        public IEnumerable<PpwAuditLog> GetMultiLogs(string propertyName)
        {
            yield return new PpwAuditLog($"{propertyName}.{nameof(Start)}", Start?.ToString(TimeFormatMask));
            yield return new PpwAuditLog($"{propertyName}.{nameof(End)}", End?.ToString(TimeFormatMask));
        }

        /// <inheritdoc />
        [ExcludeFromCodeCoverage]
        public PpwAuditLog GetSingleLog(string propertyName)
            => null;

        /// <inheritdoc />
        [ExcludeFromCodeCoverage]
        public bool IsMultiLog
            => true;

        public override bool Equals(object obj)
            => Equals(obj as Interval);

        [SuppressMessage(
            "ReSharper",
            "NonReadonlyMemberInGetHashCode",
            Justification = "Be care full when using in dictionaries / sets, etc...")]
        public override int GetHashCode()
        {
            unchecked
            {
                return (Start.GetHashCode() * 397) ^ End.GetHashCode();
            }
        }

        public static bool operator ==(Interval left, Interval right)
            => Equals(left, right);

        public static bool operator !=(Interval left, Interval right)
            => !Equals(left, right);

        [ExcludeFromCodeCoverage]
        public override string ToString()
        {
            string start = Start?.ToString(TimeFormatMask) ?? "-∞";
            string end = End?.ToString(TimeFormatMask) ?? "∞";
            return $"[{start}, {end}[";
        }

        [ExcludeFromCodeCoverage]
        [UsedImplicitly]
        public class IntervalComponentMapper : ComponentMapping<Interval>
        {
            public IntervalComponentMapper()
            {
                Property(p => p.Start);
                Property(p => p.End);
            }
        }
    }
}
