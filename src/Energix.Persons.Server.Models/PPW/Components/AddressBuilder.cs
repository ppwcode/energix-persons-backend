using System.Diagnostics;

using JetBrains.Annotations;

namespace Energix.Persons.Server.Models.PPW.Components
{
    public class AddressBuilder
    {
        private string _box;
        private string _city;
        private string _country;
        private string _houseNumber;
        private string _street;
        private string _zipCode;

        [NotNull]
        public AddressBuilder Merge([CanBeNull] Address address)
        {
            if (address != null)
            {
                _street = address.Street;
                _houseNumber = address.HouseNumber;
                _box = address.Box;
                _zipCode = address.ZipCode;
                _city = address.City;
                _country = address.Country;
            }

            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public AddressBuilder Street([CanBeNull] string street)
        {
            _street = street;
            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public AddressBuilder HouseNumber([CanBeNull] string number)
        {
            _houseNumber = number;
            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public AddressBuilder Box([CanBeNull] string box)
        {
            _box = box;
            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public AddressBuilder ZipCode([CanBeNull] string zipCode)
        {
            _zipCode = zipCode;
            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public AddressBuilder City([CanBeNull] string city)
        {
            _city = city;
            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public AddressBuilder Country([CanBeNull] string country)
        {
            _country = country;
            return this;
        }

        [DebuggerStepThrough]
        [NotNull]
        public Address Build()
            => this;

        [DebuggerStepThrough]
        [NotNull]
        public static implicit operator Address([NotNull] AddressBuilder builder)
            => new Address(
                builder._street,
                builder._houseNumber,
                builder._box,
                builder._zipCode,
                builder._city,
                builder._country);
    }
}
