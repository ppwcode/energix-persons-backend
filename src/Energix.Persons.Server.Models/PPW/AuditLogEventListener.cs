using System.Diagnostics.CodeAnalysis;

using JetBrains.Annotations;

using NHibernate.Event;

using PPWCode.Vernacular.NHibernate.III;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Models.PPW
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class AuditLogEventListener
        : AuditLogEventListener<long, AuditLog, AuditLogEventContext>
    {
        public AuditLogEventListener(
            [JetBrains.Annotations.NotNull] IIdentityProvider identityProvider,
            [JetBrains.Annotations.NotNull] ITimeProvider timeProvider)
            : base(identityProvider, timeProvider, true)
        {
        }

        /// <inheritdoc />
        protected override bool CanAuditLogFor(AbstractEvent @event, AuditLogItem auditLogItem, AuditLogActionEnum requestedLogAction)
            => true;

        /// <inheritdoc />
        protected override AuditLogEventContext CreateContext(IPostDatabaseOperationEventArgs postDatabaseOperationEventArgs)
            => new AuditLogEventContext(postDatabaseOperationEventArgs);

        /// <inheritdoc />
        protected override void OnAddAuditEntities(AuditLogEventContext context)
        {
            // NOP, there is no enrichment here for our standard context
        }
    }
}
