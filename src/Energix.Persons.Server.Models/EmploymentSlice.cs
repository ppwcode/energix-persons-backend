using System.Collections.Generic;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class EmploymentSlice : ModeDependantAuditablePersistentObject
{
    public virtual Affiliate Affiliate { get; set; }
    public virtual string EmployerUri { get; set; }

    public virtual ISet<EmploymentSliceProperties> Properties { get; set; }

    [UsedImplicitly]
    public class EmploymentSliceMapper : ModeDependantAuditablePersistentObjectMapper<EmploymentSlice>
    {
        public EmploymentSliceMapper()
        {
            Property(x => x.EmployerUri);

            ManyToOne(x => x.Affiliate);

            Set(
                e => e.Properties,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());
        }
    }
}
