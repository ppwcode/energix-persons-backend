﻿using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class Affiliate
    : ModeDependantAuditablePersistentObject
{
    public virtual ISet<AffiliateProperties> AffiliateProperties { get; set; }

    public virtual ISet<Partnerships> Partnerships { get; set; }

    public virtual ISet<Children> Children { get; set; }

    public virtual ISet<EmployerInvalidityInterval> EmployerInvalidityIntervals { get; set; }

    public virtual ISet<RecognisedInvalidityInterval> RecognisedInvalidityIntervals { get; set; }

    public virtual ISet<EmploymentSlice> EmploymentSlices { get; set; }

    public Affiliate()
    {
        AffiliateProperties = new HashSet<AffiliateProperties>();
        Partnerships = new HashSet<Partnerships>();
        Children = new HashSet<Children>();
        EmploymentSlices = new HashSet<EmploymentSlice>();
        EmployerInvalidityIntervals = new HashSet<EmployerInvalidityInterval>();
        RecognisedInvalidityIntervals = new HashSet<RecognisedInvalidityInterval>();
    }

    public virtual void AddProperties([CanBeNull] AffiliateProperties affiliateProperties)
    {
        if ((affiliateProperties != null) && AffiliateProperties.Add(affiliateProperties))
        {
            affiliateProperties.Affiliate = this;
        }
    }

    public virtual void AddPartnership([CanBeNull] Partnerships partnerships)
    {
        if ((partnerships != null) && Partnerships.Add(partnerships))
        {
            partnerships.Affiliate = this;
        }
    }

    public virtual void AddChildren([CanBeNull] Children children)
    {
        if ((children != null) && Children.Add(children))
        {
            children.Affiliate = this;
        }
    }

    public virtual void AddEmploymentInvalidityInterval([CanBeNull] EmployerInvalidityInterval employerInvalidityInterval)
    {
        if ((employerInvalidityInterval != null) && EmployerInvalidityIntervals.Add(employerInvalidityInterval))
        {
            employerInvalidityInterval.Affiliate = this;
        }
    }

    public virtual void AddRecognisedInvalidityInterval([CanBeNull] RecognisedInvalidityInterval recognisedInvalidityInterval)
    {
        if ((recognisedInvalidityInterval != null) && RecognisedInvalidityIntervals.Add(recognisedInvalidityInterval))
        {
            recognisedInvalidityInterval.Affiliate = this;
        }
    }

    public virtual void AddEmploymentSlice([CanBeNull] string employerUri)
    {
        var slice = new EmploymentSlice()
                    {
                        EmployerUri = employerUri
                    };
        if ((EmploymentSlices != null) && EmploymentSlices.Add(slice))
        {
            slice.Affiliate = this;
        }
    }

    public virtual T AddHistoricallyWhenChanged<T>(
        ISet<T> historicPropertySet,
        Action<T> addToList,
        T model,
        Sourced sourced)
        where T : IHistoricPersistentObject, IFunctionalEqualityComparer<T>, ISource
    {
        var newValidTo = DateTime.UtcNow;
        var previousHistoricValue = historicPropertySet.OrderByDescending(x => x.ValidFrom).FirstOrDefault();

        if (previousHistoricValue != null)
        {
            if (previousHistoricValue.IsFunctionallyTheSame(model))
            {
                // do not update if not needed, only link source
                previousHistoricValue.AddSourced(sourced);
                return previousHistoricValue;
            }

            previousHistoricValue.ValidTo = newValidTo;
        }

        model.ValidFrom = newValidTo;
        model.ValidTo = null;
        model.AddSourced(sourced);

        addToList(model);
        return model;
    }

    [UsedImplicitly]
    public class AffiliateMapper : ModeDependantAuditablePersistentObjectMapper<Affiliate>
    {
        public AffiliateMapper()
        {
            Set(
                e => e.AffiliateProperties,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());

            Set(
                e => e.Partnerships,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());

            Set(
                e => e.Children,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());

            Set(
                e => e.EmployerInvalidityIntervals,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());

            Set(
                e => e.EmploymentSlices,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());
        }
    }
}
