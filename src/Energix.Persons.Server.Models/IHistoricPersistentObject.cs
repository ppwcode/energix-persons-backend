using System;
using System.Linq;

namespace Energix.Persons.Server.Models;

public interface IHistoricPersistentObject
{
    DateTime ValidFrom { get; set; }
    DateTime? ValidTo { get; set; }
}

public static class IHistoricPersistentObjectExtensions
{
    public static IQueryable<T> WhereValid<T>(this IQueryable<T> query, DateTime at)
        where T : IHistoricPersistentObject
    {
        return query.Where(x => (x.ValidFrom <= at) && (!x.ValidTo.HasValue || (x.ValidTo > at)));
    }
}
