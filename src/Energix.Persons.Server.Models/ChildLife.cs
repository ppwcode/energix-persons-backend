using Energix.Persons.Server.Models.PPW.Components;

using JetBrains.Annotations;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class ChildLife
    : ModeDependantAuditablePersistentObject,
      IFunctionalEqualityComparer<ChildLife>
{
    public virtual Children Children { get; set; }

    public virtual Interval Interval { get; set; }

    [UsedImplicitly]
    public class ChildLifeMapper : ModeDependantAuditablePersistentObjectMapper<ChildLife>
    {
        public ChildLifeMapper()
        {
            ManyToOne(x => x.Children);
            Component(x => x.Interval);
        }
    }

    /// <inheritdoc />
    public virtual bool IsFunctionallyTheSame(ChildLife other)
    {
        var res = (Interval.Start == other.Interval.Start)
               && (Interval.End == other.Interval.End);
        return res;
    }
}
