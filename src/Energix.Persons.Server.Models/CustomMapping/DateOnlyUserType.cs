﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;

using NHibernate;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace Energix.Persons.Server.Models.CustomMapping;

public class DateOnlyUserType : IUserType
{
    public SqlType[] SqlTypes
    {
        get { return new[] { SqlTypeFactory.DateTime }; }
    }

    public Type ReturnedType
    {
        get { return typeof(uint); }
    }

    public bool IsMutable
    {
        get { return false; }
    }

    public int GetHashCode(object x)
    {
        if (x == null)
        {
            return 0;
        }

        return x.GetHashCode();
    }

    /// <inheritdoc />
    public object NullSafeGet(
        DbDataReader rs,
        string[] names,
        ISessionImplementor session,
        object owner)
    {
        var o = NHibernateUtil.String.NullSafeGet(
                    rs,
                    names,
                    session,
                    owner) as string;
        if (o == null)
        {
            return null;
        }

        var dateValue = DateTime.Parse(o, CultureInfo.InvariantCulture);
        return DateOnly.FromDateTime(dateValue);
    }

    /// <inheritdoc />
    public void NullSafeSet(
        DbCommand cmd,
        object value,
        int index,
        ISessionImplementor session)
    {
        if (value == null)
        {
            ((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
        }
        else
        {
            var dateOnlyValue = (DateOnly)value;
            var dateTimeValue = dateOnlyValue.ToDateTime(TimeOnly.MinValue, DateTimeKind.Utc);
            ((IDataParameter)cmd.Parameters[index]).Value = dateTimeValue;
        }
    }

    public object DeepCopy(object value)
    {
        return value;
    }

    public object Replace(object original, object target, object owner)
    {
        return original;
    }

    public object Assemble(object cached, object owner)
    {
        return cached;
    }

    public object Disassemble(object value)
    {
        return value;
    }

    bool IUserType.Equals(object x, object y)
    {
        return object.Equals(x, y);
    }
}
