using System;
using System.Diagnostics.CodeAnalysis;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode.Conformist;

using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Models;

public class Fraction
    : CivilizedObject,
      IEquatable<Fraction>
{
    public virtual int Numerator { get; set; }
    public virtual int Denominator { get; set; }

    /// <inheritdoc />
    public bool Equals(Fraction other)
    {
        if (ReferenceEquals(null, other))
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return Numerator == other.Numerator && Denominator == other.Denominator;
    }

    /// <inheritdoc />
    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj.GetType() != GetType())
        {
            return false;
        }

        return Equals((Fraction)obj);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        unchecked
        {
            return (Numerator.GetHashCode() * 397) ^ Denominator.GetHashCode();
        }
    }

    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class FractionComponentMapper : ComponentMapping<Fraction>
    {
        public FractionComponentMapper()
        {
            Property(p => p.Denominator);
            Property(p => p.Numerator);
        }
    }
}
