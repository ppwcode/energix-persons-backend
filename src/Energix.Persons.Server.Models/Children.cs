﻿using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class Children
    : ModeDependantAuditablePersistentObject,
      IHistoricPersistentObject,
      IFunctionalEqualityComparer<Children>,
      IFunctionalEqualityItemsContainer<ChildLife>,
      ISource
{
    public virtual DateTime ValidFrom { get; set; }
    public virtual DateTime? ValidTo { get; set; }
    public virtual Affiliate Affiliate { get; set; }

    public virtual ISet<ChildLife> Items { get; set; }

    public Children()
    {
        Items = new HashSet<ChildLife>();
    }

    public virtual ISet<Sourced> Sources { get; set; }

    public virtual void AddSourced(Sourced sourced)
    {
        if ((Sources != null) && Sources.Add(sourced))
        {
            sourced.Children = this;
        }
    }

    /// <inheritdoc />
    public virtual bool IsFunctionallyTheSame(Children other)
    {
        var res = this.IsItemsFunctionallyTheSame(other);
        return res;
    }

    public virtual void AddChildLives([CanBeNull] List<ChildLife> childLives)
    {
        foreach (var childLife in childLives)
        {
            if ((Items != null) && Items.Add(childLife))
            {
                childLife.Children = this;
            }
        }
    }

    [UsedImplicitly]
    public class ChildrenMapper : ModeDependantAuditablePersistentObjectMapper<Children>
    {
        public ChildrenMapper()
        {
            Property(x => x.ValidFrom);
            Property(x => x.ValidTo);
            ManyToOne(x => x.Affiliate);

            Set(
                e => e.Items,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());
        }
    }
}
