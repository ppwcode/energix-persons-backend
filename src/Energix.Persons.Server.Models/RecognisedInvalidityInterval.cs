using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class RecognisedInvalidityInterval
    : ModeDependantAuditablePersistentObject,
      IHistoricPersistentObject,
      IFunctionalEqualityComparer<RecognisedInvalidityInterval>,
      IFunctionalEqualityItemsContainer<RecognisedInvalidity>,
      ISource
{
    /// <inheritdoc />
    public virtual DateTime ValidFrom { get; set; }

    /// <inheritdoc />
    public virtual DateTime? ValidTo { get; set; }

    public virtual Affiliate Affiliate { get; set; }

    public virtual ISet<RecognisedInvalidity> Items { get; set; }

    public virtual ISet<Sourced> Sources { get; set; }

    public virtual void AddSourced(Sourced sourced)
    {
        if ((Sources != null) && Sources.Add(sourced))
        {
            sourced.RecognisedInvalidityInterval = this;
        }
    }

    public RecognisedInvalidityInterval()
    {
        Items = new HashSet<RecognisedInvalidity>();
    }

    /// <inheritdoc />
    public virtual bool IsFunctionallyTheSame(RecognisedInvalidityInterval other)
    {
        var res = this.IsItemsFunctionallyTheSame(other);
        return res;
    }

    public virtual void AddItem([CanBeNull] List<RecognisedInvalidity> recognisedInvalidities)
    {
        foreach (var employerInvalidity in recognisedInvalidities)
        {
            if ((Items != null) && Items.Add(employerInvalidity))
            {
                employerInvalidity.RecognisedInvalidityInterval = this;
            }
        }
    }

    [UsedImplicitly]
    public class RecognisedInvalidityIntervalMapper : ModeDependantAuditablePersistentObjectMapper<RecognisedInvalidityInterval>
    {
        public RecognisedInvalidityIntervalMapper()
        {
            Property(x => x.ValidFrom);
            Property(x => x.ValidTo);
            ManyToOne(x => x.Affiliate);

            Set(
                e => e.Items,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());
        }
    }
}
