using System.Collections.Generic;

namespace Energix.Persons.Server.Models;

public interface ISource
{
    ISet<Sourced> Sources { get; set; }
    void AddSourced(Sourced sourced);
}
