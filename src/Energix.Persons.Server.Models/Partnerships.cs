using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class Partnerships
    : ModeDependantAuditablePersistentObject,
      IHistoricPersistentObject,
      IFunctionalEqualityComparer<Partnerships>,
      IFunctionalEqualityItemsContainer<Partnership>,
      ISource
{
    public virtual DateTime ValidFrom { get; set; }
    public virtual DateTime? ValidTo { get; set; }
    public virtual Affiliate Affiliate { get; set; }

    public virtual ISet<Partnership> Items { get; set; }

    public virtual ISet<Sourced> Sources { get; set; }

    public virtual void AddSourced(Sourced sourced)
    {
        if ((Sources != null) && Sources.Add(sourced))
        {
            sourced.Partnerships = this;
        }
    }

    public Partnerships()
    {
        Items = new HashSet<Partnership>();
    }

    /// <inheritdoc />
    public virtual bool IsFunctionallyTheSame(Partnerships other)
    {
        var res = this.IsItemsFunctionallyTheSame(other);
        return res;
    }

    public virtual void AddPartners([CanBeNull] List<Partnership> partners)
    {
        foreach (var partner in partners)
        {
            if ((Items != null) && Items.Add(partner))
            {
                partner.Partnerships = this;
            }
        }
    }

    [UsedImplicitly]
    public class PartnershipsMapper : ModeDependantAuditablePersistentObjectMapper<Partnerships>
    {
        public PartnershipsMapper()
        {
            Property(x => x.ValidFrom);
            Property(x => x.ValidTo);
            ManyToOne(x => x.Affiliate);

            Set(
                e => e.Items,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());
        }
    }
}
