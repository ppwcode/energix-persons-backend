﻿using System.ComponentModel.DataAnnotations;

using JetBrains.Annotations;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class Absorption : ModeDependantAuditablePersistentObject
{
    [Required]
    public virtual Affiliate Absorbed { get; set; }

    [Required]
    public virtual Affiliate AbsorbedBy { get; set; }

    [UsedImplicitly]
    public class AbsorptionMapper : ModeDependantAuditablePersistentObjectMapper<Absorption>
    {
        public AbsorptionMapper()
        {
            ManyToOne(x => x.Absorbed);
            ManyToOne(x => x.AbsorbedBy);
        }
    }
}
