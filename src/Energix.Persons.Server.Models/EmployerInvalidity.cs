﻿using Energix.Persons.Server.Models.PPW.Components;

using JetBrains.Annotations;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class EmployerInvalidity
    : ModeDependantAuditablePersistentObject,
      IFunctionalEqualityComparer<EmployerInvalidity>
{
    public virtual EmployerInvalidityInterval EmployerInvalidityInterval { get; set; }
    public virtual decimal Degree { get; set; }
    public virtual Interval Interval { get; set; }

    /// <inheritdoc />
    public virtual bool IsFunctionallyTheSame(EmployerInvalidity other)
    {
        var isIntervalSame = (Interval.Start == other.Interval.Start)
                             && (Interval.End == other.Interval.End);
        var isDegreeSame = Degree == other.Degree;
        var res = isIntervalSame && isDegreeSame;
        return res;
    }

    [UsedImplicitly]
    public class EmployerInvalidityMapper : ModeDependantAuditablePersistentObjectMapper<EmployerInvalidity>
    {
        public EmployerInvalidityMapper()
        {
            Property(x => x.Degree);
            Component(x => x.Interval);
            ManyToOne(x => x.EmployerInvalidityInterval);
        }
    }
}
