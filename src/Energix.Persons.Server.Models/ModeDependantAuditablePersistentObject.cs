﻿using Energix.Persons.PPW.Extensions;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class ModeDependantAuditablePersistentObject
    : AuditablePersistentObject,
      IMode
{
    public virtual string Mode { get; set; }
}

public abstract class ModeDependantAuditablePersistentObjectMapper<T> : AuditablePersistentObjectMapper<T>
    where T : ModeDependantAuditablePersistentObject
{
    protected ModeDependantAuditablePersistentObjectMapper()
    {
        Property(x => x.Mode);
    }
}
