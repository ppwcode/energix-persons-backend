using System;

using Energix.Persons.API;

using JetBrains.Annotations;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class EmploymentSliceProperties
    : ModeDependantAuditablePersistentObject,
      IHistoricPersistentObject
{
    /// <inheritdoc />
    public virtual DateTime ValidFrom { get; set; }

    /// <inheritdoc />
    public virtual DateTime? ValidTo { get; set; }

    public virtual EmploymentCategory EmploymentCategory { get; set; }
    public virtual Fraction WorkFraction { get; set; }
    public virtual decimal ProjectedWorkFraction { get; set; }

    public virtual EmploymentSlice EmploymentSlice { get; set; }

    [UsedImplicitly]
    public class EmploymentSlicePropertiesMapper : ModeDependantAuditablePersistentObjectMapper<EmploymentSliceProperties>
    {
        public EmploymentSlicePropertiesMapper()
        {
            Property(x => x.ValidTo);
            Property(x => x.ValidFrom);
            Property(x => x.EmploymentCategory);
            Property(x => x.ProjectedWorkFraction);

            Component(x => x.WorkFraction);

            ManyToOne(x => x.EmploymentSlice);
        }
    }
}
