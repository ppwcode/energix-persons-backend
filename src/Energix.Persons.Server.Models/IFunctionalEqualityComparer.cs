using System.Collections.Generic;
using System.Linq;

namespace Energix.Persons.Server.Models;

/// <summary>
///  like IEqualityComparer but properties like validFrom and validTo can be ignored
/// handy when checking if you need historical record
/// </summary>
/// <typeparam name="T">model</typeparam>
public interface IFunctionalEqualityComparer<T>
{
    bool IsFunctionallyTheSame(T other);
}

public interface IFunctionalEqualityItemsContainer<T>
    where T : IFunctionalEqualityComparer<T>
{
    ISet<T> Items { get; set; }
}

public static class FunctionalEqualityItemsContainerExtensions
{
    public static bool IsItemsFunctionallyTheSame<T>(this IFunctionalEqualityItemsContainer<T> itemsContainer, IFunctionalEqualityItemsContainer<T> other)
        where T : IFunctionalEqualityComparer<T>
    {
        if (itemsContainer.Items.Count != other.Items.Count)
        {
            return false;
        }

        var items = itemsContainer.Items.ToList();
        var i = 0;
        foreach (var otherChildLife in other.Items)
        {
            if (!otherChildLife.IsFunctionallyTheSame(items[i]))
            {
                return false;
            }

            i++;
        }

        return true;
    }
}
