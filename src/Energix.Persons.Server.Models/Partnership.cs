﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Energix.Persons.API.Enums;
using Energix.Persons.Server.Models.PPW.Components;

using JetBrains.Annotations;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class Partnership
    : ModeDependantAuditablePersistentObject,
      IFunctionalEqualityComparer<Partnership>
{
    // van wanneer tot wanneer loop het partnership (daydates, geen uren)
    public virtual Interval Interval { get; set; }

    [Required]
    public virtual DateTime DateOfBirth { get; set; }

    [Required]
    public virtual Gender MortalityTableGender { get; set; }

    [Required]
    public virtual CivilState CivilState { get; set; }

    [Required]
    public virtual Partnerships Partnerships { get; set; }

    [UsedImplicitly]
    public class PartnershipMapper : ModeDependantAuditablePersistentObjectMapper<Partnership>
    {
        public PartnershipMapper()
        {
            Property(x => x.DateOfBirth);
            Property(x => x.MortalityTableGender);
            Property(x => x.CivilState);

            ManyToOne(x => x.Partnerships);
            Component(x => x.Interval);
        }
    }

    /// <inheritdoc />
    public virtual bool IsFunctionallyTheSame(Partnership other)
    {
        var res = Interval.Start == other.Interval.Start
                  && Interval.End == other.Interval.End
                  && DateOfBirth == other.DateOfBirth
                  && MortalityTableGender == other.MortalityTableGender
                  && CivilState == other.CivilState;
        return res;
    }
}
