using System.ComponentModel.DataAnnotations;

using Energix.Persons.API;

using JetBrains.Annotations;

namespace Energix.Persons.Server.Models;

public class Sourced : ModeDependantAuditablePersistentObject
{
    [Required]
    [StringLength(FieldLengths.SourceLength)]
    public virtual string Source { get; set; }

    public virtual AffiliateProperties AffiliateProperties { get; set; }

    public virtual Children Children { get; set; }

    public virtual Partnerships Partnerships { get; set; }

    public virtual EmployerInvalidityInterval EmployerInvalidityInterval { get; set; }

    public virtual RecognisedInvalidityInterval RecognisedInvalidityInterval { get; set; }

    public Sourced()
    {
    }

    [UsedImplicitly]
    public class SourcedMapper : ModeDependantAuditablePersistentObjectMapper<Sourced>
    {
        public SourcedMapper()
        {
            Property(x => x.Source);
            ManyToOne(x => x.AffiliateProperties);
            ManyToOne(x => x.Children);
            ManyToOne(x => x.Partnerships);
            ManyToOne(x => x.EmployerInvalidityInterval);
            ManyToOne(x => x.RecognisedInvalidityInterval);
        }
    }
}
