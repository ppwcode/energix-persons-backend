using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class EmployerInvalidityInterval
    : ModeDependantAuditablePersistentObject,
      IHistoricPersistentObject,
      IFunctionalEqualityComparer<EmployerInvalidityInterval>,
      IFunctionalEqualityItemsContainer<EmployerInvalidity>,
      ISource
{
    /// <inheritdoc />
    public virtual DateTime ValidFrom { get; set; }

    /// <inheritdoc />
    public virtual DateTime? ValidTo { get; set; }

    public virtual Affiliate Affiliate { get; set; }

    public virtual ISet<EmployerInvalidity> Items { get; set; }

    public virtual ISet<Sourced> Sources { get; set; }

    public virtual void AddSourced(Sourced sourced)
    {
        if ((Sources != null) && Sources.Add(sourced))
        {
            sourced.EmployerInvalidityInterval = this;
        }
    }

    public EmployerInvalidityInterval()
    {
        Items = new HashSet<EmployerInvalidity>();
    }

    /// <inheritdoc />
    public virtual bool IsFunctionallyTheSame(EmployerInvalidityInterval other)
    {
        var res = this.IsItemsFunctionallyTheSame(other);
        return res;
    }

    public virtual void AddItem([CanBeNull] List<EmployerInvalidity> employerInvalidities)
    {
        foreach (var employerInvalidity in employerInvalidities)
        {
            if ((Items != null) && Items.Add(employerInvalidity))
            {
                employerInvalidity.EmployerInvalidityInterval = this;
            }
        }
    }

    [UsedImplicitly]
    public class EmployerInvalidityIntervalMapper : ModeDependantAuditablePersistentObjectMapper<EmployerInvalidityInterval>
    {
        public EmployerInvalidityIntervalMapper()
        {
            Property(x => x.ValidFrom);
            Property(x => x.ValidTo);
            ManyToOne(x => x.Affiliate);

            Set(
                e => e.Items,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());
        }
    }
}
