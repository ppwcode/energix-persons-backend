using Energix.Persons.API.Enums;
using Energix.Persons.Server.Models.PPW.Components;

using JetBrains.Annotations;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class RecognisedInvalidity
    : ModeDependantAuditablePersistentObject,
      IFunctionalEqualityComparer<RecognisedInvalidity>
{
    public virtual RecognisedInvalidityInterval RecognisedInvalidityInterval { get; set; }
    public virtual decimal Degree { get; set; }
    public virtual Interval Interval { get; set; }

    public virtual InsuranceBenefitTypeGroup Type { get; set; }

    /// <inheritdoc />
    public virtual bool IsFunctionallyTheSame(RecognisedInvalidity other)
    {
        var isIntervalSame = (Interval.Start == other.Interval.Start)
                             && (Interval.End == other.Interval.End);
        var isDegreeSame = Degree == other.Degree;
        var isSameType = Type == other.Type;
        var res = isIntervalSame && isDegreeSame && isSameType;
        return res;
    }

    [UsedImplicitly]
    public class RecognisedInvalidityMapper : ModeDependantAuditablePersistentObjectMapper<RecognisedInvalidity>
    {
        public RecognisedInvalidityMapper()
        {
            Property(x => x.Degree);
            Property(x => x.Type);
            Component(x => x.Interval);
            ManyToOne(x => x.RecognisedInvalidityInterval);
        }
    }
}
