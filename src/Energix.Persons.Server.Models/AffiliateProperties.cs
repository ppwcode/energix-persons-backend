using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Energix.Persons.API;
using Energix.Persons.API.Enums;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models.PPW.Components;

using JetBrains.Annotations;

using NHibernate.Mapping.ByCode;

using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.Models;

public class AffiliateProperties
    : ModeDependantAuditablePersistentObject,
      IHistoricPersistentObject,
      IFunctionalEqualityComparer<AffiliateProperties>,
      ISource
{
    public AffiliateProperties()
    {
        Sources = new HashSet<Sourced>();
    }

    [Required]
    [StringLength(FieldLengths.NameLength)]
    public virtual string FirstName { get; set; }

    [Required]
    [StringLength(FieldLengths.NameLength)]
    public virtual string LastName { get; set; }

    [Required]
    public virtual string Inss { get; set; }

    /// <summary>
    /// Native support for TimeOnly and DateOnly .NET 6 types
    /// https://github.com/nhibernate/nhibernate-core/issues/2912
    /// </summary>
    [Required]
    public virtual DateOnly DateOfBirth { get; set; }

    [Required]
    public virtual Gender MortalityTableGender { get; set; }

    [Required]
    public virtual Language Language { get; set; }

    [StringLength(FieldLengths.EmailLength)]
    public virtual string EmailsPrivate { get; set; }

    [StringLength(FieldLengths.EmailLength)]
    public virtual string EmailsProfessional { get; set; }

    [StringLength(FieldLengths.EmailLength)]
    public virtual string Tel { get; set; }

    public virtual DateTime? DateOfDeath { get; set; }
    public virtual DateTime? RetirementDate { get; set; }

    public virtual DateTime ValidFrom { get; set; }
    public virtual DateTime? ValidTo { get; set; }

    [CanBeNull]
    public virtual Address Address { get; set; }

    [Required]
    public virtual Affiliate Affiliate { get; set; }

    public virtual ISet<Sourced> Sources { get; set; }

    public virtual void AddSourced(Sourced sourced)
    {
        if ((Sources != null) && Sources.Add(sourced))
        {
            sourced.AffiliateProperties = this;
        }
    }

    [UsedImplicitly]
    public class AffiliatePropertiesMapper : ModeDependantAuditablePersistentObjectMapper<AffiliateProperties>
    {
        public AffiliatePropertiesMapper()
        {
            Property(x => x.FirstName);
            Property(x => x.LastName);
            Property(x => x.Inss);
            Property(x => x.DateOfBirth);
            Property(x => x.EmailsPrivate);
            Property(x => x.EmailsProfessional);
            Property(x => x.Tel);
            Property(x => x.DateOfDeath);
            Property(x => x.RetirementDate);
            Property(x => x.ValidFrom);
            Property(x => x.ValidTo);
            Property(x => x.MortalityTableGender);
            Property(x => x.Language);

            ManyToOne(x => x.Affiliate);
            Component(sev => sev.Address);

            Set(
                e => e.Sources,
                m => m.Cascade(Cascade.All.Include(Cascade.DeleteOrphans)),
                r => r.OneToMany());
        }
    }

    public virtual bool IsFunctionallyTheSame(AffiliateProperties other)
    {
        // all props, except validFrom & validTo
        var equal = (FirstName == other.FirstName) &&
                    (LastName == other.LastName) &&
                    (Inss == other.Inss) &&
                    (DateOfBirth == other.DateOfBirth) &&
                    (EmailsPrivate == other.EmailsPrivate) &&
                    (EmailsProfessional == other.EmailsProfessional) &&
                    (Tel == other.Tel) &&
                    (DateOfDeath == other.DateOfDeath) &&
                    (RetirementDate == other.RetirementDate) &&
                    (MortalityTableGender == other.MortalityTableGender) &&
                    (Language == other.Language) &&
                    ((other.Address == null && Address == null) || (Address?.Equals(other.Address) ?? false));
        return equal;
    }
}
