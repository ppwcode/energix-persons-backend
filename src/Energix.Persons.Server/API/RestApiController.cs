using System.IO;
using System.Threading.Tasks;

using JetBrains.Annotations;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

using MimeMapping;

namespace Energix.Persons.Server.API
{
    [Authorize]
    public abstract class RestApiController : PPWCode.Server.Core.API.RestApiController
    {
        [NotNull]
        protected Task<FileStreamResult> Ok([NotNull] Stream stream, [NotNull] string fileDownloadName)
            => Task.FromResult(
                new FileStreamResult(
                    stream,
                    new MediaTypeHeaderValue(MimeUtility.GetMimeMapping(fileDownloadName)))
                {
                    FileDownloadName = fileDownloadName
                });
    }
}
