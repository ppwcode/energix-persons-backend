﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.BusinessLogic;
using Energix.Persons.Server.Mappers;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using PPWCode.Server.Core.Transactional;

namespace Energix.Persons.Server.API.I;

[V1]
[Route(Energix.Persons.API.Routes.Affiliate)]
[AllowAnonymous]
[Transactional(true)]
public class AffiliateInvalidityController : RestApiController
{
    private readonly IAffiliateInvalidityManager _affiliateInvalidityManager;
    private readonly ISourceManager _sourceManager;
    private readonly IEmployerInvalidityIntervalMapper _employerInvalidityIntervalMapper;
    private readonly IHistoryMapper _historyMapper;

    public AffiliateInvalidityController(
        IAffiliateInvalidityManager affiliateInvalidityManager,
        ISourceManager sourceManager,
        IEmployerInvalidityIntervalMapper employerInvalidityIntervalMapper,
        IHistoryMapper historyMapper)
    {
        _affiliateInvalidityManager = affiliateInvalidityManager;
        _sourceManager = sourceManager;
        _employerInvalidityIntervalMapper = employerInvalidityIntervalMapper;
        _historyMapper = historyMapper;
    }

    [HttpGet("{id}/employerinvalidity", Name = Routes.Affiliate_Employer_Invalidity)]
    [ProducesResponseType(typeof(EmployerInvalidityIntervalDto), 200)]
    public async Task<IActionResult> EmployerInvalidity(long id, [FromQuery] DateTime? at, CancellationToken cancellationToken)
    {
        var model = await _affiliateInvalidityManager.GetEmployerInvalidity(id, at ?? DateTime.UtcNow);
        var dtos = await _employerInvalidityIntervalMapper.MapAsync(model, cancellationToken);
        return Ok(dtos);
    }

    [HttpPut("{id}/employerinvalidity", Name = Routes.Affiliate_Employer_Invalidity_Put)]
    public async Task<IActionResult> EmployerInvalidity(long id, [FromBody] EmployerInvalidityIntervalDto dto, CancellationToken cancellationToken)
    {
        var sourced = await _sourceManager.GetOrCreate(dto.Sourced);
        var children = await _employerInvalidityIntervalMapper.MapAsync(dto, cancellationToken);
        var r = await _affiliateInvalidityManager.UpdateEmployerInvalidity(id, children, sourced);
        var updatedChildren = await _employerInvalidityIntervalMapper.MapAsync(r, cancellationToken);
        return Ok(updatedChildren);
    }

    [HttpGet("{id}/employerinvalidity/history", Name = Routes.Affiliate_Employer_Invalidity_History)]
    [ProducesResponseType(typeof(HistoryDto), 200)]
    public async Task<IActionResult> EmployerInvalidityHistory(long id)
    {
        var dates = await _affiliateInvalidityManager.GetEmployerInvalidityHistory(id);
        var dtos = _historyMapper.Map(dates, Routes.Affiliate_Employer_Invalidity, id);
        return Ok(dtos);
    }
}
