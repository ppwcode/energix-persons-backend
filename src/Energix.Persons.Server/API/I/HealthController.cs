﻿using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.BusinessLogic;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using PPWCode.Server.Core.Transactional;

namespace Energix.Persons.Server.API.I;

[V1]
[Route(Energix.Persons.API.Routes.Health)]
[AllowAnonymous]
[Transactional(true)]
public class HealthController : RestApiController
{
    private readonly IHealthManager _healthManager;

    public HealthController(IHealthManager healthManager)
    {
        _healthManager = healthManager;
    }

    [HttpGet("", Name = Routes.Health_Index)]
    [ProducesResponseType(typeof(AffiliateDto), 200)]
    public async Task<IActionResult> Index()
    {
        var db = await _healthManager.GetDatabaseHealth();
        return Ok(new { db = db });
    }
}
