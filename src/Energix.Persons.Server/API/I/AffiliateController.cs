﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.BusinessLogic;
using Energix.Persons.Server.Mappers;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using PPWCode.Server.Core.Transactional;

namespace Energix.Persons.Server.API.I;

[V1]
[Route(Energix.Persons.API.Routes.Affiliate)]
[AllowAnonymous]
[Transactional(true)]
public class AffiliateController : RestApiController
{
    private readonly IAffiliateManager _affiliateManager;
    private readonly ISourceManager _sourceManager;
    private readonly IAffiliateMapper _affiliateMapper;
    private readonly IPartnershipsMapper _partnershipsMapper;
    private readonly IChildrenMapper _childrenMapper;
    private readonly IAbsorptionMapper _absorptionMapper;
    private readonly IHistoryMapper _historyMapper;

    public AffiliateController(
        IAffiliateManager affiliateManager,
        ISourceManager sourceManager,
        IAffiliateMapper affiliateMapper,
        IPartnershipsMapper partnershipsMapper,
        IChildrenMapper childrenMapper,
        IAbsorptionMapper absorptionMapper,
        IHistoryMapper historyMapper)
    {
        _affiliateManager = affiliateManager;
        _sourceManager = sourceManager;
        _affiliateMapper = affiliateMapper;
        _partnershipsMapper = partnershipsMapper;
        _childrenMapper = childrenMapper;
        _absorptionMapper = absorptionMapper;
        _historyMapper = historyMapper;
    }

    [HttpGet("{id}", Name = Routes.Affiliate_Get)]
    [ProducesResponseType(typeof(AffiliateDto), 200)]
    public async Task<IActionResult> Get(long id, [FromQuery] DateTime? at, CancellationToken cancellationToken)
    {
        var model = await _affiliateManager.GetById(id, at ?? DateTime.UtcNow);
        var dto = await _affiliateMapper.MapAsync(model, cancellationToken);
        return Ok(dto);
    }

    [HttpGet("{id}/history", Name = Routes.Affiliate_History)]
    [ProducesResponseType(typeof(AffiliateDto), 200)]
    public async Task<IActionResult> History(long id)
    {
        var dates = await _affiliateManager.GetHistoryById(id);
        var dto = _historyMapper.Map(dates, Routes.Affiliate_Get, id);
        return Ok(dto);
    }

    [HttpGet("{id}/sources", Name = Routes.Affiliate_Sources)]
    [ProducesResponseType(typeof(HistoryDto), 200)]
    public async Task<IActionResult> Sources(long id, [FromQuery] DateTime? at, CancellationToken cancellationToken)
    {
        var affiliate = await _affiliateManager.GetById(id, at ?? DateTime.UtcNow);
        var sources = affiliate.Sources.Select(x => x.Source).ToList();
        return Ok(sources);
    }

    [HttpPost("", Name = Routes.Affiliate_Post)]
    [ProducesResponseType(typeof(AffiliateDto), 200)]
    public async Task<IActionResult> Post([FromBody] AffiliateDto dto, CancellationToken cancellationToken)
    {
        var sourced = await _sourceManager.GetOrCreate(dto.Sourced);
        var model = await _affiliateMapper.MapAsync(dto, cancellationToken);
        var r = await _affiliateManager.Create(model, sourced);
        if (r.ExistingAffiliateId.HasValue)
        {
            return Conflict(
                new
                {
                    reason = "inssAlreadyUsed",
                    affiliate = Url.Link(Routes.Affiliate_Get, new { id = r.ExistingAffiliateId.Value })
                });
        }

        if (r.CreatedAffiliate != null)
        {
            var createdAffiliateDto = await _affiliateMapper.MapAsync(r.CreatedAffiliate, cancellationToken);
            return Ok(createdAffiliateDto);
        }

        return BadRequest();
    }

    [HttpPut("{id}", Name = Routes.Affiliate_Put)]
    [ProducesResponseType(typeof(AffiliateDto), 200)]
    public async Task<IActionResult> Put(long id, [FromBody] AffiliateDto dto, CancellationToken cancellationToken)
    {
        var sourced = await _sourceManager.GetOrCreate(dto.Sourced);
        var affiliateProperties = await _affiliateMapper.MapAsync(dto, cancellationToken);
        var r = await _affiliateManager.Update(id, affiliateProperties, sourced);
        var updatedAffiliate = await _affiliateMapper.MapAsync(r, cancellationToken);
        return Ok(updatedAffiliate);
    }

    [HttpGet("{id}/partnerships", Name = Routes.Affiliate_Partnerships)]
    [ProducesResponseType(typeof(PartnershipsDto), 200)]
    public async Task<IActionResult> Partnerships(long id, [FromQuery] DateTime? at, CancellationToken cancellationToken)
    {
        var partnerships = await _affiliateManager.GetAffiliatePartnerships(id, at ?? DateTime.UtcNow);
        var dtos = await _partnershipsMapper.MapAsync(partnerships, cancellationToken);
        return Ok(dtos);
    }

    [HttpPut("{id}/partnerships", Name = Routes.Affiliate_Partnerships_Put)]
    [ProducesResponseType(typeof(PartnershipsDto), 200)]
    public async Task<IActionResult> Partnerships(long id, [FromBody] PartnershipsDto dto, CancellationToken cancellationToken)
    {
        var partnerships = await _partnershipsMapper.MapAsync(dto, cancellationToken);
        var sourced = await _sourceManager.GetOrCreate(dto.Sourced);
        var r = await _affiliateManager.UpdatePartnerShips(id, partnerships, sourced);
        var updatedPartnerships = await _partnershipsMapper.MapAsync(r, cancellationToken);
        return Ok(updatedPartnerships);
    }

    [HttpGet("{id}/partnerships/history", Name = Routes.Affiliate_Partnerships_History)]
    [ProducesResponseType(typeof(HistoryDto), 200)]
    public async Task<IActionResult> Partnerships(long id, CancellationToken cancellationToken)
    {
        var dates = await _affiliateManager.GetAffiliatePartnershipsHistory(id);
        var dto = _historyMapper.Map(dates, Routes.Affiliate_Partnerships, id);
        return Ok(dto);
    }

    [HttpGet("{id}/children", Name = Routes.Affiliate_Children)]
    [ProducesResponseType(typeof(ChildrenDto), 200)]
    public async Task<IActionResult> Children(long id, [FromQuery] DateTime? at, CancellationToken cancellationToken)
    {
        var model = await _affiliateManager.GetAffiliateChildren(id, at ?? DateTime.UtcNow);
        var dtos = await _childrenMapper.MapAsync(model, cancellationToken);
        return Ok(dtos);
    }

    [HttpPut("{id}/children", Name = Routes.Affiliate_Children_Put)]
    public async Task<IActionResult> Children(long id, [FromBody] ChildrenDto dto, CancellationToken cancellationToken)
    {
        var sourced = await _sourceManager.GetOrCreate(dto.Sourced);
        var children = await _childrenMapper.MapAsync(dto, cancellationToken);
        var r = await _affiliateManager.UpdateChildren(id, children, sourced);
        var updatedChildren = await _childrenMapper.MapAsync(r, cancellationToken);
        return Ok(updatedChildren);
    }

    [HttpGet("{id}/children/history", Name = Routes.Affiliate_Children_History)]
    [ProducesResponseType(typeof(HistoryDto), 200)]
    public async Task<IActionResult> ChildrenHistory(long id)
    {
        var dates = await _affiliateManager.GetAffiliateChildrenHistory(id);
        var dtos = _historyMapper.Map(dates, Routes.Affiliate_Children, id);
        return Ok(dtos);
    }

    [HttpGet("{id}/absorbed", Name = Routes.Affiliate_Absorbed)]
    [ProducesResponseType(typeof(AbsorptionDto), 200)]
    public async Task<IActionResult> Absorbed(long id, CancellationToken cancellationToken)
    {
        var model = await _affiliateManager.GetAbsorptionForAbsorbedAffiliate(id);
        var dtos = await _absorptionMapper.MapAsync(model, cancellationToken);
        return Ok(dtos);
    }

    [HttpGet("{id}/absorptions", Name = Routes.Affiliate_Absorptions)]
    [ProducesResponseType(typeof(List<AbsorptionDto>), 200)]
    public async Task<IActionResult> Absorptions(long id, CancellationToken cancellationToken)
    {
        var model = await _affiliateManager.GetAbsorptionsByAffiliate(id);
        var dtos = await _absorptionMapper.MapAsync(model, cancellationToken);
        return Ok(dtos);
    }
}
