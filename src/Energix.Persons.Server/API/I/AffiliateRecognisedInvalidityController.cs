﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.BusinessLogic;
using Energix.Persons.Server.Mappers;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using PPWCode.Server.Core.Transactional;

namespace Energix.Persons.Server.API.I;

[V1]
[Route(Energix.Persons.API.Routes.Affiliate)]
[AllowAnonymous]
[Transactional(true)]
public class AffiliateRecognisedInvalidityController : RestApiController
{
    private readonly IAffiliateRecognisedInvalidityManager _affiliateRecognisedInvalidityManager;
    private readonly ISourceManager _sourceManager;
    private readonly IRecognisedInvalidityIntervalMapper _recognisedInvalidityIntervalMapper;
    private readonly IHistoryMapper _historyMapper;

    public AffiliateRecognisedInvalidityController(
        IAffiliateRecognisedInvalidityManager affiliateRecognisedInvalidityManager,
        ISourceManager sourceManager,
        IRecognisedInvalidityIntervalMapper recognisedInvalidityIntervalMapper,
        IHistoryMapper historyMapper)
    {
        _affiliateRecognisedInvalidityManager = affiliateRecognisedInvalidityManager;
        _sourceManager = sourceManager;
        _recognisedInvalidityIntervalMapper = recognisedInvalidityIntervalMapper;
        _historyMapper = historyMapper;
    }

    [HttpGet("{id}/recognisedinvalidity", Name = Routes.Affiliate_Recognised_Invalidity)]
    [ProducesResponseType(typeof(RecognisedInvalidityIntervalDto), 200)]
    public async Task<IActionResult> RecognisedInvalidity(long id, [FromQuery] DateTime? at, CancellationToken cancellationToken)
    {
        var model = await _affiliateRecognisedInvalidityManager.GetRecognisedInvalidity(id, at ?? DateTime.UtcNow);
        var dtos = await _recognisedInvalidityIntervalMapper.MapAsync(model, cancellationToken);
        return Ok(dtos);
    }

    [HttpPut("{id}/RecognisedInvalidity", Name = Routes.Affiliate_Recognised_Invalidity_Put)]
    public async Task<IActionResult> RecognisedInvalidity(long id, [FromBody] RecognisedInvalidityIntervalDto dto, CancellationToken cancellationToken)
    {
        var sourced = await _sourceManager.GetOrCreate(dto.Sourced);
        var children = await _recognisedInvalidityIntervalMapper.MapAsync(dto, cancellationToken);
        var r = await _affiliateRecognisedInvalidityManager.UpdateRecognisedInvalidity(id, children, sourced);
        var updatedChildren = await _recognisedInvalidityIntervalMapper.MapAsync(r, cancellationToken);
        return Ok(updatedChildren);
    }

    [HttpGet("{id}/RecognisedInvalidity/history", Name = Routes.Affiliate_Recognised_Invalidity_History)]
    [ProducesResponseType(typeof(HistoryDto), 200)]
    public async Task<IActionResult> RecognisedInvalidityHistory(long id)
    {
        var dates = await _affiliateRecognisedInvalidityManager.GetRecognisedInvalidityHistory(id);
        var dtos = _historyMapper.Map(dates, Routes.Affiliate_Recognised_Invalidity, id);
        return Ok(dtos);
    }
}
