﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.BusinessLogic;
using Energix.Persons.Server.Mappers;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using PPWCode.Server.Core.Transactional;

namespace Energix.Persons.Server.API.I;

[V1]
[Route(Energix.Persons.API.Routes.Affiliate)]
[AllowAnonymous]
[Transactional(true)]
public class AffiliateEmploymentController : RestApiController
{
    private readonly IAffiliateEmploymentManager _affiliateEmploymentManager;
    private readonly IEmployersMapper _employersMapper;
    private readonly IEmploymentSliceMapper _employmentSliceMapper;

    public AffiliateEmploymentController(
        IAffiliateEmploymentManager affiliateEmploymentManager,
        IEmployersMapper employersMapper,
        IEmploymentSliceMapper employmentSliceMapper)
    {
        _affiliateEmploymentManager = affiliateEmploymentManager;
        _employersMapper = employersMapper;
        _employmentSliceMapper = employmentSliceMapper;
    }

    [HttpGet("{id}/employer", Name = Routes.Affiliate_Employers_Get)]
    [ProducesResponseType(typeof(EmployersDto), 200)]
    public async Task<IActionResult> Employer(long id, [FromQuery] DateTime? at, CancellationToken cancellationToken)
    {
        var properties = await _affiliateEmploymentManager.GetEmploymentsForAffiliate(id, at ?? DateTime.UtcNow);
        var dtos = _employersMapper.Map(properties, id);
        return Ok(dtos);
    }

    [HttpGet("{id}/employer/EmploymentSlice/{employmentSliceId}", Name = Routes.Affiliate_EmploymentSlice_Get)]
    [ProducesResponseType(typeof(EmployersDto), 200)]
    public async Task<IActionResult> EmploymentSlice(
        long id,
        long employmentSliceId,
        [FromQuery] DateTime? at,
        CancellationToken cancellationToken)
    {
        var properties = await _affiliateEmploymentManager.GetEmploymentProperties(id, employmentSliceId, at ?? DateTime.UtcNow);
        var dto = await _employmentSliceMapper.MapAsync(properties, cancellationToken);
        return Ok(dto);
    }
}
