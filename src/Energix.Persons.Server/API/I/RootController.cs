using System.Diagnostics;
using System.Reflection;
using Energix.Persons.API;
using Energix.Persons.Server.BusinessLogic.PPW;
using Energix.Persons.Server.PPW.LinksManagers.Metadata;

using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using PPWCode.Server.Core.Transactional;

namespace Energix.Persons.Server.API.I
{
    [V1]
    [Route(Energix.Persons.API.Routes.Root)]
    [AllowAnonymous]
    [Transactional(false)]
    public class RootController : RestApiController
    {
        public RootController(
            [NotNull] IHostEnvironment hostEnvironment,
            [NotNull] IMetadataLinksManager metadataLinksManager)
        {
            HostEnvironment = hostEnvironment;
            MetadataLinksManager = metadataLinksManager;
        }

        [NotNull]
        public IHostEnvironment HostEnvironment { get; }

        [NotNull]
        public ILinksManager<Metadata, LinksV1Context> MetadataLinksManager { get; }

        [HttpGet(Name = Routes.Root_GetRoot)]
        [ProducesResponseType(typeof(void), 200)]
        public IActionResult GetRoot()
            => Ok();

        [HttpGet(Energix.Persons.API.Routes.Metadata, Name = Routes.Root_GetMetadata)]
        [ProducesResponseType(typeof(Metadata), 200)]
        public IActionResult GetMetadata()
        {
            Assembly thisAssembly = Assembly.GetAssembly(typeof(RootController));
            string version =
                thisAssembly != null
                    ? FileVersionInfo.GetVersionInfo(thisAssembly.Location).ProductVersion
                    : "unknown";

            Metadata metadata =
                new Metadata
                {
                    ApplicationName = HostEnvironment.ApplicationName,
                    EnvironmentName = HostEnvironment.EnvironmentName,
                    ContentRootPath = HostEnvironment.ContentRootPath,
                    ServerVersion = version
                };
            MetadataLinksManager.Initialize(metadata);

            return Ok(metadata);
        }
    }
}
