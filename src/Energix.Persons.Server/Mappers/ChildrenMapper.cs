using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IChildrenMapper : IEnergixMapper<Children, ChildrenDto, MapperContext>
{
}

public class ChildrenMapper
    : ComponentMapper<Children, ChildrenDto, MapperContext>,
      IChildrenMapper
{
    private readonly IEnergixRequestContext _requestContext;
    private readonly IChildLifeMapper _childLifeMapper;

    public ChildrenMapper(IEnergixRequestContext requestContext, IChildLifeMapper childLifeMapper)
    {
        _requestContext = requestContext;
        _childLifeMapper = childLifeMapper;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        Children component,
        ChildrenDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        dto.Href = new HistoryHrefDto
                   {
                       History = _requestContext.Link(
                           Routes.Affiliate_Children_History,
                           new Dictionary<string, object>
                           {
                               { "id", component.Affiliate.Id }
                           })
                   };

        dto.Items = await _childLifeMapper.MapAsync(component.Items, cancellationToken);
    }

    public override async Task<Children> MapAsync(ChildrenDto dto, MapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        ChildrenDto dto,
        Children component,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        var childLifeModels = await _childLifeMapper.MapAsync(dto.Items, cancellationToken);
        component.AddChildLives(childLifeModels);
    }

    /// <inheritdoc />
    protected override Children CreateModel(ChildrenDto dto, MapperContext context)
        => new Children();

    /// <inheritdoc />
    protected override ChildrenDto CreateDto()
        => new ChildrenDto();
}
