﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IRecognisedInvalidityIntervalMapper : IEnergixMapper<RecognisedInvalidityInterval, RecognisedInvalidityIntervalDto, MapperContext>
{
}

public class RecognisedInvalidityIntervalMapper
    : ComponentMapper<RecognisedInvalidityInterval, RecognisedInvalidityIntervalDto, MapperContext>,
      IRecognisedInvalidityIntervalMapper
{
    private readonly IEnergixRequestContext _requestContext;
    private readonly IRecognisedInvalidityMapper _recognisedInvalidityMapper;

    public RecognisedInvalidityIntervalMapper(IEnergixRequestContext requestContext, IRecognisedInvalidityMapper recognisedInvalidityMapper)
    {
        _requestContext = requestContext;
        _recognisedInvalidityMapper = recognisedInvalidityMapper;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        RecognisedInvalidityInterval component,
        RecognisedInvalidityIntervalDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        dto.Href = new HistoryHrefDto
                   {
                       History = _requestContext.Link(
                           Routes.Affiliate_Recognised_Invalidity_History,
                           new Dictionary<string, object>
                           {
                               { "id", component.Affiliate.Id }
                           })
                   };

        dto.Items = await _recognisedInvalidityMapper.MapAsync(component.Items, cancellationToken);
    }

    /// <inheritdoc />
    protected override RecognisedInvalidityInterval CreateModel(RecognisedInvalidityIntervalDto dto, MapperContext context)
        => new RecognisedInvalidityInterval();

    /// <inheritdoc />
    protected override RecognisedInvalidityIntervalDto CreateDto()
        => new RecognisedInvalidityIntervalDto();

    public override async Task<RecognisedInvalidityInterval> MapAsync(RecognisedInvalidityIntervalDto dto, MapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        RecognisedInvalidityIntervalDto dto,
        RecognisedInvalidityInterval component,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        var recognisedInvalidities = await _recognisedInvalidityMapper.MapAsync(dto.Items, cancellationToken);
        component.AddItem(recognisedInvalidities);
    }
}
