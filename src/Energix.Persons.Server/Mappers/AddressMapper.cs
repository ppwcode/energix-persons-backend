﻿using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.Server.Models.PPW.Components;

using JetBrains.Annotations;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

using AddressDto = Energix.Persons.API.Common.Address;

namespace Energix.Persons.Server.Mappers;

public interface IAddressMapper : IEnergixMapper<Address, AddressDto, MapperContext>
{
}

[UsedImplicitly]
public class AddressMapper
    : ComponentMapper<Address, AddressDto, MapperContext>,
      IAddressMapper
{
    /// <inheritdoc />
    public override Task MapAsync(
        Address source,
        AddressDto destination,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        destination.Street = source.Street;
        destination.HouseNumber = source.HouseNumber;
        destination.Box = source.Box;
        destination.ZipCode = source.ZipCode;
        destination.City = source.City;
        destination.Country = source.Country;

        return Task.CompletedTask;
    }

    /// <inheritdoc />
    protected override Address CreateModel(AddressDto dto, MapperContext context)
        => new AddressBuilder()
            .Street(dto.Street)
            .HouseNumber(dto.HouseNumber)
            .Box(dto.Box)
            .ZipCode(dto.ZipCode)
            .City(dto.City)
            .Country(dto.Country);

    /// <inheritdoc />
    protected override AddressDto CreateDto()
        => new AddressDto();
}
