using PPWCode.Server.Core.Mappers;

namespace Energix.Persons.Server.Mappers;

public class EnergixMapperContext : MapperContext
{
    public long? AffiliateId { get; set; }
}
