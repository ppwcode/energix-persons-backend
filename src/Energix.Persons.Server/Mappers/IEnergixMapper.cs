using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using PPWCode.API.Core;
using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Interfaces;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.Mappers;

public interface IEnergixMapper<TModel, TDto, in TContext>
    : PPWCode.Server.Core.Mappers.Interfaces.IBiDirectionalComponentMapper<TModel, TDto, TContext>,
      IMapper // important for container registration
    where TModel : ICivilizedObject
    where TDto : class, IDto
    where TContext : MapperContext, new()
{
}

public static class IEnergixMapperExtensions
{
    public static async Task<List<TDto>> MapAsync<TModel, TDto, TContext>(this IEnergixMapper<TModel, TDto, TContext> mapper, IEnumerable<TModel> models, CancellationToken cancellationToken)
        where TModel : ICivilizedObject
        where TDto : class, IDto
        where TContext : MapperContext, new()
    {
        var dtos = new List<TDto>();
        foreach (var model in models)
        {
            var dto = await mapper.MapAsync(model, cancellationToken);
            dtos.Add(dto);
        }

        return dtos;
    }

    public static async Task<List<TModel>> MapAsync<TModel, TDto, TContext>(this IEnergixMapper<TModel, TDto, TContext> mapper, IEnumerable<TDto> dtos, CancellationToken cancellationToken)
        where TModel : ICivilizedObject
        where TDto : class, IDto
        where TContext : MapperContext, new()
    {
        var models = new List<TModel>();
        foreach (var dto in dtos)
        {
            var model = await mapper.MapAsync(dto, cancellationToken);
            models.Add(model);
        }

        return models;
    }
}
