﻿using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.Models;
using Energix.Persons.Server.Models.PPW.Components;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IEmployerInvalidityMapper : IEnergixMapper<EmployerInvalidity, EmployerInvalidityDto, MapperContext>
{
}

public class EmployerInvalidityMapper
    : ComponentMapper<EmployerInvalidity, EmployerInvalidityDto, MapperContext>,
      IEmployerInvalidityMapper
{
    /// <inheritdoc />
    public override Task MapAsync(
        EmployerInvalidity model,
        EmployerInvalidityDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        if (model.Interval == null)
        {
            return Task.CompletedTask;
        }

        dto.Degree = model.Degree;
        dto.Interval = new IntervalDto()
                       {
                           Start = model.Interval.Start,
                           End = model.Interval.End
                       };
        return Task.CompletedTask;
    }

    public override async Task<EmployerInvalidity> MapAsync(EmployerInvalidityDto dto, MapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override Task MapAsync(
        EmployerInvalidityDto dto,
        EmployerInvalidity component,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        component.Interval = new Interval(dto.Interval.Start, dto.Interval.End);
        component.Degree = dto.Degree;
        return Task.CompletedTask;
    }

    /// <inheritdoc />
    protected override EmployerInvalidity CreateModel(EmployerInvalidityDto dto, MapperContext context)
        => new EmployerInvalidity();

    /// <inheritdoc />
    protected override EmployerInvalidityDto CreateDto()
        => new EmployerInvalidityDto();
}
