﻿using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.Models;
using Energix.Persons.Server.Models.PPW.Components;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IChildLifeMapper : IEnergixMapper<ChildLife, ChildLifeDto, MapperContext>
{
}

public class ChildLifeMapper
    : ComponentMapper<ChildLife, ChildLifeDto, MapperContext>,
      IChildLifeMapper
{
    /// <inheritdoc />
    public override Task MapAsync(
        ChildLife model,
        ChildLifeDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        if (model.Interval == null)
        {
            return Task.CompletedTask;
        }

        dto.Interval = new IntervalDto()
                       {
                           Start = model.Interval.Start,
                           End = model.Interval.End
                       };
        return Task.CompletedTask;
    }

    public override async Task<ChildLife> MapAsync(ChildLifeDto dto, MapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override Task MapAsync(
        ChildLifeDto dto,
        ChildLife component,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        component.Interval = new Interval(dto.Interval.Start, dto.Interval.End);
        return Task.CompletedTask;
    }

    /// <inheritdoc />
    protected override ChildLife CreateModel(ChildLifeDto dto, MapperContext context)
        => new ChildLife();

    /// <inheritdoc />
    protected override ChildLifeDto CreateDto()
        => new ChildLifeDto();
}
