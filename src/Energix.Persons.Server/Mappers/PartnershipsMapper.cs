﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IPartnershipsMapper : IEnergixMapper<Partnerships, PartnershipsDto, MapperContext>
{
}

public class PartnershipsMapper
    : ComponentMapper<Partnerships, PartnershipsDto, MapperContext>,
      IPartnershipsMapper
{
    private readonly IEnergixRequestContext _requestContext;
    private readonly IPartnershipMapper _partnershipMapper;

    public PartnershipsMapper(IEnergixRequestContext requestContext, IPartnershipMapper partnershipMapper)
    {
        _requestContext = requestContext;
        _partnershipMapper = partnershipMapper;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        Partnerships component,
        PartnershipsDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        dto.Href = new HistoryHrefDto
                   {
                       History = _requestContext.Link(
                           Routes.Affiliate_Partnerships_History,
                           new Dictionary<string, object>
                           {
                               { "id", component.Affiliate.Id }
                           })
                   };

        dto.Items = await _partnershipMapper.MapAsync(component.Items, cancellationToken);
    }

    public override async Task<Partnerships> MapAsync(PartnershipsDto dto, MapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        PartnershipsDto dto,
        Partnerships component,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        var partnerShipModels = await _partnershipMapper.MapAsync(dto.Items, cancellationToken);
        component.AddPartners(partnerShipModels);
    }

    /// <inheritdoc />
    protected override Partnerships CreateModel(PartnershipsDto dto, MapperContext context)
        => new Partnerships();

    /// <inheritdoc />
    protected override PartnershipsDto CreateDto()
        => new PartnershipsDto();
}
