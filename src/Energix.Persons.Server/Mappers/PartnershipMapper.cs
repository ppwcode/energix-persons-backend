using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.Models;
using Energix.Persons.Server.Models.PPW.Components;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IPartnershipMapper : IEnergixMapper<Partnership, PartnershipDto, MapperContext>
{
}

public class PartnershipMapper
    : ComponentMapper<Partnership, PartnershipDto, MapperContext>,
      IPartnershipMapper
{
    /// <inheritdoc />
    public override Task MapAsync(
        Partnership model,
        PartnershipDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        dto.Gender = model.MortalityTableGender;
        dto.CivilState = model.CivilState;
        dto.DateOfBirth = model.DateOfBirth;
        dto.Interval = new IntervalDto()
                       {
                           Start = model.Interval.Start,
                           End = model.Interval.End
                       };
        return Task.CompletedTask;
    }

    public override async Task<Partnership> MapAsync(PartnershipDto dto, MapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override Task MapAsync(
        PartnershipDto dto,
        Partnership component,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        component.Interval = new Interval(dto.Interval.Start, dto.Interval.End);
        component.CivilState = dto.CivilState;
        component.DateOfBirth = dto.DateOfBirth;
        component.MortalityTableGender = dto.Gender;
        return Task.CompletedTask;
    }

    /// <inheritdoc />
    protected override Partnership CreateModel(PartnershipDto dto, MapperContext context)
        => new Partnership();

    /// <inheritdoc />
    protected override PartnershipDto CreateDto()
        => new PartnershipDto();
}
