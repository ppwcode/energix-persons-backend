﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IEmployerInvalidityIntervalMapper : IEnergixMapper<EmployerInvalidityInterval, EmployerInvalidityIntervalDto, MapperContext>
{
}

public class EmployerInvalidityIntervalMapper
    : ComponentMapper<EmployerInvalidityInterval, EmployerInvalidityIntervalDto, MapperContext>,
      IEmployerInvalidityIntervalMapper
{
    private readonly IEnergixRequestContext _requestContext;
    private readonly IEmployerInvalidityMapper _employerInvalidityMapper;

    public EmployerInvalidityIntervalMapper(IEnergixRequestContext requestContext, IEmployerInvalidityMapper employerInvalidityMapper)
    {
        _requestContext = requestContext;
        _employerInvalidityMapper = employerInvalidityMapper;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        EmployerInvalidityInterval component,
        EmployerInvalidityIntervalDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        dto.Href = new HistoryHrefDto
                   {
                       History = _requestContext.Link(
                           Routes.Affiliate_Employer_Invalidity_History,
                           new Dictionary<string, object>
                           {
                               { "id", component.Affiliate.Id }
                           })
                   };

        dto.Items = await _employerInvalidityMapper.MapAsync(component.Items, cancellationToken);
    }

    /// <inheritdoc />
    protected override EmployerInvalidityInterval CreateModel(EmployerInvalidityIntervalDto dto, MapperContext context)
        => new EmployerInvalidityInterval();

    /// <inheritdoc />
    protected override EmployerInvalidityIntervalDto CreateDto()
        => new EmployerInvalidityIntervalDto();

    public override async Task<EmployerInvalidityInterval> MapAsync(EmployerInvalidityIntervalDto dto, MapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        EmployerInvalidityIntervalDto dto,
        EmployerInvalidityInterval component,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        var employerInvalidities = await _employerInvalidityMapper.MapAsync(dto.Items, cancellationToken);
        component.AddItem(employerInvalidities);
    }
}
