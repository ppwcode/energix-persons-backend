﻿using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.Models;
using Energix.Persons.Server.Models.PPW.Components;

using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IRecognisedInvalidityMapper : IEnergixMapper<RecognisedInvalidity, RecognisedInvalidityDto, MapperContext>
{
}

public class RecognisedInvalidityMapper
    : ComponentMapper<RecognisedInvalidity, RecognisedInvalidityDto, MapperContext>,
      IRecognisedInvalidityMapper
{
    /// <inheritdoc />
    public override Task MapAsync(
        RecognisedInvalidity model,
        RecognisedInvalidityDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        if (model.Interval == null)
        {
            return Task.CompletedTask;
        }

        dto.Degree = model.Degree;
        dto.Type = model.Type;
        dto.Interval = new IntervalDto()
                       {
                           Start = model.Interval.Start,
                           End = model.Interval.End
                       };
        return Task.CompletedTask;
    }

    public override async Task<RecognisedInvalidity> MapAsync(RecognisedInvalidityDto dto, MapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override Task MapAsync(
        RecognisedInvalidityDto dto,
        RecognisedInvalidity component,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        component.Interval = new Interval(dto.Interval.Start, dto.Interval.End);
        component.Degree = dto.Degree;
        component.Type = dto.Type;
        return Task.CompletedTask;
    }

    /// <inheritdoc />
    protected override RecognisedInvalidity CreateModel(RecognisedInvalidityDto dto, MapperContext context)
        => new RecognisedInvalidity();

    /// <inheritdoc />
    protected override RecognisedInvalidityDto CreateDto()
        => new RecognisedInvalidityDto();
}
