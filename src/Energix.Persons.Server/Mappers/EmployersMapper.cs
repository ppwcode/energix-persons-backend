using System.Collections.Generic;
using System.Linq;

using Energix.Persons.API;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models;

using PPWCode.Server.Core.Mappers.Interfaces;

namespace Energix.Persons.Server.Mappers;

public interface IEmployersMapper : IMapper
{
    EmployersDto Map(List<EmploymentSliceProperties> employmentSliceProperties, long affiliateId);
}

public class EmployersMapper : IEmployersMapper
{
    private readonly IEnergixRequestContext _requestContext;

    public EmployersMapper(IEnergixRequestContext requestContext)
    {
        _requestContext = requestContext;
    }

    public EmployersDto Map(List<EmploymentSliceProperties> employmentSliceProperties, long affiliateId)
    {
        var links = employmentSliceProperties.Select(
                x => _requestContext.Link(
                    Routes.Affiliate_EmploymentSlice_Get,
                    new Dictionary<string, object>
                    {
                        { "id", affiliateId },
                        { "employmentSliceId", x.EmploymentSlice.Id },
                        { "at", x.CreatedAt }
                    }))
            .ToList();
        return new EmployersDto
               {
                   Hrefs = links
               };
    }
}
