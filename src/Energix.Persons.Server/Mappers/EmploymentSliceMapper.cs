﻿using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.Server.Models;

using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IEmploymentSliceMapper : IEnergixMapper<EmploymentSliceProperties, EmploymentSliceDto, EnergixMapperContext>
{
}

public class EmploymentSliceMapper
    : ComponentMapper<EmploymentSliceProperties, EmploymentSliceDto, EnergixMapperContext>,
      IEmploymentSliceMapper
{
    /// <inheritdoc />
    public override Task MapAsync(
        EmploymentSliceProperties component,
        EmploymentSliceDto dto,
        EnergixMapperContext context,
        CancellationToken cancellationToken)
    {
        dto.EmploymentCategory = component.EmploymentCategory;
        dto.WorkFraction = new FractionDto
                           {
                               Numerator = component.WorkFraction.Numerator,
                               Denominator = component.WorkFraction.Denominator
                           };
        dto.ProjectedWorkFraction = component.ProjectedWorkFraction;
        return Task.CompletedTask;
    }

    /// <inheritdoc />
    protected override EmploymentSliceProperties CreateModel(EmploymentSliceDto dto, EnergixMapperContext context)
        => new EmploymentSliceProperties();

    /// <inheritdoc />
    protected override EmploymentSliceDto CreateDto()
        => new EmploymentSliceDto();
}
