﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models;

using PPWCode.Server.Core.Mappers;

namespace Energix.Persons.Server.Mappers;

public interface IAbsorptionMapper : IEnergixMapper<Absorption, AbsorptionDto, MapperContext>
{
}

public class AbsorptionMapper
    : PPWCode.Server.Core.Mappers.Implementations.ComponentMapper<Absorption, AbsorptionDto, MapperContext>,
      IAbsorptionMapper
{
    private readonly IEnergixRequestContext _requestContext;

    public AbsorptionMapper(IEnergixRequestContext requestContext)
    {
        _requestContext = requestContext;
    }

    /// <inheritdoc />
    public override Task MapAsync(
        Absorption model,
        AbsorptionDto dto,
        MapperContext context,
        CancellationToken cancellationToken)
    {
        dto.Absorbed = _requestContext.Link(
            Routes.Affiliate_Get,
            new Dictionary<string, object>
            {
                { "id", model.Absorbed.Id }
            });

        dto.AbsorbedBy = _requestContext.Link(
            Routes.Affiliate_Get,
            new Dictionary<string, object>
            {
                { "id", model.AbsorbedBy.Id }
            });

        return Task.CompletedTask;
    }

    /// <inheritdoc />
    protected override Absorption CreateModel(AbsorptionDto dto, MapperContext context)
        => new Absorption();

    /// <inheritdoc />
    protected override AbsorptionDto CreateDto()
        => new AbsorptionDto();
}
