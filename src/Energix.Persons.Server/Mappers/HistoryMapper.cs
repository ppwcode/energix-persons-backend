using System;
using System.Collections.Generic;
using System.Linq;

using Energix.Persons.API;
using Energix.Persons.PPW.Extensions;

using PPWCode.Server.Core.Mappers.Interfaces;

namespace Energix.Persons.Server.Mappers;

public interface IHistoryMapper : IMapper
{
    HistoryDto Map(List<DateTime> dates, string routeName, long routeId);
}

public class HistoryMapper : IHistoryMapper
{
    private readonly IEnergixRequestContext _requestContext;

    public HistoryMapper(IEnergixRequestContext requestContext)
    {
        _requestContext = requestContext;
    }

    public HistoryDto Map(List<DateTime> dates, string routeName, long routeId)
    {
        var links = dates.Select(
                x => _requestContext.Link(
                    routeName,
                    new Dictionary<string, object>
                    {
                        { "id", routeId },
                        { "at", x.ToString("O") }
                    }))
            .ToList();
        return new HistoryDto
               {
                   Items = links
               };
    }
}