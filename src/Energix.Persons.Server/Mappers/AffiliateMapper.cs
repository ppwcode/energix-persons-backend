﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API;
using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.Models;

using PPWCode.Server.Core.Mappers.Implementations;

namespace Energix.Persons.Server.Mappers;

public interface IAffiliateMapper : IEnergixMapper<AffiliateProperties, AffiliateDto, EnergixMapperContext>
{
}

public class AffiliateMapper
    : ComponentMapper<AffiliateProperties, AffiliateDto, EnergixMapperContext>,
      IAffiliateMapper
{
    private readonly IEnergixRequestContext _requestContext;
    private readonly IAddressMapper _addressMapper;

    public AffiliateMapper(IEnergixRequestContext requestContext, IAddressMapper addressMapper)
    {
        _requestContext = requestContext;
        _addressMapper = addressMapper;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        AffiliateProperties model,
        AffiliateDto dto,
        EnergixMapperContext context,
        CancellationToken cancellationToken)
    {
        var affiliateId = model.Affiliate.Id;
        dto.Id = affiliateId;
        dto.FirstName = model.FirstName;
        dto.LastName = model.LastName;
        dto.Inss = model.Inss;
        dto.Language = model.Language;
        dto.DateOfBirth = model.DateOfBirth;
        dto.MortalityTableGender = model.MortalityTableGender;
        dto.Address = await _addressMapper.MapAsync(model.Address, cancellationToken);

        var affiliateDict = new Dictionary<string, object>
                            {
                                { "id", affiliateId }
                            };
        dto.Href = new AffiliateHref
                   {
                       History = _requestContext.Link(Routes.Affiliate_History, affiliateDict),
                       Partnerships = _requestContext.Link(Routes.Affiliate_Partnerships, affiliateDict),
                       Absorbed = _requestContext.Link(Routes.Affiliate_Absorbed, affiliateDict),
                       Children = _requestContext.Link(Routes.Affiliate_Children, affiliateDict),
                       Absorptions = _requestContext.Link(Routes.Affiliate_Absorptions, affiliateDict),
                       Sources = _requestContext.Link(Routes.Affiliate_Sources, affiliateDict),
                   };
    }

    /// <inheritdoc />
    public override async Task<AffiliateProperties> MapAsync(AffiliateDto dto, EnergixMapperContext context, CancellationToken cancellationToken)
    {
        if (dto == null)
        {
            return null;
        }

        var model = await base.MapAsync(dto, context, cancellationToken);
        if (model == null)
        {
            return null;
        }

        await MapAsync(
            dto,
            model,
            context,
            cancellationToken);
        return model;
    }

    /// <inheritdoc />
    public override async Task MapAsync(
        AffiliateDto dto,
        AffiliateProperties component,
        EnergixMapperContext context,
        CancellationToken cancellationToken)
    {
        component.FirstName = dto.FirstName;
        component.LastName = dto.LastName;
        component.Inss = dto.Inss;
        component.Language = dto.Language;
        component.DateOfBirth = dto.DateOfBirth;
        component.MortalityTableGender = dto.MortalityTableGender;
        component.Address = await _addressMapper.MapAsync(dto.Address, cancellationToken);
    }

    /// <inheritdoc />
    protected override AffiliateProperties CreateModel(AffiliateDto dto, EnergixMapperContext context)
        => new AffiliateProperties();

    /// <inheritdoc />
    protected override AffiliateDto CreateDto()
        => new AffiliateDto();
}
