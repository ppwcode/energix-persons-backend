using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Energix.Persons.API.Common;

using JetBrains.Annotations;

using PPWCode.API.Core;
using PPWCode.API.Core.Contracts;
using PPWCode.Server.Core.Mappers;
using PPWCode.Server.Core.Mappers.Interfaces;
using PPWCode.Vernacular.Persistence.IV;

namespace Energix.Persons.Server.PPW.Util
{
    public static class LinqExtensions
    {
        public static async Task<IPagedList<TDto>> GetPagedList<TModel, TIdentity, TDto, TContext, TCriteria>(
            [CanBeNull] this IOrderedEnumerable<TModel> items,
            [NotNull] TCriteria criteria,
            [NotNull] IToDtoPersistentObjectMapper<TModel, TIdentity, TDto, TContext> mapper,
            [CanBeNull] TContext context,
            CancellationToken cancellationToken)
            where TModel : class, IPersistentObject<TIdentity>
            where TIdentity : struct, IEquatable<TIdentity>
            where TCriteria : PagedSearchCriteria, new()
            where TDto : PersistentDto<TIdentity>
            where TContext : MapperContext, new()
        {
            Contract.Requires(criteria.Page != null);
            Contract.Requires(criteria.PageSize != null);

            if (items == null)
            {
                return
                    new PagedList<TDto>(
                        Enumerable.Empty<TDto>(),
                        criteria.Page.Value,
                        criteria.PageSize.Value,
                        0);
            }

            IEnumerable<TModel> pageItems =
                items
                    .Skip((criteria.Page.Value - 1) * criteria.PageSize.Value)
                    .Take(criteria.PageSize.Value);

            TDto[] mappedItems =
                context == null
                    ? await mapper
                          .MapAsync(pageItems, cancellationToken)
                          .ConfigureAwait(false)
                    : await mapper
                          .MapAsync(pageItems, context, cancellationToken)
                          .ConfigureAwait(false);

            return
                new PagedList<TDto>(
                    mappedItems,
                    criteria.Page.Value,
                    criteria.PageSize.Value,
                    items.Count());
        }

        public static IPagedList<TModel> GetPagedList<TModel, TCriteria>(
            [CanBeNull] this IOrderedEnumerable<TModel> items,
            [NotNull] TCriteria criteria)
            where TModel : class
            where TCriteria : PagedSearchCriteria, new()
        {
            Contract.Requires(criteria.Page != null);
            Contract.Requires(criteria.PageSize != null);

            if (items == null)
            {
                return
                    new PagedList<TModel>(
                        Enumerable.Empty<TModel>(),
                        criteria.Page.Value,
                        criteria.PageSize.Value,
                        0);
            }

            IEnumerable<TModel> pageItems =
                items
                    .Skip((criteria.Page.Value - 1) * criteria.PageSize.Value)
                    .Take(criteria.PageSize.Value);

            return
                new PagedList<TModel>(
                    pageItems,
                    criteria.Page.Value,
                    criteria.PageSize.Value,
                    items.Count());
        }

        public static IPagedList<TModel> GetPagedList<TModel, TCriteria>(
            [CanBeNull] this IQueryable<TModel> items,
            [NotNull] TCriteria criteria)
            where TModel : class
            where TCriteria : PagedSearchCriteria, new()
        {
            Contract.Requires(criteria.Page != null);
            Contract.Requires(criteria.PageSize != null);

            if (items == null)
            {
                return
                    new PagedList<TModel>(
                        Enumerable.Empty<TModel>(),
                        criteria.Page.Value,
                        criteria.PageSize.Value,
                        0);
            }

            IEnumerable<TModel> pageItems =
                items
                    .Skip((criteria.Page.Value - 1) * criteria.PageSize.Value)
                    .Take(criteria.PageSize.Value);

            return
                new PagedList<TModel>(
                    pageItems,
                    criteria.Page.Value,
                    criteria.PageSize.Value,
                    items.Count());
        }
    }
}
