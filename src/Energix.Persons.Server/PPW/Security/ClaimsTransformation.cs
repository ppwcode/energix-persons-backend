using System.Security.Claims;
using System.Threading.Tasks;

using JetBrains.Annotations;

using Microsoft.AspNetCore.Authentication;

namespace Energix.Persons.Server.PPW.Security
{
    /// <inheritdoc />
    [UsedImplicitly]
    public class ClaimsTransformation : IClaimsTransformation
    {
        /// <inheritdoc />
        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
            => Task.FromResult(principal);
    }
}
