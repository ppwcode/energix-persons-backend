using Energix.Persons.Server.BusinessLogic.PPW;

namespace Energix.Persons.Server.PPW.LinksManagers.Metadata
{
    /// <inheritdoc />
    public interface IMetadataLinksManager : ILinksManager<Energix.Persons.API.Metadata, LinksV1Context>
    {
    }
}
