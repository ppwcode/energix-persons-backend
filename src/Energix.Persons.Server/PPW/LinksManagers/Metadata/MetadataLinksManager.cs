using System.Collections.Generic;

using Energix.Persons.PPW.Extensions;
using Energix.Persons.Server.BusinessLogic.PPW;

using JetBrains.Annotations;

namespace Energix.Persons.Server.PPW.LinksManagers.Metadata
{
    /// <inheritdoc cref="IMetadataLinksManager" />
    [UsedImplicitly]
    public class MetadataLinksManager
        : SimpleLinksManager<Energix.Persons.API.Metadata, LinksV1Context>,
          IMetadataLinksManager
    {
        public MetadataLinksManager([NotNull] IEnergixRequestContext requestContext)
            : base(requestContext)
        {
        }

        /// <inheritdoc />
        protected override string SelfRoute
            => Routes.Root_GetMetadata;

        /// <inheritdoc />
        protected override IDictionary<string, object> GetSelfRouteParameters(Energix.Persons.API.Metadata source, LinksV1Context context)
            => new Dictionary<string, object>();

        /// <inheritdoc />
        protected override IEnumerable<KeyValuePair<string, IDictionary<string, object>>> GetAdditionalLinks(Energix.Persons.API.Metadata source, LinksV1Context context)
        {
            yield return new KeyValuePair<string, IDictionary<string, object>>(
                "root",
                new Dictionary<string, object>
                {
                    {
                        HRefKey,
                        GetHref(source, context, Routes.Root_GetRoot, GetSelfRouteParameters(source, context))
                    }
                });
        }
    }
}
