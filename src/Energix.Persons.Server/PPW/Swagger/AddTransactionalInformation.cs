using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;

using PPWCode.Server.Core.Transactional;

using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Server.PPW.Swagger
{
    /// <inheritdoc />
    /// <summary>
    ///     Operation filter to add the transaction information for the endpoint.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class AddTransactionalInformation : BaseResponseCodes
    {
        /// <inheritdoc />
        public override void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            ControllerActionDescriptor controllerActionDescriptor = ControllerActionDescriptor(context);
            if (controllerActionDescriptor != null)
            {
                TransactionalAttribute attribute =
                    controllerActionDescriptor
                        .MethodInfo
                        .GetCustomAttributes(typeof(TransactionalAttribute), true)
                        .OfType<TransactionalAttribute>()
                        .SingleOrDefault()
                    ?? controllerActionDescriptor
                        .ControllerTypeInfo
                        .GetCustomAttributes(typeof(TransactionalAttribute), true)
                        .OfType<TransactionalAttribute>()
                        .SingleOrDefault();
                bool transactional = attribute?.Transactional ?? true;
                IsolationLevel isolationLevel = attribute?.IsolationLevel ?? IsolationLevel.Unspecified;
                StringBuilder sb = new StringBuilder();
                if (transactional)
                {
                    sb
                        .Append("<b>Transactional</b>: Yes<br>")
                        .AppendFormat("<b>Isolation level</b>: {0}<br>", isolationLevel);
                }
                else
                {
                    sb
                        .Append("<b>Transactional</b>: No<br>");
                }

                if (operation.Description != null)
                {
                    sb
                        .Append("<br>")
                        .Append(operation.Description);
                }

                operation.Description = sb.ToString();
            }
        }
    }
}
