using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Microsoft.OpenApi.Models;

using PPWCode.Server.Core.Models;

using Swashbuckle.AspNetCore.SwaggerGen;

namespace Energix.Persons.Server.PPW.Swagger
{
    /// <inheritdoc />
    public abstract class BaseResponseCodes : IOperationFilter
    {
        public abstract void Apply([NotNull] OpenApiOperation operation, [NotNull] OperationFilterContext context);

        protected virtual bool IsGet([NotNull] OperationFilterContext context)
            => string.Equals(context.ApiDescription.HttpMethod, HttpMethod.Get.ToString(), StringComparison.OrdinalIgnoreCase);

        protected virtual bool IsPost([NotNull] OperationFilterContext context)
            => string.Equals(context.ApiDescription.HttpMethod, HttpMethod.Post.ToString(), StringComparison.OrdinalIgnoreCase);

        protected virtual bool IsPut([NotNull] OperationFilterContext context)
            => string.Equals(context.ApiDescription.HttpMethod, HttpMethod.Put.ToString(), StringComparison.OrdinalIgnoreCase);

        protected virtual bool IsDelete([NotNull] OperationFilterContext context)
            => string.Equals(context.ApiDescription.HttpMethod, HttpMethod.Delete.ToString(), StringComparison.OrdinalIgnoreCase);

        [CanBeNull]
        protected virtual ControllerActionDescriptor ControllerActionDescriptor([NotNull] OperationFilterContext context)
            => context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;

        [CanBeNull]
        protected virtual string GetControllerName([NotNull] OperationFilterContext context)
            => ControllerActionDescriptor(context)?.ControllerName;

        protected virtual IEnumerable<ApiParameterDescription> GetParameters(ApiDescription apiDescription, Func<ApiDescription, ApiParameterDescription, bool> func = null)
            => apiDescription
                .ParameterDescriptions
                .Where(pd => (func == null) || func(apiDescription, pd));

        protected virtual string GetIdentification(ApiParameterDescription description)
        {
            string paramName = description.Name;
            if (description.ParameterDescriptor != null)
            {
                Type paramType = description.ParameterDescriptor.ParameterType;
                if (typeof(IPersistentObject).IsAssignableFrom(paramType))
                {
                    paramName = string.Concat(paramName, ".Id");
                }
            }

            return paramName;
        }

        protected virtual string GetIdentifications(ApiDescription apiDescription, Func<ApiDescription, ApiParameterDescription, bool> func = null)
            => string.Join(
                ", ",
                GetParameters(apiDescription, func)
                    .Where(p => p.ParameterDescriptor.ParameterType != typeof(ApiVersion))
                    .Select(GetIdentification));

        protected virtual bool ResponseExists(OpenApiOperation operation, string key)
            => operation.Responses.ContainsKey(key);

        protected virtual void AddResponse(OpenApiOperation operation, string key, OpenApiResponse response)
        {
            operation.Responses.Add(key, response);
        }

        protected virtual void RemoveResponse(OpenApiOperation operation, string key)
        {
            if (ResponseExists(operation, key))
            {
                operation.Responses.Remove(key);
            }
        }

        protected virtual void AddResponseIfNotExists(OpenApiOperation operation, string key, OpenApiResponse response)
        {
            if (!ResponseExists(operation, key))
            {
                AddResponse(operation, key, response);
            }
        }

        protected virtual void ForceAddResponse(OpenApiOperation operation, string key, OpenApiResponse response)
        {
            if (ResponseExists(operation, key))
            {
                RemoveResponse(operation, key);
            }

            AddResponse(operation, key, response);
        }

        protected virtual void ConditionalAddResponse(OpenApiOperation operation, string key, OpenApiResponse response)
        {
            AddResponse(operation, ResponseExists(operation, key) ? string.Concat('*', key) : key, response);
        }
    }
}
