﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using JetBrains.Annotations;

using PPWCode.Server.Core.Mappers.Interfaces;
using PPWCode.Server.Core.Models;

namespace Energix.Persons.Server.PPW.Bootstrap.Installers
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class MapperInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Assembly thisAssembly = typeof(MapperInstaller).Assembly;
            Assembly serverCoreAssembly = typeof(IPersistentObject).Assembly;

            container.Register(
                Classes
                    .FromAssembly(thisAssembly)
                    .BasedOn(typeof(IMapper))
                    .WithService.FromInterface()
                    .LifestyleTransient(),
                Classes
                    .FromAssembly(serverCoreAssembly)
                    .BasedOn(typeof(IMapper))
                    .WithService.FromInterface()
                    .LifestyleTransient());
        }
    }
}
