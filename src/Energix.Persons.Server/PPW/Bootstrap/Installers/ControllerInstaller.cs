using System.Diagnostics.CodeAnalysis;
using System.Reflection;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using JetBrains.Annotations;

using PPWCode.Server.Core.API;

namespace Energix.Persons.Server.PPW.Bootstrap.Installers
{
    /// <inheritdoc cref="IWindsorInstaller" />
    [ExcludeFromCodeCoverage]
    [UsedImplicitly]
    public class ControllerInstaller : IWindsorInstaller
    {
        /// <inheritdoc />
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Assembly thisAssembly = typeof(ControllerInstaller).Assembly;

            container.Register(
                Classes
                    .FromAssembly(thisAssembly)
                    .BasedOn<IRestApiController>()
                    .LifestyleScoped());
        }
    }
}
