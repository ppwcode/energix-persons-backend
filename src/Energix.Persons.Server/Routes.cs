using System.Diagnostics.CodeAnalysis;

namespace Energix.Persons.Server
{
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Route naming [controller]_[method]")]
    [SuppressMessage("ReSharper", "SA1310", Justification = "Route naming [controller]_[method]")]
    public static class Routes
    {
        public const string Root_GetRoot = nameof(Root_GetRoot);
        public const string Root_GetMetadata = nameof(Root_GetMetadata);

        public const string Health_Index = nameof(Health_Index);

        public const string Affiliate_Post = nameof(Affiliate_Post);
        public const string Affiliate_Put = nameof(Affiliate_Put);
        public const string Affiliate_Get = nameof(Affiliate_Get);
        public const string Affiliate_History = nameof(Affiliate_History);
        public const string Affiliate_Partnerships = nameof(Affiliate_Partnerships);
        public const string Affiliate_Partnerships_Put = nameof(Affiliate_Partnerships_Put);
        public const string Affiliate_Partnerships_History = nameof(Affiliate_Partnerships_History);
        public const string Affiliate_Children = nameof(Affiliate_Children);
        public const string Affiliate_Children_Put = nameof(Affiliate_Children_Put);
        public const string Affiliate_Children_History = nameof(Affiliate_Children_History);
        public const string Affiliate_Absorbed = nameof(Affiliate_Absorbed);
        public const string Affiliate_Absorptions = nameof(Affiliate_Absorptions);
        public const string Affiliate_Sources = nameof(Affiliate_Sources);

        public const string Affiliate_Employers_Get = nameof(Affiliate_Employers_Get);
        public const string Affiliate_EmploymentSlice_Get = nameof(Affiliate_EmploymentSlice_Get);

        public const string Affiliate_Employer_Invalidity = nameof(Affiliate_Employer_Invalidity);
        public const string Affiliate_Employer_Invalidity_Put = nameof(Affiliate_Employer_Invalidity_Put);
        public const string Affiliate_Employer_Invalidity_History = nameof(Affiliate_Employer_Invalidity_History);

        public const string Affiliate_Recognised_Invalidity = nameof(Affiliate_Recognised_Invalidity);
        public const string Affiliate_Recognised_Invalidity_Put = nameof(Affiliate_Recognised_Invalidity_Put);
        public const string Affiliate_Recognised_Invalidity_History = nameof(Affiliate_Recognised_Invalidity_History);
    }
}
