using System;

using Microsoft.AspNetCore.Mvc;

namespace Energix.Persons.Server
{
    /// <inheritdoc />
    /// <remarks>The ApiVersion is 1.0 with this attribute</remarks>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class V1Attribute : ApiVersionAttribute
    {
        public V1Attribute()
            : base(new ApiVersion(1, 0))
        {
        }
    }
}
